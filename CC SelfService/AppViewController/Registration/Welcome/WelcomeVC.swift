//
//  WelcomeVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 10/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit
import FirebaseFirestore

class WelcomeVC: BaseViewController {
    
    @IBOutlet var viewParentSelectLangauge: UIView!
    @IBOutlet var constraintYAppLogo: NSLayoutConstraint!
    @IBOutlet var imgAppLogo: UIImageView!
    
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupFirebase()
    }
    
    func setupFirebase() {
        Indicator.shared.showProgressView(self.view)
        let docRef = Firestore.firestore().collection("SelfService").document("UrlType")
        
        docRef.getDocument { (querySnapshot, err)  in
            if let err = err {
                print("Error getting documents: \(err)")
                self.setTestUrl(isTest: false)
            } else {
                let data = FirebaseDataModel(snapsot: (querySnapshot?.data())!)
                self.setTestUrl(isTest: data.isTest ?? false)
            }
        }
    }
    
    func setTestUrl(isTest: Bool) {
        if isTest {
            KCurrentAPIBaseURL = KTestCurrentAPIBaseURL
            KAppKeyDefaultTestApp = KTestAppKeyDefaultTestApp
            KSecretKeyDefaultTestApp = KTestSecretKeyDefaultTestApp
        }
        Indicator.shared.hideProgressView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        viewParentSelectLangauge.isHidden=true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Do Animation (Animate image view  from center to bottom) UI Updation should be done on Main Thread
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            self.doLogoAnimationFromCenterToBottom()
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.viewParentSelectLangauge.isHidden = false
            self.showLanguageSubViewWithAnimation()
        })
        
    }
    
    //MARK: HELPER METHODS
    func setupView() {
        viewParentSelectLangauge.backgroundColor = UIColor.clear
    }
    
    func doLogoAnimationFromCenterToBottom()  {
        
        UIView.animate(withDuration: 1, animations: {
            
            self.constraintYAppLogo.constant = 200
            self.view.layoutIfNeeded()
            
            /*
             self.constranitImgVLogoWidth.constant = self.imageViewLogo.frame.width
             self.constranitImgVLogoHeight.constant = self.imageViewLogo.frame.height
             self.imageViewLogo.contentMode = .scaleAspectFit
             self.imageViewLogo.clipsToBounds = true
             */
            
        }, completion: nil)
    }
    
    func showLanguageSubViewWithAnimation()  {
        viewParentSelectLangauge.transform = CGAffineTransform (scaleX:0.00001, y: 0.00001)
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.viewParentSelectLangauge.transform = CGAffineTransform.identity
        })
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnEnglishClicked(_ sender: Any) {
        
        // UIView.appearance().semanticContentAttribute =  .forceLeftToRight
        let newViewController = StartVC()
        self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    
    @IBAction func btnArabicClicked(_ sender: Any) {
        
        
        let newViewController = StartVC()
        self.navigationController?.pushViewController(newViewController, animated: true)
        
        //view.makeToast("Arabic text not available", duration: 2.0, position: .center)
         /*
         UIView.appearance().semanticContentAttribute = .forceRightToLeft
         let newViewController = StartVC()
         self.navigationController?.pushViewController(newViewController, animated: true)
         */
    }
}
