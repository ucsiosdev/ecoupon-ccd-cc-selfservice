
//
//  PopupReciptEntryDatePicker.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 06/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

protocol PopupReciptEntryDatePickerDelegate : class{
    
    
    func datePickerValueDidChange(selectedDate:Date)
    
}


class PopupReciptEntryDatePicker: BaseViewController {
    
    @IBOutlet var datePicker: UIDatePicker!
    weak var delegate : PopupReciptEntryDatePickerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initObj()
        
    }
    
    
    func initObj (){
        
        let calendar = Calendar(identifier: .gregorian)
        let comps = DateComponents()
        //comps.year = 30
        let maxDate = calendar.date(byAdding: comps, to: Date())
        //comps.year = -50
        //let minDate = calendar.date(byAdding: comps, to: Date())
        datePicker.maximumDate = maxDate
        // datePicker.minimumDate = minDate
        datePicker.datePickerMode =  .date
    }
    
    
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = kdisplayDateFormatWithoutTime
        if let _ = self.delegate?.datePickerValueDidChange(selectedDate: datePicker.date){
           print ("\n\n *** is Delegate Method > datePickerValueDidChange Avaialble: YES  ***\n\n")
        }
    }
}
