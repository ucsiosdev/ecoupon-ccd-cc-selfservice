//
//  TblViewCellReceiptEntry.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 29/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class TblViewCellReceiptEntry: UITableViewCell {

    @IBOutlet var lblReceipt: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblStore: UILabel!
    @IBOutlet var lblMallName: UILabel!
    @IBOutlet var viewParentLabels: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
