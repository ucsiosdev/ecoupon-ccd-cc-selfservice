//
//  ReceiptEntryVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 29/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class ReceiptEntryVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet var viewParentCellLables: UIView!
    @IBOutlet var headerView: UITableViewCell!
    @IBOutlet var TFSelectMall: SelfSerticeTextField!
    @IBOutlet var TFSelectStore: SelfSerticeTextField!
    @IBOutlet var TFReciptNumber: SelfSerticeTextField!
    @IBOutlet var TFDateOfReceipt: SelfSerticeTextField!
    @IBOutlet var TFReceiptAmount: SelfSerticeTextField!
    @IBOutlet var TFPaymentMode: SelfSerticeTextField!
    @IBOutlet var tblView: UITableView!
    //@IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet var btnSaveAndAddNew: UIButton!
    
    var restManager = RestManager()
    var globalModelInvoiceMallList = GetInvoiceMallList()
    var modelNewRegistrationDetals = NewRegistrationDetails()
    var modelReceiptArray = [ReceiptArray]()
    //var globalStoreListArray = [StoreListaArray]()
    var globalPaymentListaArray = [PaymentListaArray]()
    
    var globalDateOfReceiptDisplayFormat = String()
    var globalDateOfReceiptAPIFormat = String()
    var globalMalllID = String()
    var globalStoreID = String()
    var globalPaymentMode = String()
    var isNeedHitInvoiceMallsList = false
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
        if globalPaymentListaArray.count == 0 {
            self.callAPIGetPaymentMethods()
        }
        
        /*
        if self.isMovingToParentViewController {
            
            callAPIGetInvoiceMallsList()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.3) {
                self.callAPIGetPaymentMethods()
            }
        }
        */
        
        /*
        if globalModelInvoiceMallList.ListInvoiceMall.count == 0{
            callAPIGetInvoiceMallsList()
        }
       
        if globalPaymentListaArray.count == 0 {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.3) {
                self.callAPIGetPaymentMethods()
            }
        }
         */
        
        //handlAPIsCall()
    }
    
    func initObj(){
    
        btnSaveAndAddNew.backgroundColor = SelfServiceColor().KLigthOrange
        btnSaveAndAddNew.titleLabel?.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        btnSaveAndAddNew.setTitleColor(SelfServiceColor().KWhite, for: .normal)
        btnSaveAndAddNew.layer.cornerRadius = 17
        btnSaveAndAddNew.clipsToBounds = true
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib(nibName: "TblViewCellReceiptEntry", bundle: nil), forCellReuseIdentifier: "TblViewCellReceiptEntry")
        
        TFReciptNumber.rightView = nil
        TFReceiptAmount.rightView = nil
        
        
        //handle defautl mall
        if (SINGLETON.currentActiveApproveCode != nil &&  SINGLETON.currentActiveApproveCode?.WorkingMall?.count ?? 0 > 0 && SINGLETON.currentActiveApproveCode?.MallID?.count ?? 0 > 0  ){
            
            TFSelectMall.text =  SINGLETON.currentActiveApproveCode?.WorkingMall ?? ""
            globalMalllID = SINGLETON.currentActiveApproveCode?.MallID ?? "-1"
            
        }
        
        //Hint useful date formats: kdisplayDateFormatWithoutTime, kDatabaseDateFormatWithOutTime
        //handle today date
        let currentUTCDateDisplayFormat = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kdisplayDateFormatWithoutTime)
        TFDateOfReceipt.text = currentUTCDateDisplayFormat
        globalDateOfReceiptDisplayFormat =  currentUTCDateDisplayFormat
        
        let currentUTCDateAPIFormat = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithOutTime)
        globalDateOfReceiptAPIFormat = currentUTCDateAPIFormat
        
        
        
        
    }
    /*
    func handlAPIsCall(){
        
        if (SINGLETON.currentActiveApproveCode == nil ||  SINGLETON.currentActiveApproveCode?.WorkingMall?.count == 0 ){
            
            callAPIGetInvoiceMallsList()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.callAPIGetPaymentMethods()
            }
        }else {
            callAPIGetPaymentMethods()
        }
    }
 */
    
    /*
    func showMallListPopupWithStringArray(){
        
        let mallListPopup = GenericStringArrayListPopup()
        mallListPopup.delegate=self
        mallListPopup.navigTitle = "Select Mall"
        mallListPopup.isComingFromSettingVC = true
        var stringArray  = [String]()
        stringArray.append(SINGLETON.currentActiveApproveCode?.WorkingMall ?? "")
        mallListPopup.stringsArray = stringArray
        let controller = UINavigationController(rootViewController: mallListPopup)
        controller.preferredContentSize = CGSize(width: 300, height: 200)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFSelectMall
            popover.sourceRect = TFSelectMall.bounds
            popover.permittedArrowDirections = .any
            self.present(controller, animated: true) {
            }
        }
    }
    */
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
            
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = kDatabaseDateFormatWithOutTime //"MM/dd/yyyy"
            
            // Apply date format
            //let selectedDate: String = dateFormatter.string(from: sender.date)
            
            //print("Selected value \(selectedDate)")
            //labelOne.text = selectedDate
        
            let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
            if let day = components.day, let month = components.month, let year = components.year {
                //labelTwo.text = "Day: \(day) Month: \(month) Year: \(year)"
                print("\n ** result: Day: \(day) Month: \(month) Year: \(year)  *\n")
                
            }
        
        }

    /*
       func createDatePicker(){
           
           datePicker.datePickerMode = .date
           datePicker.backgroundColor = .white
           datePicker.minuteInterval = 30
           datePicker.setValue(UIColor.black, forKey: "textColor")
           TFDateOfReceipt.inputView = datePicker
           
       }
       
       func createToolbar(){
           let toolBar = UIToolbar()
           toolBar.barStyle = .default
           toolBar.isTranslucent = true
           toolBar.sizeToFit()
           
           let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.datePicketDoneClicked))
           toolBar.setItems([doneButton], animated: false)
           toolBar.isUserInteractionEnabled = true
           TFDateOfReceipt.inputAccessoryView = toolBar
           
       }
       
      */
    
    
    //MARK: ACTION BUTTON
    /*
    @objc func datePicketDoneClicked() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = kdisplayDateFormatWithoutTime
        TFDateOfReceipt.text = dateFormatter.string(from: datePicker.date)
        TFDateOfReceipt.resignFirstResponder()
    }
    */
    @IBAction func btnAddClicked(_ sender: Any) {
       
        TFSelectMall.text =  TFSelectMall.text?.trimString()
        TFSelectStore.text =  TFSelectStore.text?.trimString()
        TFReciptNumber.text =  TFReciptNumber.text?.trimString()
        TFDateOfReceipt.text =  TFDateOfReceipt.text?.trimString()
        TFReceiptAmount.text =  TFReceiptAmount.text?.trimString()
        TFPaymentMode.text =  TFPaymentMode.text?.trimString()
        
        
        if TFSelectMall.text?.count == 0{
            view.makeToast("Select Mall", duration: 2.0, position: .center)
            TFSelectMall.shake()
            return
        }
        
        if TFSelectStore.text?.count == 0{
            view.makeToast("Select Store", duration: 2.0, position: .center)
            TFSelectStore.shake()
            return
        }
        if TFReciptNumber.text?.count == 0{
            view.makeToast("Enter Receipt Number", duration: 2.0, position: .center)
            TFReciptNumber.shake()
            return
        }
        
        if TFDateOfReceipt.text?.count == 0{
            view.makeToast("Select Receipt Date", duration: 2.0, position: .center)
            TFDateOfReceipt.shake()
            return
        }
        
        if TFReceiptAmount.text?.count == 0{
            view.makeToast("Enter Receipt Value", duration: 2.0, position: .center)
            TFReceiptAmount.shake()
            return
        }
        
        var isAmountIntOrFlot = false
        if let value =  Int(TFReceiptAmount.text!){
            
            isAmountIntOrFlot = true
            
            if value <= 0 {
                view.makeToast(KInvaidReceiptAmountMsg, duration: 2.0, position: .center)
                TFReceiptAmount.shake()
                return
            }else if value > 1000000{
                view.makeToast("Maximum Receipt Amount allowed is 10,00,000", duration: 2.4, position: .center)
                TFReceiptAmount.shake()
                return
            }
            
        }else if let value = Float(TFReceiptAmount.text!){
            
            isAmountIntOrFlot = true
            
            if value <= 0 {
                view.makeToast(KInvaidReceiptAmountMsg, duration: 2.0, position: .center)
                TFReceiptAmount.shake()
                return
            }else if value > 1000000{
                
                view.makeToast("Maximum Receipt Amount allowed is 10,00,000", duration: 2.4, position: .center)
                TFReceiptAmount.shake()
                return
            }
        }
        
        else if let value = Double(TFReceiptAmount.text!){
            
            isAmountIntOrFlot = true
            
            if value <= 0 {
                view.makeToast(KInvaidReceiptAmountMsg, duration: 2.0, position: .center)
                TFReceiptAmount.shake()
                return
            }else if value > 1000000{
                view.makeToast("Maximum Receipt Amount allowed is 10,00,000", duration: 2.4, position: .center)
                TFReceiptAmount.shake()
                return
            }
        }
        
        if isAmountIntOrFlot == false {
            view.makeToast(KInvaidReceiptAmountMsg, duration: 2.4, position: .center)
            TFReceiptAmount.shake()
            return
        }
        
        
        if TFPaymentMode.text?.count == 0{
            view.makeToast("Select Payment Mode", duration: 2.0, position: .center)
            TFPaymentMode.shake()
            return
        }
              
        //let model = ReceiptArray.init(ReceiptNumber: , MallName: TFSelectMall.text!, StoreName: TFSelectStore.text!, DateString: TFDateOfReceipt.text!, ReceiptAmount: TFReceiptAmount.text!)
        
        /*
         var InvoiceNumber: String?
         var MallName: String?
         var StoreName: String?
         var MallID: String?
         var StoreID: String?
         var InvoiceValue: String?
         var InvoiceDateStr: String?
         var PaymentModeStr: String?
       */
        
        
        var model = ReceiptArray()
        model.InvoiceNumber = TFReciptNumber.text!
        model.MallName = TFSelectMall.text!
        model.StoreName = TFSelectStore.text!
        model.MallID = globalMalllID
        model.StoreID = globalStoreID
        model.InvoiceValue = TFReceiptAmount.text!
        model.InvoiceDateStrAPIFormat = globalDateOfReceiptAPIFormat
        model.InvoiceDateStrDisplayFormat = globalDateOfReceiptDisplayFormat
        model.PaymentModeStr = globalPaymentMode
        
        print("\n\n ** added in ReceiptArray: \(model) **\n\n")
        
        modelReceiptArray.append(model)
        
        tblView.reloadData()
        
        //remove old existing text of text fields
        TFSelectStore.text =  ""
        TFReciptNumber.text =  ""
        TFReceiptAmount.text =  ""
        
        
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextClicked(_ sender: Any) {
        
        if modelReceiptArray.count > 0{
            
           let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("\n\n **** Net Not connected  ***\n\n")
                self.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
                return
                
            case .online(_): break
                
            }
            
            let newViewController = ReceiptConfirmationVC()
            newViewController.modelNewRegistrationDetals = modelNewRegistrationDetals
            newViewController.modelReceiptArray = modelReceiptArray
            print("\n\n Hint 1: model Registration: \(modelNewRegistrationDetals) **\n\n ")
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else {
            self.view.makeToast("No receipt added. Please add some receipts.", duration: 2.0, position: .center)
        }
        
    }
    
    @IBAction func btnProfileEditClicked(_ sender: Any) {
        let newViewController = UpdateCustomerVC()
        newViewController.delegate = self
        newViewController.modelNewRegistrationDetals = modelNewRegistrationDetals
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    
    @objc func btnDeleteClicked(sender: UIButton) {
        
        let alertController = UIAlertController(title: KCCSELFSERVICE, message: "Delete this invoice?", preferredStyle: .alert)
        let btnDelete = UIAlertAction(title: NSLocalizedString(KDelete, comment: ""), style: .default, handler: { action in
            self.modelReceiptArray.remove(at: sender.tag)
            self.tblView.reloadData()
        })
        
        let btnCancel = UIAlertAction(title: NSLocalizedString(KAlertCancelButtonTitle, comment: ""), style: .destructive, handler: { action in
        })
        
        let actionsArray = [btnCancel, btnDelete]
        showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
    }
    
    //MARK: -TEXT FIELD DELEGATE METHODS
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField ==  TFSelectMall {
            
            view.endEditing(true)
            TFSelectStore.text = ""
            showWorkingMallListPopupWithStringArray()
            
            /*
            if globalModelInvoiceMallList.ListInvoiceMall.count == 0{
                isNeedHitInvoiceMallsList = true
                callAPIGetInvoiceMallsList()
            }
            else {
                showMallListPopupWithModelArray(modelArray: globalModelInvoiceMallList)
            }
            */
            
            
            return false
        }
        else if textField ==  TFSelectStore {
             view.endEditing(true)
            
            if TFSelectMall.text?.count == 0 {
                view.makeToast("Please select Mall first", duration: 2.0, position: .center)
                return false
            }else {
                callAPIGetInvoiceStoresList()
                return false
            }
        }
        
        else if textField ==  TFDateOfReceipt {
            
            view.endEditing(true)
            
            let popup = PopupReciptEntryDatePicker()
            popup.delegate=self
            let controller = UINavigationController(rootViewController: popup)
            controller.preferredContentSize = CGSize(width: 330, height: 200)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFDateOfReceipt
                popover.sourceRect = TFDateOfReceipt.bounds
                popover.permittedArrowDirections = .down
                self.present(controller, animated: true) {
                }
            }
            return false
            
            /*
             datePicker.isHidden = false
             
             let calendar = Calendar(identifier: .gregorian)
             let comps = DateComponents()
             //comps.year = 30
             let maxDate = calendar.date(byAdding: comps, to: Date())
             //comps.year = -50
             //let minDate = calendar.date(byAdding: comps, to: Date())
             datePicker.maximumDate = maxDate
            // datePicker.minimumDate = minDate
             datePicker.datePickerMode =  .date
             datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: .valueChanged)
                   createDatePicker()
                   createToolbar()
            
            return false
            */
        }
        else if textField ==  TFPaymentMode {
             view.endEditing(true)
            if globalPaymentListaArray.count == 0{
                callAPIGetPaymentMethods()
            }else {
                showPaymentListPopup(paymentModeArray: globalPaymentListaArray)
            }
            
             return false
            
        }
        
        return true
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    // Allow only float number input
    if textField == TFReceiptAmount  {
        var allowedCharacters = ""
        allowedCharacters = ".0123456789"
        
        _ = (textField.text! as NSString)
            .replacingCharacters(in: range, with: string)
        let characterSet = CharacterSet(charactersIn: allowedCharacters)
        let range = string.rangeOfCharacter(from: characterSet)
        if range == nil && string != ""  {
            // print("\n\n ** letters found  **\n\n")
            return false
        }
        
       }
      return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    //MARK:- TABLE VIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 44
       }
       
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
           headerView.selectionStyle = .none
           headerView.backgroundColor = SelfServiceColor().KBlue
           //headerView.contentView.addTopandBottomBorder()
            viewParentCellLables.layer.cornerRadius = 18
            viewParentCellLables.layer.masksToBounds = false
           //headerView.contentView.clipsToBounds = true
           return headerView.contentView
       }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelReceiptArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblViewCellReceiptEntry", for: indexPath) as! TblViewCellReceiptEntry
        
        cell.viewParentLabels.layer.borderColor = SelfServiceColor().KGreyBorderColor.cgColor
        cell.viewParentLabels.layer.borderWidth = 2.0
        cell.viewParentLabels.layer.cornerRadius = 18
        cell.viewParentLabels.layer.masksToBounds = false
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.imageView?.contentMode = .scaleAspectFit
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked), for: .touchUpInside)
        
        let model = modelReceiptArray[indexPath.row]
        cell.lblReceipt.text = model.InvoiceNumber ?? ""
        cell.lblMallName.text = model.MallName ?? ""
        cell.lblStore.text = model.StoreName ?? ""
        cell.lblDate.text = model.InvoiceDateStrDisplayFormat ?? ""
        cell.lblValue.text = model.InvoiceValue ?? ""
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
            guard let indexPath = tableView.indexPathForSelectedRow  else { return }
            let model = modelReceiptArray[indexPath.row]
            TFSelectMall.text = model.MallName ?? ""
            TFSelectStore.text = model.StoreName ?? ""
            TFReciptNumber.text = model.InvoiceNumber ?? ""
            TFDateOfReceipt.text = model.InvoiceDateStrDisplayFormat ?? ""
            TFReceiptAmount.text = model.InvoiceValue ?? ""
            
            modelReceiptArray.remove(at: indexPath.row)
            
            tblView.reloadData()
       }
}

//MARK: API CALL METHODS
extension ReceiptEntryVC{
    
    /*
    func callAPIGetInvoiceMallsList(){
        
        /*
         input params:
         RequestTimeStamp
         AppKey
         TokenID
         
         format > "RequestTimeStamp=""&AppKey=""
         */
        
        
        /*
         guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLSettings_GetInvoiceMallsList)") else { return }
         restManager.urlQueryParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
         restManager.urlQueryParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
         restManager.urlQueryParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        let HMAC_sha256_Value = "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID: HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLSettings_GetInvoiceMallsList)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let mallListModel = try? decoder.decode(GetInvoiceMallList.self, from: data){
                    
                    print("\n\n ***** API respone Model:\(mallListModel.description) *****\n\n")
                    self.handleAPIInvoiceMallsListRespone(mallListModel:mallListModel)
                    
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                }
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                
            }
        }
    }
    
    func handleAPIInvoiceMallsListRespone(mallListModel:GetInvoiceMallList){
        
        if mallListModel.ResponseCode == KAPIResSuccess{
            
            globalModelInvoiceMallList = mallListModel
            
            if isNeedHitInvoiceMallsList{
                showMallListPopupWithModelArray(modelArray: globalModelInvoiceMallList)
                isNeedHitInvoiceMallsList = false
            }
            
            
        }else {
             showAlertOnMainThread(title: KCCSELFSERVICE, msg: mallListModel.Result ?? KSomethingWentWrongTryAgainLater)
        }
    }
     */
    /*
    func showMallListPopupWithModelArray(modelArray:GetInvoiceMallList){
        
        //remove not usefult index from API Respone array
        if let matchedOffset = globalModelInvoiceMallList.ListInvoiceMall.index(where: {$0.MallName == K_PleaseSelect_}) {
            globalModelInvoiceMallList.ListInvoiceMall.remove(at: matchedOffset)
        }
        
        if globalModelInvoiceMallList.ListInvoiceMall.count == 0 {
            view.makeToast("Mall list not available, Please try again later.", duration: 2.0, position: .center)
            return
        }
        
        DispatchQueue.main.async {
            let popup = GenericModelArrayListPopup()
            popup.delegate=self
            popup.navigTitle = "Select Mall"
            popup.isNeedsShowMallList = true
            popup.contentArray =  self.globalModelInvoiceMallList.ListInvoiceMall as [AnyObject]
            let controller = UINavigationController(rootViewController: popup)
            controller.preferredContentSize = CGSize(width: 280, height: 300)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = self.TFSelectMall
                popover.sourceRect = self.TFSelectMall.bounds
                popover.permittedArrowDirections = .left
                self.present(controller, animated: true) {
                }
            }
        }
    }
    */
    
    
    func showWorkingMallListPopupWithStringArray(){
        
        let mallListPopup = GenericStringArrayListPopup()
        mallListPopup.delegate=self
        mallListPopup.navigTitle = "Select Mall"
        mallListPopup.isComingFromSettingVC = true
        var stringArray  = [String]()
        stringArray.append(SINGLETON.currentActiveApproveCode?.WorkingMall ?? "")
        mallListPopup.stringsArray = stringArray
        let controller = UINavigationController(rootViewController: mallListPopup)
        controller.preferredContentSize = CGSize(width: 300, height: 200)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFSelectMall
            popover.sourceRect = TFSelectMall.bounds
            popover.permittedArrowDirections = .down
            self.present(controller, animated: true) {
            }
        }
    }
    
    
    
    func callAPIGetInvoiceStoresList(){
           
           /*
            input params:
         https://testecoupon.maf.ae/svc/svcHifi.svc/GetInvoiceStoresList?MallID=1&RequestTimeStamp=2020-04-21%2011:43:25&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a&TokenID=q6WKbWotlJucg5RAszaDuPicwhuiX4rBGLqLd3233oM%3D
         
         
         https://testecoupon.maf.ae/svc/svcHifi.svc/GetInvoiceStoresList?MallID=1&RequestTimeStamp=2020-04-29 05:28:25&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a&TokenID=
         
             MallID
             RequestTimeStamp
             AppKey
         
            Format TokenID:
            MallID=1&RequestTimeStamp=2020-04-29 05:28:25&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         
           */
           
          
           let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
         //let HMAC_sha256_Value = "\(KMallID)=\("2")\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        
        let HMAC_sha256_Value = "\(KMallID)=\(globalMalllID)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
           
           let queryParams: [String: String] = [
              
               KMallID : globalMalllID,
               KRequestTimeStamp: currentDateTimeInUTC,
               KAppKey : KAppKeyDefaultTestApp,
               KTokenID: HMAC_sha256_Value
           ]
           
           var urlComponents = URLComponents()
           urlComponents.setQueryItems(with: queryParams)
        
         var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLSettings_GetInvoiceStoresList)" + (urlComponents.url?.absoluteString ?? "")
           
        completeURLString = completeURLString.stringReplacePlusSign()
        
        /*
        working
        completeURLString = "https://testecoupon.maf.ae/svc/svcHifi.svc/GetInvoiceStoresList?MallID=2&RequestTimeStamp=2020-04-30%2008:43:13&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a&TokenID=HEFH4OXHZWQ6LE8LPLVRC6ZTBGFGFGJOXQEIPFBXPTI%3D"
            */
           
           guard let finalURL = URL(string:completeURLString) else {return}
           
           print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
           Indicator.shared.showProgressView(self.view)
           
           restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
               
               Indicator.shared.hideProgressView()
               
               guard let response = results.response else { return }
               if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                   guard let data = results.data else { return }
                   let decoder = JSONDecoder()
                   if let storeListModel = try? decoder.decode(StoreList.self, from: data){
                       print("\n\n ***** API respone Model:\(storeListModel.description) *****\n\n")
                       self.handleAPIGetInvoiceStoresListRespone(storeListModel:storeListModel)
                    
                    
                   }else {
                       data.handleAPIParsingErrorCase(currentVC: self)
                   }
               }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                   
               }
           }
       }
       
    
    func handleAPIGetInvoiceStoresListRespone(storeListModel:StoreList){
        
        if storeListModel.ResponseCode == KAPIResSuccess{
            
            //remove not usefult index from API Respone array
            var storeArrayLocal = storeListModel.ListInvoiceStores
            if let matchedOffset = storeListModel.ListInvoiceStores.index(where: {$0.StoreName == K_PleaseSelect_}) {
                storeArrayLocal.remove(at: matchedOffset)
            }
            
            if storeArrayLocal.count == 0{
                self.view.makeToast("Store list is not available, please try again later", duration: 2.0, position: .center)
                
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    //self.globalStoreListArray = storeArrayLocal
                    self.showStoreListPopup(storeArray:storeArrayLocal)
                }
            }
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: storeListModel.Result ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    
    func showStoreListPopup(storeArray:[StoreListaArray]){
        
        
        //remove not usefult index from API Respone array
        var tempArray = storeArray
        if let matchedOffset = storeArray.index(where: {$0.StoreName == K_PleaseSelect_}) {
            tempArray.remove(at: matchedOffset)
        }
        
        if tempArray.count == 0{
           self.view.makeToast("Store list not available, Please try again later.", duration: 2.0, position: .center)
            return
        }
        
        let popup = GenericModelArrayListPopup()
        popup.delegate=self
        popup.navigTitle = "Select Store"
        popup.isShowStoreList = true
        popup.contentArray = tempArray as [AnyObject]
        let controller = UINavigationController(rootViewController: popup)
        controller.preferredContentSize = CGSize(width: 351, height: 450)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFSelectStore
            popover.sourceRect = TFSelectStore.bounds
            popover.permittedArrowDirections = .right
            self.present(controller, animated: true) {
            }
        }
        
    }
    
    
     func callAPIGetPaymentMethods(){
              
              /*
               input params:
            https://testecoupon.maf.ae/svc/svcHifi.svc/GetPaymentMethods?RequestTimeStamp=2020-04-21%2011:20:33&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a&TokenID=z%2FB9RaIP7GWPXwajVIRCBl8%2FhW%2FWUE0nQgYEKQkrIkw%3D
         
             RequestTimeStamp
             AppKey
            
              */
              
             
              let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
             let HMAC_sha256_Value = "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
              
              let queryParams: [String: String] = [
                  KRequestTimeStamp: currentDateTimeInUTC,
                  KAppKey : KAppKeyDefaultTestApp,
                  KTokenID: HMAC_sha256_Value
              ]
              
              var urlComponents = URLComponents()
              urlComponents.setQueryItems(with: queryParams)
              
              var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetPaymentMethods)" + (urlComponents.url?.absoluteString ?? "")
              
              completeURLString = completeURLString.stringReplacePlusSign()
              
              
              guard let finalURL = URL(string:completeURLString) else {return}
              
              print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
              
              Indicator.shared.showProgressView(self.view)
              
              restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
                  
                  Indicator.shared.hideProgressView()
                  
                  guard let response = results.response else { return }
                  if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                      guard let data = results.data else { return }
                      let decoder = JSONDecoder()
                      if let modelPaymentList = try? decoder.decode(PaymentList.self, from: data){
                          
                          print("\n\n ***** API respone Model:\(modelPaymentList.description) *****\n\n")
                          
                       self.handleAPIGetPaymentMethodsRespone(modelPaymentList:modelPaymentList)
                       
                      }else {
                         data.handleAPIParsingErrorCase(currentVC: self)
                      }
                  }else {
                     
                    self.showAlertOnMainThread(title: KCCSELFSERVICE, msg:KSomethingWentWrongTryAgainLater )
                  }
              }
          }
          
       
       func handleAPIGetPaymentMethodsRespone(modelPaymentList:PaymentList){
              
        if modelPaymentList.ResponseCode == KAPIResSuccess{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                //remove not usefult index from API Respone array
                var arrayTemp = modelPaymentList.ListPaymentModeContract
                if let matchedOffset = modelPaymentList.ListPaymentModeContract.index(where: {$0.Description == K_PleaseSelect_}) {
                    arrayTemp.remove(at: matchedOffset)
                }
                self.globalPaymentListaArray = arrayTemp
                
                if self.globalPaymentListaArray.count > 0{
                    self.TFPaymentMode.text = self.globalPaymentListaArray.first?.Description ?? ""
                    self.globalPaymentMode = self.globalPaymentListaArray.first?.Description ?? ""
                }
                
                
                //self.showPaymentListPopup(cityArray:modelPaymentList.ListPaymentModeContract)
            }
            
         }else {
               showAlertOnMainThread(title: KCCSELFSERVICE, msg:modelPaymentList.Result ?? KSomethingWentWrongTryAgainLater )
              }
          }
    
          
         func showPaymentListPopup(paymentModeArray:[PaymentListaArray]){
             
             
            //remove not usefult index from API Respone array
            var tempArray = paymentModeArray
            if let matchedOffset = paymentModeArray.index(where: {$0.Description == K_PleaseSelect_}) {
                tempArray.remove(at: matchedOffset)
            }
            
            if tempArray.count == 0{
               self.view.makeToast("Payment list not available, Please try again later.", duration: 2.0, position: .center)
               return
            }
            
             let popup = GenericModelArrayListPopup()
             popup.delegate=self
             popup.navigTitle = "Select Payment Mode"
             popup.isComingReceiptEntry = true
             popup.contentArray = tempArray as [AnyObject]
             
             let controller = UINavigationController(rootViewController: popup)
             controller.preferredContentSize = CGSize(width: 351, height: 300)
             controller.modalPresentationStyle = UIModalPresentationStyle.popover
             if let popover = controller.popoverPresentationController  {
                 popover.delegate = self
                 popover.sourceView = TFPaymentMode
                 popover.sourceRect = TFPaymentMode.bounds
                 popover.permittedArrowDirections = .left
                 self.present(controller, animated: true) {
                 }
             }
             
         }
         
    
}


//MARK: LIST POP UP DELEGATE METHOD | MODEL ARRAY
extension ReceiptEntryVC :GenericModelArrayListPopupDelegate {
    func activeCampaignMallDidSelect(modelActiveCampaignMall: ArrayActiveCampaignsByMall) {
        
    }
    
     func storeDidSelect(modelStore: StoreListaArray) {
         TFSelectStore.text = modelStore.StoreName ?? ""
         globalStoreID = String(modelStore.StoreID ?? -1)
    }
    
    func paymentModeSelectedPayment(modelPayment: PaymentListaArray) {
        TFPaymentMode.text = modelPayment.Description ?? ""
        globalPaymentMode = modelPayment.Description ?? ""
    }
    
    func nameTitlePopupDidSelectedTitle(index: NSInteger) {
           
          print("\n\n **** row number: \(index)   ***\n\n")
        if index < globalModelInvoiceMallList.ListInvoiceMall.count {
            
            let model = globalModelInvoiceMallList.ListInvoiceMall[index]
            TFSelectMall.text = model.MallName ?? ""
            globalMalllID = String(model.MallID ?? -1)
               
           }else {
                 self.view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
            }
         
       }
    
    func countryListDidSelectCountry(rowNumber: NSInteger) {
        
    }
    
    func mallPopupDidSelectedMall(selectedMall: InvoiceMallList) {
        
    }
    
   
    
    func residentialStatusSelected(model: ResidentialStatusArray) {
        
    }
    
    func nationlitySelected(index: NSInteger) {
        
    }
    
    func residenceCounrySelected(index: NSInteger) {
        
    }
    
    func cityEmirateSelected(modelCity: CityListaArray) {
        
    }
    
    func areaSelected(modelArea: AreaListaArray) {
        
    }
}

//MARK: DATE PICKER DELEGATE METHOD
extension ReceiptEntryVC :PopupReciptEntryDatePickerDelegate {
    
    func datePickerValueDidChange(selectedDate: Date) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = kDatabaseDateFormatWithOutTime
        globalDateOfReceiptAPIFormat = dateFormatter.string(from: selectedDate)
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = kdisplayDateFormatWithoutTime
        TFDateOfReceipt.text = dateFormatter.string(from: selectedDate)
        globalDateOfReceiptDisplayFormat =  dateFormatter.string(from: selectedDate)
        TFDateOfReceipt.resignFirstResponder()
    }
    


}

//MARK: LIST POP UP DELEGATE METHOD | STRING ARRAY
extension ReceiptEntryVC :GenericStringArrayListPopupDelegate {
    
    func mallDidSelectWorkingMall() {
        print("\n\n **** mallDidSelectWorkingMall  ***\n\n")
        if (SINGLETON.currentActiveApproveCode != nil &&  SINGLETON.currentActiveApproveCode?.WorkingMall?.count ?? 0 > 0 ){
            TFSelectMall.text = SINGLETON.currentActiveApproveCode?.WorkingMall ?? ""
            globalMalllID = SINGLETON.currentActiveApproveCode?.MallID ?? "-1"
        }
    }
}

//MARK: UPDATE CUSTOMER VC DELEGATE
extension ReceiptEntryVC : UpdateCustomerVCDelegate {
   
    func passUpdatedCustModel(updatedCustomerModel: NewRegistrationDetails) {
         modelNewRegistrationDetals = updatedCustomerModel
    }
}
