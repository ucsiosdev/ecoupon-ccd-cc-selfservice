//
//  MobileNumberAndShareIDVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 20/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class MobileNumberAndShareIDVC: BaseViewController,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet var viewCustomerFound: UIView!
    @IBOutlet var lblCustomerFoundName: UILabel!
    @IBOutlet var btnShareIDSearch: UIButton!
    @IBOutlet var btnMobileNumberSearch: UIButton!
    @IBOutlet var TFDialCode: SelfSerticeTextField!
    @IBOutlet var TFMobileNumber: SelfSerticeTextField!
    @IBOutlet var TFShareID: SelfSerticeTextField!
    
    var globalCoundyDial = String()
    let restManager = RestManager()
    var gloablModelCustomer = Customer()
    private var arrayCountryList: [Country] {
        let countries = CountriesDetails()
        let countryList = countries.countries
        return countryList
    }
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        setupView()
        
        
    }
    
    //MARK: HELPER METHOD
    func initObj(){
        
        //for handling cornerRadius & look better adding space in right side
        lblCustomerFoundName.text = "N/A                  "
        TFMobileNumber.rightView = nil
        TFShareID.rightView = nil
        
        viewCustomerFound.isHidden = true
        
    }
    
    func setupView(){
        
        btnShareIDSearch.layer.cornerRadius = 14
        btnShareIDSearch.clipsToBounds = true
        
        btnMobileNumberSearch.layer.cornerRadius = 14
        btnMobileNumberSearch.clipsToBounds = true
        
        btnShareIDSearch.imageView?.contentMode = .scaleAspectFit
        btnMobileNumberSearch.imageView?.contentMode = .scaleAspectFit
        
        lblCustomerFoundName.layer.cornerRadius = 24
        lblCustomerFoundName.clipsToBounds = true
        lblCustomerFoundName.layer.masksToBounds = true
        
        viewCustomerFound.backgroundColor = UIColor.clear
        
        
    }
    
    //MARK: -TEXT FIELD DELEGATE METHODS
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField ==  TFDialCode {
            view.endEditing(true)
            
            if arrayCountryList.count == 0{
                self.view.makeToast("Country list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            let countryListPopup = GenericModelArrayListPopup()
            countryListPopup.delegate=self
            countryListPopup.navigTitle = "Select Country"
            countryListPopup.searchBarPlaceHolderStr = "Search Countries"
            countryListPopup.contentArray = arrayCountryList
            countryListPopup.isComingFromMobileNumberAndShareIDVC = true
            let controller = UINavigationController(rootViewController: countryListPopup)
            controller.preferredContentSize = CGSize(width: 350, height: 450)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFDialCode
                popover.sourceRect = TFDialCode.bounds
                popover.permittedArrowDirections = .left
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
        else if textField ==  TFMobileNumber {
            view.endEditing(true)
            
            return true
        }
            
        else if textField ==  TFShareID {
            view.endEditing(true)
            return true
        }
        
        return true
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Allow only positive numerical input for mobile number
        if textField == TFMobileNumber  {
            var allowedCharacters = ""
            allowedCharacters = "0123456789"
            if textField.text?.count ?? 0  > 14 && string != "" {
                return false
            }
            
            _ = (textField.text! as NSString)
                .replacingCharacters(in: range, with: string)
            let characterSet = CharacterSet(charactersIn: allowedCharacters)
            let range = string.rangeOfCharacter(from: characterSet)
            if range == nil && string != ""  {
                // print("\n\n ** letters found  **\n\n")
                return false
            }
            
        }
            //ShareID must be 16 digit and also must only number
        else if textField == TFShareID  {
            var allowedCharacters = ""
            allowedCharacters = "0123456789"
            if textField.text?.count ?? 0  > 15 && string != "" {
                return false
            }
            
            _ = (textField.text! as NSString)
                .replacingCharacters(in: range, with: string)
            let characterSet = CharacterSet(charactersIn: allowedCharacters)
            let range = string.rangeOfCharacter(from: characterSet)
            if range == nil && string != ""  {
                // print("\n\n ** letters found  **\n\n")
                return false
            }
        }
        return true
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnYesCustomerFoundCicked(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            print("\n\n **** Net Not connected  ***\n\n")
            Indicator.shared.hideProgressView()
            self.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
            return
            
        case .online(_): break
            
        }
        
        
        let newViewController = ReceiptEntryVC()
        var modelRegistationDetails = NewRegistrationDetails()
        modelRegistationDetails.TitleCode = gloablModelCustomer.Title ?? ""
        modelRegistationDetails.Title = gloablModelCustomer.Title ?? ""
        modelRegistationDetails.FirstName = gloablModelCustomer.FirstName ?? ""
        modelRegistationDetails.LastName = gloablModelCustomer.LastName ?? ""
        modelRegistationDetails.CustName = gloablModelCustomer.CustName ?? ""
        modelRegistationDetails.CustomerID = gloablModelCustomer.CustID ?? ""
        modelRegistationDetails.ResidentialStatusID = gloablModelCustomer.ResidenceStatus ?? ""
        modelRegistationDetails.NationalityID = gloablModelCustomer.Nationality ?? ""
        modelRegistationDetails.CountryResidenceID = gloablModelCustomer.ResidenceCountry ?? ""
        modelRegistationDetails.DialCode = gloablModelCustomer.CountryCode ?? ""
        modelRegistationDetails.MobileNumber = gloablModelCustomer.MobileNo ?? ""
        modelRegistationDetails.ShareId = gloablModelCustomer.ShareID ?? ""
        modelRegistationDetails.EmailAddress = gloablModelCustomer.Email ?? ""
        newViewController.modelNewRegistrationDetals = modelRegistationDetails
        self.navigationController?.pushViewController(newViewController, animated: true)
        
        
       // self.view.makeToast("Next screen name: Receipt Entry in progress.", duration: 2.0, position: .center)
    }
    
    @IBAction func btnNoCustomerFoundClicked(_ sender: Any) {
        
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            print("\n\n **** Net Not connected  ***\n\n")
            self.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
            return
            
        case .online(_): break
            
        }
        
        let newViewController = NewRegistrationVC()
        //let model = NewRegistrationDetails.init(Title: "", FirstName: "", LastName: "", ResidentialStatus: "", Nationality: "", ResidentialCountry: "", CityEmirate: "", AreaOfResidence: "", CountryCode:"", DialCode:globalCoundyDial, MobileNumber: TFMobileNumber.text, ShareId: TFShareID.text, EmailAddress: "")
        
        var model = NewRegistrationDetails()
        model.DialCode = globalCoundyDial
        model.MobileNumber = TFMobileNumber.text
        model.ShareId = TFShareID.text
       
        newViewController.modelNewRegistrationDetals = model
        self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchMobileClicked(_ sender: Any) {
        
        
        viewCustomerFound.isHidden = true
        
        // internation mobile number : min 5 max 15
        
        //Validation part
        
        TFDialCode.text =  TFDialCode.text?.trimString()
        TFMobileNumber.text =  TFMobileNumber.text?.trimString()
        
        if TFDialCode.text?.count == 0{
            view.makeToast("Please select country code", duration: 2.0, position: .center)
            TFDialCode.shake()
            return
        }
        else if TFMobileNumber.text?.count ?? 0 < 5 || TFMobileNumber.text?.count ?? 0 > 15 {
            view.makeToast(KMobilenNumberLengthError, duration: 2.0, position: .center)
             TFMobileNumber.shake()
            return
        }
            
        //validation suceess now hit api
        else{
            view.endEditing(true)
            callAPIFindCustomerForMobileNumber()
        }
        
    }
    
    @IBAction func btnSearchShareIDClicked(_ sender: Any) {
        
        viewCustomerFound.isHidden = true
        
        TFShareID.text =  TFShareID.text?.trimString()
        
        if TFShareID.text == nil || TFShareID.text?.count ?? 0 < 16 {
            view.makeToast("Share ID must be 16 digits", duration: 2.0, position: .center)
            TFShareID.shake()
            return
        }else {
            view.endEditing(true)
            callAPIFindCustomerForShareID()
        }
        
    }
    
    func callAPIFindCustomerForMobileNumber(){
        
        /*
         MobileNo:9188888888
         ShareID:
         RequestTimeStamp:2020-04-21 09:45:08
         AppKey:3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         TokenID:qMHkuBYOUIobDUz8P7QZ66NG6jel7AYel7xyCbxCx6Y=
         
         */
        
        /*
         let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
         
         guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLFindCustomer)") else { return }
         
         let mobileNumberWithCode = "\(globalCoundyDial)\(TFMobileNumber.text!)"
         
         // Warning: do not run below line for GET API, else GET API will not work. It is only for POST API.
         
         //restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
         
         restManager.urlQueryParameters.add(value:mobileNumberWithCode , forKey: KMobileNo)
         restManager.urlQueryParameters.add(value: "", forKey: KShareID)
         restManager.urlQueryParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
         restManager.urlQueryParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
         
         let HMAC_sha256_Value = "\(KMobileNo)=\(mobileNumberWithCode)&\(KShareID)=\("")&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
         
         restManager.urlQueryParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        let mobileNumberWithCode = "\(globalCoundyDial)\(TFMobileNumber.text!)"
        
        let HMAC_sha256_Value = "\(KMobileNo)=\(mobileNumberWithCode)&\(KShareID)=\("")&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KMobileNo: mobileNumberWithCode,
            KShareID : "",
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID :HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLFindCustomer)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        
        /* Hint (lldb) po results.error?.localizedDescription
         ▿ Optional<String>
         - some : "The request timed out." */
        
        Indicator.shared.showProgressView(self.view)
        
        restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelCustomer = try? decoder.decode(Customer.self, from: data){
                    print("\n\n ***** API respone Model:\(modelCustomer.description) *****\n\n")
                    //avoid crash UI task must done on main thread.
                    DispatchQueue.main.async(execute: {
                        self.handleAPISearchCustomerRespone(modelCustomer:modelCustomer)
                    })
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                    
                }
            }else {
                
              self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    
    func callAPIFindCustomerForShareID(){
        
        /*
         MobileNo:
         ShareID: 6646464646333333 OR 1234567890123456
         RequestTimeStamp:2020-04-21 09:45:08
         AppKey:3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         TokenID:qMHkuBYOUIobDUz8P7QZ66NG6jel7AYel7xyCbxCx6Y=
         */
        
        /*
         let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
         
         guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLFindCustomer)") else { return }
         
         // Warning: do not run below line for GET API, else GET API will not work. It is only for POST API.
         
         //restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
         
         restManager.urlQueryParameters.add(value:"" , forKey: KMobileNo)
         restManager.urlQueryParameters.add(value: TFShareID.text!, forKey: KShareID)
         restManager.urlQueryParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
         restManager.urlQueryParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
         let HMAC_sha256_Value = "\(KMobileNo)=\("")&\(KShareID)=\(TFShareID.text!)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
         restManager.urlQueryParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        let HMAC_sha256_Value = "\(KMobileNo)=\("")&\(KShareID)=\(TFShareID.text!)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KMobileNo: "",
            KShareID : TFShareID.text!,
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey:KAppKeyDefaultTestApp,
            KTokenID:HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLFindCustomer)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelCustomer = try? decoder.decode(Customer.self, from: data){
                    print("\n\n ***** API respone Model:\(modelCustomer.description) *****\n\n")
                    //avoid crash UI task must done on main thread.
                    DispatchQueue.main.async(execute: {
                        self.handleAPISearchCustomerRespone(modelCustomer:modelCustomer)
                    })
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                
                self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    
    func handleAPISearchCustomerRespone(modelCustomer:Customer){
        
        viewCustomerFound.isHidden = true
        
        let customerID = String.getValidStringValue(string: modelCustomer.CustID ?? "")
        let processRespone = String.getValidStringValue(string: modelCustomer.ProcessResponse ?? "")
        
        //case: record found
        if modelCustomer.ResponseCode == KAPIResSuccess  && customerID.count > 0 {
            print("\n\n ***  case 1: Customer record found  *** ")
            gloablModelCustomer = modelCustomer
            if let name = modelCustomer.CustName{
                lblCustomerFoundName.text = "\(name)       "
            }
            
            viewCustomerFound.isHidden = false
        }
            //case: record not found
        else if  modelCustomer.ResponseCode == KAPIResFailed && processRespone == "Record not found" {
            
            print("\n\n ***  case 2: Customer record NOT found  *** ")
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("\n\n **** Net Not connected  ***\n\n")
                self.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
                return
                
            case .online(_): break
                
            }
            
            let newViewController = NewRegistrationVC()
            
            //let model = NewRegistrationDetails.init(Title: "", FirstName: "", LastName: "", ResidentialStatus: "", Nationality: "", ResidentialCountry: "", CityEmirate: "", AreaOfResidence: "", CountryCode: "",DialCode: globalCoundyDial, MobileNumber: TFMobileNumber.text, ShareId: TFShareID.text, EmailAddress: "")
            
            
            var model = NewRegistrationDetails()
            model.DialCode = globalCoundyDial
            model.MobileNumber =  TFMobileNumber.text
            model.ShareId =  TFShareID.text
            
            newViewController.modelNewRegistrationDetals = model
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }
            //case: something went wrong
        else {
            print("\n\n ***  case 3: Error during API hit *** ")
            showAlertOnMainThread(title: KCCSELFSERVICE, msg:modelCustomer.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            
        }
        
    }
}


//MARK: COUNTRY LIST POP UP DELEGATE METHOD
extension MobileNumberAndShareIDVC :GenericModelArrayListPopupDelegate {
    
    
    func activeCampaignMallDidSelect(modelActiveCampaignMall: ArrayActiveCampaignsByMall) {
        
    }
    
   
    
    func storeDidSelect(modelStore: StoreListaArray) {
        
    }
    
    
    func paymentModeSelectedPayment(modelPayment: PaymentListaArray) {
        
    }
    
    func countryListDidSelectCountry(rowNumber: NSInteger) {
        
        print("\n\n **** row number: \(rowNumber)   ***\n\n")
        if rowNumber < arrayCountryList.count{
            TFDialCode.text = "+\(arrayCountryList[rowNumber].phoneExtension)"
            globalCoundyDial = arrayCountryList[rowNumber].phoneExtension
        }else {
            view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
        }
    }
    
    func areaSelected(modelArea: AreaListaArray) {
        
    }
    func cityEmirateSelected(modelCity: CityListaArray) {
        
    }
    func nationlitySelected(index: NSInteger) {
        
    }
    
    func residenceCounrySelected(index: NSInteger) {
        
    }
    
    
    func residentialStatusSelected(model: ResidentialStatusArray) {
        
    }
    func nameTitlePopupDidSelectedTitle(index: NSInteger) {
        
    }
    func mallPopupDidSelectedMall(selectedMall: InvoiceMallList) {
        
    }
}
