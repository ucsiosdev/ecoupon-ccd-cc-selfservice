//
//  StartVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 14/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class StartVC: BaseViewController {
    
    @IBOutlet var lblAppServerAndVersion: UILabel!
    @IBOutlet var lblTapStart: UILabel!
    @IBOutlet var btnTokenMgt: UIButton!
    let restManager = RestManager()
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK: HELPER METHODS
    func setupView(){
        
        //btnTokenMgt.layer.cornerRadius = 20
        //btnTokenMgt.clipsToBounds = true
        lblTapStart.text  = "Tap Start to Participate in\n the Campaign"
        lblTapStart.lineBreakMode = .byWordWrapping
        
        if let strVersion = SelfServiceDefaults.retrieveDefaults.getAppVersionWithBuild(){
            
            if KCurrentAPIBaseURL.contains("testecoupon") {
                lblAppServerAndVersion.text = "Test v\(String(describing: strVersion))"
            }else {
                lblAppServerAndVersion.text = "Live v\(String(describing: strVersion))"
            }
        }
    }
    
    func getApproveCodeFromUser(){
        
        
        let alertController = UIAlertController(title: KCCSELFSERVICE, message: KEneterYourApporovalCodeMsg, preferredStyle: .alert)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            //textField.placeholder = "Enter Second Name"
             textField.isSecureTextEntry = true
        }
        
        let btnProceed = UIAlertAction(title: NSLocalizedString(KProceed, comment: ""), style: .default, handler: { action in
            
            
            if let alertTextField = alertController.textFields?.first, alertTextField.text != nil {
                
                if ( alertTextField.text == nil || alertTextField.text?.count == 0){
                    self.view.makeToast("Please enter your Approval Code, and try again", duration: 2.0, position: .center)
                }else {
                    self.callAPIValidateApprovalCode(approvalCode:alertTextField.text!)
                }
            }
        })
        
        
        let btnCancel = UIAlertAction(title: NSLocalizedString(KAlertCancelButtonTitle, comment: ""), style: .destructive, handler: { action in
            
        })
        
        let actionsArray = [btnCancel, btnProceed]
        showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
        
    }
    
    
    //MARK: ACTION BUTTON
    @IBAction func btnStartClicked(_ sender: Any) {
        
        if SINGLETON.currentActiveApproveCode == nil {
            getApproveCodeFromUser()
        }else {
            let newViewController = MobileNumberAndShareIDVC()
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    @IBAction func btnTokenMgtClicked(_ sender: Any) {
         let newViewController = TokenVerificationVC()
         self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func btnSettingsClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: KCCSELFSERVICE, message: "Enter your password to change Settings", preferredStyle: .alert)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
              //textField.text = "a"
              textField.isSecureTextEntry = true

        }
        
        let btnProceed = UIAlertAction(title: NSLocalizedString(KProceed, comment: ""), style: .default, handler: { action in
            
            if let alertTextField = alertController.textFields?.first, alertTextField.text != nil {

                if alertTextField.text == KSettingsPasswordDefault{
                    let newViewController = SettingsVC()
                    self.navigationController?.pushViewController(newViewController, animated: true)
                }
                else {
                    self.view.makeToast("Wrong password, Please try again.")
                }
            }
                   
        })
                   
                   
        let btnCancel = UIAlertAction(title: NSLocalizedString(KAlertCancelButtonTitle, comment: ""), style: .destructive, handler: { action in
                                  
        })
                   
        let actionsArray = [btnCancel, btnProceed]
         // showConfirmationAlert(alertTitle: KCCSELFSERVICE, alertMessage: "Enter your password to change Settings", actionsArray: actionsArray)
        showConfirmationAlertWithTextFiled(alertController: alertController,actionsArray: actionsArray)
     
    }
}

//MARK: API CALL METHODS
extension StartVC {
    
    func callAPIValidateApprovalCode(approvalCode:String){
        
        /*
         Excellent article for learn / demo for API
         https://www.appcoda.com/restful-api-library-swift/
         
         let HMAC_sha256_Value = "ApprovalCode=NS546&RequestTimeStamp=2020-04-17 12:04:33&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a".hmac(algorithm: .sha256, key: "1ae62e36396109bf379788556159525c1a3afa70c67ea5fb5b1a63c7f7484573")
         
         Base64 result via online site which is tested in working: Site: https://www.devglan.com/online-tools/hmac-sha256-online
         For example:
         input: ApprovalCode=NS546&RequestTimeStamp=2020-04-17 12:04:33&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         Output (Base64) : Oqzps6tAaEo3LtcWnNOl9xC7jKEYcqNcS96ApuGUF1Y=
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLSettings_ValidateApprovalCode)") else { return }
        
        restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        restManager.httpBodyParameters.add(value: approvalCode, forKey: KApprovalCode)
        restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        
        let HMAC_sha256_Value = "\(KApprovalCode)=\(approvalCode)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        
        restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let currentApprovalCode = try? decoder.decode(CurrentActiceApprovalCode.self, from: data){
                    print("\n\n ***** API respone Model:\(currentApprovalCode.description) *****\n\n")
                    self.handleAPIApprovalCodeRespone(currentApprovalCode:currentApprovalCode)
                }else {
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
               
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                
            }
        }
    }
    
    
    func handleAPIApprovalCodeRespone(currentApprovalCode:CurrentActiceApprovalCode){
        
        if currentApprovalCode.ResponseCode == KAPIResSuccess{
            
            // Save API respone in UserDefaults & SINGLETON for future use
            let encoder = JSONEncoder()
            if let userData = try? encoder.encode(currentApprovalCode){
                UserDefaults.standard.set(userData, forKey: UserDefaultKeys.CURRENT_ACTIVE_APPROVE_CODE_DETAILS)
                SINGLETON.currentActiveApproveCode = currentApprovalCode
            }else {
                print("\n\n **** WARNING: Error during encoding ****\n\n")
            }
            print("\n\n  *** SINGLETON.currentActiveApproveCode: \(SINGLETON.currentActiveApproveCode ??  CurrentActiceApprovalCode())   ***\n\n")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
               let newViewController = MobileNumberAndShareIDVC()
               self.navigationController?.pushViewController(newViewController, animated: true)
            }
          }else {
            self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: currentApprovalCode.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
             
           }
    }
}
