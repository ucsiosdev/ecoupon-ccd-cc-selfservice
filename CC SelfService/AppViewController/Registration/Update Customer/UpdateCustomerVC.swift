//
//  UpdateCustomerVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 18/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit


protocol UpdateCustomerVCDelegate : class{
      func passUpdatedCustModel(updatedCustomerModel:NewRegistrationDetails)
}

class UpdateCustomerVC: BaseViewController,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet var TFTitle: SelfSerticeTextField!
    @IBOutlet var TFFirstName: SelfSerticeTextField!
    @IBOutlet var TFLastName: SelfSerticeTextField!
    @IBOutlet var TFResidentalStatus: SelfSerticeTextField!
    @IBOutlet var TFNationlity: SelfSerticeTextField!
    @IBOutlet var TFResidentalCountry: SelfSerticeTextField!
    @IBOutlet var TFCityEmirate: SelfSerticeTextField!
    @IBOutlet var TFAreaOfResidance: SelfSerticeTextField!
    @IBOutlet var TFDialCode: SelfSerticeTextField!
    @IBOutlet var TFMobile: SelfSerticeTextField!
    @IBOutlet var TFShareID: SelfSerticeTextField!
    @IBOutlet var TFEmail: SelfSerticeTextField!
    
    var modelNewRegistrationDetals = NewRegistrationDetails()
    var globalModelNameTitleList = NameTitleList()
    var globalModelNationality = Nationality()
    let restManagerTitle = RestManager()
    let restManagerNationality = RestManager()
    let restManagerCity = RestManager()
    let restManagerArea = RestManager()
    let restManagerUpdateCustomer = RestManager()
    weak var delegate : UpdateCustomerVCDelegate?
    
    private var arrayCountryList: [Country] {
           let countries = CountriesDetails()
           let countryList = countries.countries
           return countryList
       }
    
    
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        DispatchQueue.main.async(execute: {
            self.handleExistingCustomerData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if globalModelNameTitleList.ListTitleContract.count == 0 {
            callAPIGetTitle()
        }
        if globalModelNationality.ListNationalityContract.count == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.3) {
                self.callAPIGetNationality()
            }
        }
        
        /*
         fetchNameTitlesFromJsonFile()
         fetchNationalityFromJsonFile()
         */
    }
    
    
    func handleExistingCustomerData(){
           
           //title
           if modelNewRegistrationDetals.TitleCode?.count ?? 0 > 0{
               
               if let matchedOffset = globalModelNameTitleList.ListTitleContract.index(where: {$0.Code == modelNewRegistrationDetals.TitleCode}) {
                   TFTitle.text = globalModelNameTitleList.ListTitleContract[matchedOffset].Description
               }
           }
           
           //First name
           if modelNewRegistrationDetals.FirstName?.count ?? 0 > 0{
               TFFirstName.text = modelNewRegistrationDetals.FirstName ?? ""
           }
           
           //Last name
           if modelNewRegistrationDetals.LastName?.count ?? 0 > 0{
               TFLastName.text = modelNewRegistrationDetals.LastName ?? ""
           }
           
           //Residental status
           if modelNewRegistrationDetals.ResidentialStatusID?.count ?? 0 > 0{
               if let matchedOffset = createModelArrayResidentialStatus().index(where: {$0.Code == modelNewRegistrationDetals.ResidentialStatusID}) {
                   TFResidentalStatus.text = createModelArrayResidentialStatus()[matchedOffset].Description
               }
           }
           
           
           //Nationality
           if modelNewRegistrationDetals.NationalityID?.count ?? 0 > 0{
               if let matchedOffset = globalModelNationality.ListNationalityContract.index(where: {$0.Code == modelNewRegistrationDetals.NationalityID}) {
                   TFNationlity.text = globalModelNationality.ListNationalityContract[matchedOffset].Description
               }
           }
           
           
           //Residence country
           if modelNewRegistrationDetals.CountryResidenceID?.count ?? 0 > 0{
               if let matchedOffset = globalModelNationality.ListNationalityContract.index(where: {$0.Code == modelNewRegistrationDetals.CountryResidenceID}) {
                   TFResidentalCountry.text = globalModelNationality.ListNationalityContract[matchedOffset].Description
               }
           }
        
          //counday code, it means dial code here
           if modelNewRegistrationDetals.DialCode?.count ?? 0 > 0{
               TFDialCode.text = "+\(modelNewRegistrationDetals.DialCode ?? "")"
           }
          
           //mobile number
          if modelNewRegistrationDetals.MobileNumber?.count ?? 0 > 0 {
            TFMobile.text = modelNewRegistrationDetals.MobileNumber ?? ""
          }
          
          //share id
          if modelNewRegistrationDetals.ShareId?.count ?? 0 > 0 {
            TFShareID.text = modelNewRegistrationDetals.ShareId ?? ""
          }
           
           //email
           if modelNewRegistrationDetals.EmailAddress?.count ?? 0 > 0{
               TFEmail.text = modelNewRegistrationDetals.EmailAddress ?? ""
           }
        
           
       }
    
    
    
    //MARK: HELPER METHODS
    func setupView(){
        
        TFFirstName.rightView = nil
        TFLastName.rightView = nil
        TFMobile.rightView = nil
        TFShareID.rightView = nil
        TFEmail.rightView = nil
    }
    
    func handlePreviousScreenData(){
        
        if modelNewRegistrationDetals.DialCode?.count ?? 0 > 0{
            TFDialCode.text = "+\(modelNewRegistrationDetals.DialCode ?? "")"
        }
        if modelNewRegistrationDetals.MobileNumber?.count ?? 0 > 0 {
            TFMobile.text = modelNewRegistrationDetals.MobileNumber ?? ""
        }
        if modelNewRegistrationDetals.ShareId?.count ?? 0 > 0 {
            TFShareID.text = modelNewRegistrationDetals.ShareId ?? ""
        }
    }
    
    /*
    func fetchNameTitlesFromJsonFile(){
        
        guard let url = Bundle.main.url(forResource: "NameTitles", withExtension: "json") else { return  }
        guard let data = try? Data(contentsOf: url) else { return  }
        do {
            globalModelNameTitleList = try JSONDecoder().decode(NameTitleList.self, from: data)
            //print(model)
        } catch {
            // I find it handy to keep track of why the decoding has failed. E.g.:
            print("\n\n*** WARNING!! JSONDecoder error: \(error) ***\n\n")
            // Insert error handling here
        }
        
    }
    
    func fetchNationalityFromJsonFile(){
        
        guard let url = Bundle.main.url(forResource: "nationalities", withExtension: "json") else { return  }
        guard let data = try? Data(contentsOf: url) else { return  }
        do {
            globalModelNationality = try JSONDecoder().decode(Nationality.self, from: data)
            //print(model)
        } catch {
            // I find it handy to keep track of why the decoding has failed. E.g.:
            print("\n\n*** WARNING!! JSONDecoder error: \(error) ***\n\n")
            // Insert error handling here
        }
    }
    */
    func createModelArrayResidentialStatus() -> [ResidentialStatusArray]{
        
        var model_1 = ResidentialStatusArray.init()
        model_1.Code = "R"
        model_1.Description = "Resident"
        
        var model_2 = ResidentialStatusArray.init()
        model_2.Code = "V"
        model_2.Description = "Visitor"
        
        var array = [ResidentialStatusArray]()
        array.append(model_1)
        array.append(model_2)
        
        return array
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnTermOfUseClicked(_ sender: Any) {
        
        if let url = URL(string: "http://www.majidalfuttaim.com/privacy"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {
            
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        
        TFTitle.text =  TFTitle.text?.trimString()
        TFFirstName.text =  TFFirstName.text?.trimString()
        TFLastName.text =  TFLastName.text?.trimString()
        TFMobile.text =  TFMobile.text?.trimString()
        TFShareID.text =  TFShareID.text?.trimString()
        TFEmail.text =  TFEmail.text?.trimString()
        
        if modelNewRegistrationDetals.TitleCode?.count == 0 || TFTitle.text?.count == 0 {
            view.makeToast("Please select Title", duration: 2.0, position: .center)
            TFTitle.shake()
            return
        }
        if TFFirstName.text?.count == 0 {
            view.makeToast("Please enter your first name", duration: 2.0, position: .center)
            TFFirstName.shake()
            return
        }
        
        if let number = Int.parse(from: TFFirstName.text ?? "") {
            // Do something with this number
            print("\n * FirstName number found: \(number) *\n")
            view.makeToast("Digits are not allowed in name fileds", duration: 2.0, position: .center)
            TFFirstName.shake()
            return
        }
        
        
        if TFLastName.text?.count == 0 {
            view.makeToast("Please enter your last name", duration: 2.0, position: .center)
            TFLastName.shake()
            return
        }
        
        if let number = Int.parse(from: TFLastName.text ?? "") {
            // Do something with this number
            print("\n *  LastName number found: \(number) *\n")
            view.makeToast("Digits are not allowed in name fileds", duration: 2.0, position: .center)
            TFLastName.shake()
            return
        }
        
        if TFNationlity.text?.count == 0 || modelNewRegistrationDetals.NationalityID?.count == 0 {
            view.makeToast("Please select Nationality", duration: 2.0, position: .center)
            TFNationlity.shake()
            return
        }
        
        
        
        if TFDialCode.text?.count == 0 {
            view.makeToast("Please select country code", duration: 2.0, position: .center)
            TFDialCode.shake()
            return
        }
        
        if TFMobile.text?.count ?? 0 < 5 || TFMobile.text?.count ?? 0 > 15 {
            view.makeToast(KMobilenNumberLengthError, duration: 2.0, position: .center)
            TFMobile.shake()
            return
        }
        
        if TFShareID.text?.count ?? 0 > 0 {
            if TFShareID.text?.count ?? 0 < 16 {
                view.makeToast("Share ID must be 16 digits", duration: 2.0, position: .center)
                TFShareID.shake()
                return
            }
        }
        
        if TFEmail.text?.count ?? 0 > 0 {
            if String.validateEmail(sourceString: TFEmail.text!){
                print("**\n email is valid **\n")
            }else {
                view.makeToast("Please enter valid email", duration: 2.0, position: .center)
                TFEmail.shake()
                return
            }
        }
        
        if modelNewRegistrationDetals.CustomerID?.count ?? 0 > 0 {
            self.callAPIPOSTUpdateCustomer()
        }
        else {
            
            DispatchQueue.main.async(execute: {
                
                self.updateCustomerModel()
                
                if let _ = self.delegate?.passUpdatedCustModel(updatedCustomerModel: self.modelNewRegistrationDetals){
                    print ("\n\n *** is Delegate Method > passUpdatedCustModel Avaialble: YES  ***\n\n")
                }
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        
        
    }
    
    
    //MARK: -TEXT FIELD DELEGATE METHODS
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField ==  TFTitle {
            view.endEditing(true)
            
            if globalModelNameTitleList.ListTitleContract.count == 0{
                self.view.makeToast("Title list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            let nameTitle = GenericModelArrayListPopup()
            nameTitle.delegate=self
            nameTitle.navigTitle = "Select Title"
            nameTitle.searchBarPlaceHolderStr = "Search"
            nameTitle.isNeedsShowMallList = true
            nameTitle.contentArray = globalModelNameTitleList.ListTitleContract as [AnyObject]
            let controller = UINavigationController(rootViewController: nameTitle)
            controller.preferredContentSize = CGSize(width: 280, height: 240)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFTitle
                popover.sourceRect = TFTitle.bounds
                popover.permittedArrowDirections = .left
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }else if textField ==  TFResidentalStatus {
            view.endEditing(true)
            
            if createModelArrayResidentialStatus().count == 0{
                self.view.makeToast("Residential Status list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            let residentialStatus = GenericModelArrayListPopup()
            residentialStatus.delegate=self
            residentialStatus.navigTitle = "Select Residential Status"
            residentialStatus.isComingNewRegistartionVC_ResStatus = true
            residentialStatus.contentArray = createModelArrayResidentialStatus() as [AnyObject]
            let controller = UINavigationController(rootViewController: residentialStatus)
            controller.preferredContentSize = CGSize(width: 351, height: 200)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFResidentalStatus
                popover.sourceRect = TFResidentalStatus.bounds
                popover.permittedArrowDirections = .left
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
            
        else if textField ==  TFNationlity {
            view.endEditing(true)
            
            if  globalModelNationality.ListNationalityContract.count == 0 {
                view.makeToast("Nationality list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            let nationlity = GenericModelArrayListPopup()
            nationlity.delegate=self
            nationlity.navigTitle = "Select Nationality"
            nationlity.isComingNewRegistartionVC_Nationality = true
            nationlity.contentArray = globalModelNationality.ListNationalityContract as [AnyObject]
            
            let controller = UINavigationController(rootViewController: nationlity)
            controller.preferredContentSize = CGSize(width: 351, height: 450)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFNationlity
                popover.sourceRect = TFNationlity.bounds
                popover.permittedArrowDirections = .down
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
            
        else if textField == TFResidentalCountry {
            view.endEditing(true)
            
            TFCityEmirate.text = ""
            TFAreaOfResidance.text = ""
            
            if  globalModelNationality.ListNationalityContract.count == 0 {
                view.makeToast("Residential Country list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            let nationlity = GenericModelArrayListPopup()
            nationlity.delegate=self
            nationlity.navigTitle = "Select Residential Country"
            nationlity.isComingNewRegistartionVC_ResCountry = true
            nationlity.contentArray = globalModelNationality.ListNationalityContract as [AnyObject]
            
            let controller = UINavigationController(rootViewController: nationlity)
            controller.preferredContentSize = CGSize(width: 351, height: 450)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFResidentalCountry
                popover.sourceRect = TFResidentalCountry.bounds
                popover.permittedArrowDirections = .right
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
        else if textField ==  TFCityEmirate {
            view.endEditing(true)
            
            if TFResidentalCountry.text?.count ?? 0 > 0 {
                TFAreaOfResidance.text = ""
                callAPIGetResidenceCity()
            }else {
                view.makeToast("Please select Residential Country first", duration: 2.0, position: .center)
            }
            return false
        }
            
        else if textField ==  TFAreaOfResidance {
            view.endEditing(true)
            
            if TFCityEmirate.text?.count ?? 0 > 0 {
                callAPIGetGetResidenceArea()
            }else {
                view.makeToast("Please select City/Emirate first", duration: 2.0, position: .center)
            }
            return false
        }
        
        if textField ==  TFDialCode {
            view.endEditing(true)
            
            
            if  arrayCountryList.count == 0 {
                view.makeToast("Country list not available, Please try again later.", duration: 2.0, position: .center)
                return false
            }
            
            
            let countryListPopup = GenericModelArrayListPopup()
            countryListPopup.delegate=self
            countryListPopup.navigTitle = "Select Country Code"
            countryListPopup.searchBarPlaceHolderStr = "Search Countries"
            countryListPopup.contentArray = arrayCountryList
            countryListPopup.isComingFromMobileNumberAndShareIDVC = true
            let controller = UINavigationController(rootViewController: countryListPopup)
            controller.preferredContentSize = CGSize(width: 350, height: 450)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFDialCode
                popover.sourceRect = TFDialCode.bounds
                popover.permittedArrowDirections = .left
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
        
        
        
        return true
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == TFFirstName || textField ==  TFLastName  {
            if textField.text?.count ?? 0  > KNameTextFieldLimit && string != "" {
                return false
            }
        }
        
        // Allow only positive numerical input for mobile number
        if textField == TFMobile  {
            var allowedCharacters = ""
            allowedCharacters = "0123456789"
            if textField.text?.count ?? 0  > 14 && string != "" {
                return false
            }
            _ = (textField.text! as NSString)
                .replacingCharacters(in: range, with: string)
            let characterSet = CharacterSet(charactersIn: allowedCharacters)
            let range = string.rangeOfCharacter(from: characterSet)
            if range == nil && string != ""  {
                // print("\n\n ** letters found  **\n\n")
                return false
            }
            
        }
            
            //ShareID must be 16 digit and also must only number
        else if textField == TFShareID  {
            var allowedCharacters = ""
            allowedCharacters = "0123456789"
            if textField.text?.count ?? 0  > 15 && string != "" {
                return false
            }
            
            _ = (textField.text! as NSString)
                .replacingCharacters(in: range, with: string)
            let characterSet = CharacterSet(charactersIn: allowedCharacters)
            let range = string.rangeOfCharacter(from: characterSet)
            if range == nil && string != ""  {
                // print("\n\n ** letters found  **\n\n")
                return false
            }
        }
        else if textField == TFEmail{
            
            if textField.text?.count ?? 0  > KEmailTextFieldLimit && string != "" {
                return false
            }
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: POP UP LIST DELEGATE METHOD | MODEL ARRAY
extension UpdateCustomerVC :GenericModelArrayListPopupDelegate {
   
    func activeCampaignMallDidSelect(modelActiveCampaignMall: ArrayActiveCampaignsByMall) {
        
    }
    
    
    
    func storeDidSelect(modelStore: StoreListaArray) {
        
    }
    func paymentModeSelectedPayment(modelPayment: PaymentListaArray) {
        
    }
    
    func areaSelected(modelArea: AreaListaArray) {
        TFAreaOfResidance.text = modelArea.AreaName ?? ""
        modelNewRegistrationDetals.AreaOfResidenceID =  String(modelArea.AreaID ?? -1)
    }
    
    func cityEmirateSelected(modelCity: CityListaArray) {
        TFCityEmirate.text = modelCity.CityName ?? ""
        modelNewRegistrationDetals.ResidenceCityID = String(modelCity.CityID ?? -1)
    }
    
    
    func nationlitySelected(index: NSInteger) {
        
        print("\n\n **** row number: \(index)   ***\n\n")
        
        if index < globalModelNationality.ListNationalityContract.count {
            
            let model = globalModelNationality.ListNationalityContract[index]
            TFNationlity.text = model.Description ?? ""
            modelNewRegistrationDetals.NationalityID = model.Code ?? ""
          
            
        }else {
            view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
        }
    }
    
    func residenceCounrySelected(index: NSInteger) {
        
        print("\n\n **** row number: \(index)   ***\n\n")
        
        if index < globalModelNationality.ListNationalityContract.count {
            
            let model = globalModelNationality.ListNationalityContract[index]
            TFResidentalCountry.text = model.Description ?? ""
            modelNewRegistrationDetals.CountryCode = model.Code ?? ""
            modelNewRegistrationDetals.CountryResidenceID = model.Code ?? ""
            
        }else {
            view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
        }
    }
    
    
    func residentialStatusSelected(model: ResidentialStatusArray) {
        TFResidentalStatus.text = model.Description ?? ""
        modelNewRegistrationDetals.ResidentialStatusID = model.Code ?? ""
    }
    
    func nameTitlePopupDidSelectedTitle(index: NSInteger) {
        
        print("\n\n **** row number: \(index)   ***\n\n")
        
        if index < globalModelNameTitleList.ListTitleContract.count {
            TFTitle.text = globalModelNameTitleList.ListTitleContract[index].Description ?? ""
            modelNewRegistrationDetals.Title = globalModelNameTitleList.ListTitleContract[index].Description ?? ""
            modelNewRegistrationDetals.TitleCode = globalModelNameTitleList.ListTitleContract[index].Code ?? ""
        }else {
            view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
        }
    }
   
    func countryListDidSelectCountry(rowNumber: NSInteger) {
        
        print("\n\n **** row number: \(rowNumber)   ***\n\n")
        if rowNumber < arrayCountryList.count{
            TFDialCode.text = "+\(arrayCountryList[rowNumber].phoneExtension)"
            modelNewRegistrationDetals.DialCode = arrayCountryList[rowNumber].phoneExtension
        }else {
            view.makeToast(KSomethingWentWrongTryAgainLater, duration: 2.0, position: .center)
        }
    }
    
    func mallPopupDidSelectedMall(selectedMall: InvoiceMallList) {
        
    }
}

//MARK: API CALL METHODS
extension UpdateCustomerVC{
    
    
    func callAPIGetTitle(){
        
        /*
         
         RequestTimeStamp =
         AppKey
         TokenID
         
         Format TokenID:
         RequestTimeStamp=2020-04-28 02:01:01&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        let HMAC_sha256_Value = "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID :HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetTitle)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerTitle.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelNameTitle = try? decoder.decode(NameTitleList.self, from: data){
                    print("\n\n ***** API respone Model:\(modelNameTitle.description) *****\n\n")
                    self.handleAPIGetTitleRespone(modelNameTitle:modelNameTitle)
                    
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                    
                }
            }else {
                
                self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    func handleAPIGetTitleRespone(modelNameTitle:NameTitleList){
           
           if modelNameTitle.ResponseCode == KAPIResSuccess{
               
               //remove not usefult index from API Respone array
               var nameTitleArrayLocal = modelNameTitle.ListTitleContract
               if let matchedOffset = modelNameTitle.ListTitleContract.index(where: {$0.Description == K_PleaseSelect_}) {
                   nameTitleArrayLocal.remove(at: matchedOffset)
               }
               
               if nameTitleArrayLocal.count == 0{
                   self.view.makeToast("Name title list is not available, please try again later", duration: 2.0, position: .center)
                   
               }
               else {
                   globalModelNameTitleList.ListTitleContract = nameTitleArrayLocal
                    //self.showStoreListPopup(storeArray:storeArrayLocal)
                
               }
           }else {
               showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelNameTitle.Result ?? KSomethingWentWrongTryAgainLater)
           }
       }
    
    
    
    
    func callAPIGetNationality(){
        
        /*
         
         RequestTimeStamp =
         AppKey
         TokenID
         
         Format TokenID:
         RequestTimeStamp=2020-04-28 02:01:01&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        let HMAC_sha256_Value = "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID :HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetNationality)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerNationality.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modalNationality = try? decoder.decode(Nationality.self, from: data){
                    print("\n\n ***** API respone Model:\(modalNationality.description) *****\n\n")
                    self.handleAPIGetNationalityRespone(modalNationality:modalNationality)
                    
                }else {
                   data.handleAPIParsingErrorCase(currentVC: self)
                    
                }
            }else {
                
                self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    
    func handleAPIGetNationalityRespone(modalNationality:Nationality){
        
        if modalNationality.ResponseCode == KAPIResSuccess{
            
            //remove not usefult index from API Respone array
            var tempArray = modalNationality.ListNationalityContract
            if let matchedOffset = modalNationality.ListNationalityContract.index(where: {$0.Description == K_PleaseSelect_}) {
                tempArray.remove(at: matchedOffset)
            }
            
            if tempArray.count == 0{
                self.view.makeToast("Nationality/Residential country list is not available, please try again later", duration: 2.0, position: .center)
                
            }
            else {
                globalModelNationality.ListNationalityContract = tempArray
                 //self.showStoreListPopup(storeArray:storeArrayLocal)
             
            }
            
            
            //handle update customer screen case
            DispatchQueue.main.async(execute: {
                self.handleExistingCustomerData()
            })
                   
            
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: modalNationality.Result ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    
    
    func callAPIGetResidenceCity(){
        
        /*
         Country = IN
         RequestTimeStamp =
         AppKey
         TokenID
         
         Format TokenID:
         Country=IN&RequestTimeStamp=2020-04-28 02:01:01&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        let HMAC_sha256_Value = "\(KCountry)=\(modelNewRegistrationDetals.CountryCode ?? "")&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KCountry: modelNewRegistrationDetals.CountryCode ?? "",
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID :HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetResidenceCity)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        
        
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerCity.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelCity = try? decoder.decode(CityList.self, from: data){
                    print("\n\n ***** API respone Model:\(modelCity.description) *****\n\n")
                    self.handleAPIResidenceCityRespone(modelCity:modelCity)
                    
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                    
                }
            }else {
                
                self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    
    func handleAPIResidenceCityRespone(modelCity:CityList){
        
        if modelCity.ResponseCode == KAPIResSuccess{
            //case 1: no city found for this country
            if modelCity.ListCityContract.count <= 1{
                 self.view.makeToast("City list is not available for this country", duration: 2.0, position: .center)
                
            }
                
                //case 2: city found for this country
            else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.showCityListPopup(cityArray:modelCity.ListCityContract)
                }
            }
        }else {
            
            self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelCity.Result ?? KSomethingWentWrongTryAgainLater)
            
        }
    }
    
    func showCityListPopup(cityArray:[CityListaArray]){
        
        
        let city = GenericModelArrayListPopup()
        city.delegate=self
        city.navigTitle = "Select City/Emirate"
        city.isComingNewRegistartionVC_City = true
        
        //remove not usefult index from API Respone array
        var cityArrayLocal = cityArray
        if let matchedOffset = cityArray.index(where: {$0.CityName == K_PleaseSelect_}) {
            cityArrayLocal.remove(at: matchedOffset)
        }
        
        if cityArrayLocal.count == 0 {
           view.makeToast("City list not available, Please try again later.", duration: 2.0, position: .center)
           return
        }
        
        city.contentArray = cityArrayLocal as [AnyObject]
        let controller = UINavigationController(rootViewController: city)
        controller.preferredContentSize = CGSize(width: 351, height: 450)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFCityEmirate
            popover.sourceRect = TFCityEmirate.bounds
            popover.permittedArrowDirections = .left
            self.present(controller, animated: true) {
            }
        }
        
    }
    
    
    
    func callAPIGetGetResidenceArea(){
        
        /*
         City = 15
         RequestTimeStamp =
         AppKey
         TokenID
         
         Format TokenID:
         
         City=15&RequestTimeStamp=2020-04-28 02:01:01&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        let HMAC_sha256_Value = "\(KCity)=\(modelNewRegistrationDetals.ResidenceCityID ?? "")&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KCity: modelNewRegistrationDetals.ResidenceCityID ?? "",
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID :HMAC_sha256_Value
        ]
        
        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
        
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetResidenceArea)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerArea.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelArea = try? decoder.decode(AreaList.self, from: data){
                    print("\n\n ***** API respone Model:\(modelArea.description) *****\n\n")
                    self.handleAPIResponeResidenceArea(modelArea:modelArea)
                    
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                    
                }
            }else {
                
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                
            }
            
        }
    }
    
    func handleAPIResponeResidenceArea(modelArea:AreaList){
        
        if modelArea.ResponseCode == KAPIResSuccess{
            //case 1: no area found for this country
            if modelArea.ListAreaContract.count <= 1{
                 self.view.makeToast("Area list is not available for this city", duration: 2.0, position: .center)
            
            }
                
            //case 2: area found for this country
            else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.showAreaListPopup(cityArray:modelArea.ListAreaContract)
                }
            }
        }else {
           showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelArea.Result ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    
    func showAreaListPopup(cityArray:[AreaListaArray]){
        
        
        let popup = GenericModelArrayListPopup()
        popup.delegate=self
        popup.navigTitle = "Select Area"
        popup.isComingNewRegistartionVC_Area = true
        
        //remove not usefult index from API Respone array
        var areaArrayLocal = cityArray
        if let matchedOffset = cityArray.index(where: {$0.AreaName == K_PleaseSelect_}) {
            areaArrayLocal.remove(at: matchedOffset)
        }
        
        if areaArrayLocal.count == 0 {
            view.makeToast("Area list not available, Please try again later.", duration: 2.0, position: .center)
            return
        }
        
        popup.contentArray = areaArrayLocal as [AnyObject]
        
        let controller = UINavigationController(rootViewController: popup)
        controller.preferredContentSize = CGSize(width: 351, height: 450)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFCityEmirate
            popover.sourceRect = TFCityEmirate.bounds
            popover.permittedArrowDirections = .left
            self.present(controller, animated: true) {
            }
        }
        
    }
    
    func callAPIPOSTUpdateCustomer(){
        
        /*
         1"CustID":"6CE7826C-E492-4C1A-9B88-37D521027461”,
         2"RequestTimeStamp":"2020-04-21 11:59:40”,
         3"AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a”,
         4"TokenID":"MapfFGST+FkR1NwiRF5oquuz0moy1TsP4k/AsBLwpOc=“,
         5"Title":"MR”,
         6"FirstName":"Test”,
         7"LastName":"Test”,
         8"CountryCode":"91”,
         9"MobileNo":”8269514914”,
         10"ShareID":"2222222222222222”,
         11"Email":"test2@gmail.com”,
         12"UpdatedBy":"397”,
         13"Nationality":"IN”,
         14"CountryResidence":"IN”,
         15"ResidenceStatus":"R”,
         16"ResidenceCity":"15”,
         17"ResidenceArea":”267”
         
         Checksum:
         CustID
         RequestTimeStamp
         AppKey

         */
        
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTUpdateCustomer)") else { return }
        
        var allParams = ""
        
        //Content-Type
        restManagerUpdateCustomer.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        
        // 1 CustID
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.CustomerID ?? "", forKey: KCustID)
        allParams = "\(KCustID)=\(modelNewRegistrationDetals.CustomerID ?? "")&"
        
        //2 RequestTimeStamp
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        restManagerUpdateCustomer.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
        
        //3 AppKey
        restManagerUpdateCustomer.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)"
        
        //4 TokenID
        let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        restManagerUpdateCustomer.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        
        
        //5 Title
        restManagerUpdateCustomer.httpBodyParameters.add(value:modelNewRegistrationDetals.TitleCode ?? "", forKey: KTitle)
        
        //6 FirstName
        restManagerUpdateCustomer.httpBodyParameters.add(value:TFFirstName.text!, forKey: KFirstName)
        
        //7 LastName
        restManagerUpdateCustomer.httpBodyParameters.add(value:TFLastName.text!, forKey: KLastName)
        
        //8 CountryCode
        restManagerUpdateCustomer.httpBodyParameters.add(value:modelNewRegistrationDetals.DialCode!, forKey: KCountryCode)
        
        //9 MobileNo
        restManagerUpdateCustomer.httpBodyParameters.add(value:TFMobile.text!, forKey: KMobileNo)
        
        //10 ShareID
        restManagerUpdateCustomer.httpBodyParameters.add(value:TFShareID.text ?? "", forKey: KShareID)
        
        //11 Email
        restManagerUpdateCustomer.httpBodyParameters.add(value:TFEmail.text ?? "", forKey: KEmail)
        
        //12 UpdatedBy
        restManagerUpdateCustomer.httpBodyParameters.add(value:SINGLETON.currentActiveApproveCode?.UserID ?? "", forKey: KUpdatedBy)
        
        //13 Nationality
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.NationalityID ?? "", forKey: KNationality)
        
        //14 CountryResidence
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.CountryResidenceID ?? "", forKey: KCountryResidence)
        
        //15 ResidenceStatus
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.ResidentialStatusID ?? "", forKey: KResidenceStatus)
        
        //16 ResidenceCity
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.ResidenceCityID ?? "", forKey: KResidenceCity)
        
        //17 ResidenceArea
        restManagerUpdateCustomer.httpBodyParameters.add(value: modelNewRegistrationDetals.AreaOfResidenceID ?? "", forKey: KResidenceArea)
        
               
               
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(restManagerUpdateCustomer.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerUpdateCustomer.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelUpdateCus = try? decoder.decode(NewCustomerOREntryCreated.self, from: data){
                    print("\n\n ***** API respone Model:\(modelUpdateCus.description) *****\n\n")
                    self.handleAPIUpdateCustomerRespone(modelUpdateCus:modelUpdateCus)
                }else {
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
               
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                
            }
        }
    }
    
    func handleAPIUpdateCustomerRespone(modelUpdateCus:NewCustomerOREntryCreated){
        
        if modelUpdateCus.ResponseCode == KAPIResSuccess{
            
            
            DispatchQueue.main.async(execute: {
                
                self.updateCustomerModel()
                
                if let _ = self.delegate?.passUpdatedCustModel(updatedCustomerModel: self.modelNewRegistrationDetals){
                    print ("\n\n *** is Delegate Method > passUpdatedCustModel Avaialble: YES  ***\n\n")
                }
                self.navigationController?.popViewController(animated: true)
            })
            
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg:modelUpdateCus.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            
        }
    }
    
    func updateCustomerModel(){
        
        modelNewRegistrationDetals.FirstName = TFFirstName.text!
        modelNewRegistrationDetals.LastName = TFLastName.text!
        modelNewRegistrationDetals.MobileNumber = TFMobile.text!
        modelNewRegistrationDetals.ShareId = TFShareID.text ?? ""
        modelNewRegistrationDetals.EmailAddress = TFEmail.text ?? ""
        
        
    }
    
}

