//
//  CampaignTokenVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 19/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class CampaignTokenVC:  BaseViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var lblCustomerName: SelfServiceTitleLabel!
    @IBOutlet var headerView: UITableViewCell!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewParentHeader: UIView!
    
    
    var modelArrayCampaignToken = [ArrayCampaignToken]()
    var modelNewRegistrationDetals = NewRegistrationDetails()
    var modelEToken = ValidateEToken()
    var btnPrint = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initObj()
        setupView()
        tblView.reloadData()
    }
    
    func initObj(){
        
        lblCustomerName.textColor = SelfServiceColor().KLigthOrange
        tblView.tableFooterView = UIView()
        tblView.register(UINib(nibName: "TblViewCellCampaignToken", bundle: nil), forCellReuseIdentifier: "TblViewCellCampaignToken")
    }
    
    func setupView(){
        lblCustomerName.text = modelNewRegistrationDetals.CustName ?? "Campaign details"
    }
    
    // MARK: TABLE VIEW DELEGATE METHODS
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerView.selectionStyle = .none
        headerView.backgroundColor = SelfServiceColor().KBlue
        //headerView.contentView.addTopandBottomBorder()
        viewParentHeader.layer.cornerRadius = 18
        viewParentHeader.layer.masksToBounds = false
        //headerView.contentView.clipsToBounds = true
        return headerView.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArrayCampaignToken.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 66
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TblViewCellCampaignToken = tableView.dequeueReusableCell(withIdentifier: "TblViewCellCampaignToken", for: indexPath) as! TblViewCellCampaignToken
        cell.selectionStyle = .none
        cell.btnPrint.tag = indexPath.row
        cell.btnPrint.layer.cornerRadius = 17.6
        cell.btnPrint.clipsToBounds = true
        cell.btnPrint.addTarget(self, action: #selector(btnPrintClicked(sender:)), for: .touchUpInside)
        
        
        let model = modelArrayCampaignToken[indexPath.row]
        //set model data
        cell.lblCampaignName.text = model.CampaignName ?? ""
        cell.lblTokenNo.text =  model.TokenCode ?? ""
        
        return cell
        
    }
    
   
    
    //MARK: ACTION BUTTON
    @IBAction func btnBackClicked(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: StartVC.self) {
              DispatchQueue.main.async(execute: {
                 _ =  self.navigationController!.popToViewController(controller, animated: true)
              })
              break
            }
        }
        
    }
    @objc func btnPrintClicked(sender: UIButton){
        
        btnPrint = sender
        
        print("\n * buttonTag: \(btnPrint.tag) *\n")
       
        Indicator.shared.showProgressView(self.view)
        
        let viewController = CampaignTokenDetailsVC()
        viewController.modelNewRegistrationDetals = modelNewRegistrationDetals
        viewController.modelCampaignToken = modelArrayCampaignToken[btnPrint.tag]
        viewController.modelEToken = modelEToken
        viewController.previousVC = self
        viewController.btnPrint = sender
        viewController.delegate = self
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(viewController, animated: false)
        }
        
    }
}

//MARK: CAMPAIGN DETAILS DELEGATE
extension CampaignTokenVC : CampaignTokenDetailsVCDelegate{
   
    func CampaignTokenDetailsDidDismiss(activityVC: UIActivityViewController) {
       
        activityVC.popoverPresentationController?.sourceView = btnPrint
        activityVC.popoverPresentationController?.sourceRect = btnPrint.bounds
        DispatchQueue.main.async{
            self.present(activityVC, animated: true, completion: nil)
        }
        
        Indicator.shared.hideProgressView()
        
    }
    
    
}

