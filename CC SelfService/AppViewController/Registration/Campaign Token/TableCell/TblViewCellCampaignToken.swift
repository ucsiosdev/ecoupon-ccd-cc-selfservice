//
//  TblViewCellCampaignToken.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 29/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class TblViewCellCampaignToken: UITableViewCell {

    @IBOutlet var viewParentLables: UIView!
    @IBOutlet var lblCampaignName: SelfServiceTblCelllLabel!
    @IBOutlet var lblTokenNo: SelfServiceTblCelllLabel!
    @IBOutlet var btnPrint: SelfServiceOrangeButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
