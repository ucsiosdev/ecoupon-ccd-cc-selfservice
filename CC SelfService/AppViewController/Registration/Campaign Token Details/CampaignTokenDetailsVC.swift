//
//  CampaignTokenDetailsVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 19/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit


protocol CampaignTokenDetailsVCDelegate : class{
    
    func CampaignTokenDetailsDidDismiss(activityVC:UIActivityViewController)
   
}


class CampaignTokenDetailsVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var viewContent: UIView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var imgVQRCode: UIImageView!
    @IBOutlet var lblQRCode: UILabel!
    
    
    let origImage = UIImage(named: "dots-separator")
    var tintedImage: UIImage?
    
    var modelArrayTblData = [AcknowledgementTblData]()
    var modelNewRegistrationDetals = NewRegistrationDetails()
    var modelCampaignToken = ArrayCampaignToken()
    var modelEToken = ValidateEToken()
    var previousVC = CampaignTokenVC()
    var btnPrint = UIButton()
    var timestampStr = String()
    
    
    weak var delegate : CampaignTokenDetailsVCDelegate?
    
    /*
     class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil,modelEToken:ValidateEToken) -> UIView? {
     let modelEToken =  modelEToken
     print("\n\n *** modelEToken: \(modelEToken) ***\n\n")
     return UINib(
     nibName: nibNamed,
     bundle: bundle
     ).instantiate(withOwner: nil, options: nil)[0] as? UIView
     }
     */
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        addModelDataForTableView()
        generateImgOfQRCodeAndPdf()
    }
    
    func initObj(){
        
        //btnDone.titleLabel?.font = SelfServiceFont().NavigationBarButtonItemFont
        //lblNavigTitle.font = SelfServiceFont().NavigBarTitleFont
        //viewContent.dropShadowWithOutCornerRadius(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        timestampStr = "\(Int(NSDate().timeIntervalSince1970 * 1000))"
       // let timestampStr = "\(Int(NSDate().timeIntervalSince1970 * 1000))"
       // lblNavigTitle.text = "campaign_\(timestampStr)"
        //lblNavigTitle.textColor = .black
        
        //viewNavigBar.backgroundColor = .white
        viewContent.backgroundColor = .white
        
        tblView.tableFooterView = UIView()
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 55
        tblView.register(UINib(nibName: "TblCellAcknowledgement", bundle: nil), forCellReuseIdentifier: "TblCellAcknowledgement")
    }
    
    func generateImgOfQRCodeAndPdf(){
        
        guard let qrURLImage = URL(string: modelCampaignToken.TokenCode ?? "QR Code not found")?.qrImage(using: .black, logo: nil) else { return }
        imgVQRCode.image = qrURLImage
        let pdfFileName = "campaign_\(timestampStr).pdf"
        let testPath = self.view.exportAsPdfFromView(fileName:pdfFileName)
        let url = NSURL.fileURL(withPath: testPath)
        
        self.sharePdf(path: url)
        
    }
    
    
    func sharePdf(path:URL) {
        
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: path.path) {
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
            // activityViewController.popoverPresentationController?.sourceRect = self.btnShare.bounds
            //activityViewController.popoverPresentationController?.sourceView = btnPrint
            //activityViewController.popoverPresentationController?.sourceRect = btnPrint.bounds
            activityViewController.setValue("campaign_\(timestampStr).pdf", forKey: "subject")
            
            
            DispatchQueue.main.async(execute: {
                if let _ = self.delegate?.CampaignTokenDetailsDidDismiss(activityVC: activityViewController){
                    print("\n\n ** CampaignTokenDetailsDidDismiss deleage avilable: YES **\n\n")
                }
                
                self.navigationController?.popViewController(animated: false)
            })
        } else {
            print("\n\n *** WARNING: document was not found ****\n\n")
            self.showAlertOnMainThread(title: KFailureAlertTitle, msg: KNotAbleShareFileMsg)
            
        }
    }
    
    
    
    func addModelDataForTableView(){
        
        //QR code
        if modelCampaignToken.TokenCode != nil && modelCampaignToken.TokenCode?.count ?? 0 > 0 {
            lblQRCode.text = modelCampaignToken.TokenCode!
            lblQRCode.isHidden = false
        }else {
            lblQRCode.isHidden = true
        }
        
        //1 Mall
        var model = AcknowledgementTblData()
        model.Title = "Mall"
        
        if let value = modelCampaignToken.MallName {
            model.Des_Eng = value
        }
        
        modelArrayTblData.append(model)
        
        //2 Campaign
        var model_2 = AcknowledgementTblData()
        model_2.Title = "Campaign"
        
        if let value = modelCampaignToken.CampaignName {
            model_2.Des_Eng = value
        }
        
        modelArrayTblData.append(model_2)
        
        //3 Customer Name
        var model_3 = AcknowledgementTblData()
        model_3.Title = "Customer Name"
        
        if let value = modelNewRegistrationDetals.CustName {
            model_3.Des_Eng = value
        }
        
        modelArrayTblData.append(model_3)
        
        //4 Date of Entry
        var model_4 = AcknowledgementTblData()
        model_4.Title = "Date of Entry"
        model_4.Des_Eng =  String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kdisplayDateFormatWithoutTime)
        
        modelArrayTblData.append(model_4)
        
        //5 Printed on
        var model_5 = AcknowledgementTblData()
        model_5.Title = "Printed on"
        model_5.Des_Eng =  String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kdisplayDateFormatWithoutTime)
        
        
        modelArrayTblData.append(model_5)
        
        tblView.reloadData()
    }
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArrayTblData.count
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblCellAcknowledgement", for: indexPath) as! TblCellAcknowledgement
        cell.selectionStyle = .none
        cell.imgVDivder_2.isHidden = true
        cell.lblDes_Arabic.isHidden = true

        let model = modelArrayTblData[indexPath.row]

        cell.lblTitle.text = model.Title
        cell.lblDes_Eng.text = model.Des_Eng
        cell.imgVDivider.image = tintedImage
        cell.imgVDivider.tintColor = .black
        cell.lblDes_Eng.textAlignment = .left
       
        return cell
        
    }
}
