
//
//  PopupCampaignList.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 04/06/20.
//  Copyright © 2020 UCS. All rights reserved.
//


import UIKit

protocol PopupCampaignListDelegate : class{
    func activeCampaignsMallDidSelect(modelActiveCampaignsMall:ArrayActiveCampaignsByMall,totalSelecteRows:NSInteger,selectedCellArray:[Int])
}

class PopupCampaignList: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var navigTitle: UILabel!
    @IBOutlet var tblView: UITableView!
    var navibarTitleStr = KCCSELFSERVICE
    var arraySelectedCells:[Int] = []
    weak var delegate : PopupCampaignListDelegate?
    var contentArray :[AnyObject] = []
    var unFilteredContentArray :[AnyObject] = []
    var modelNewRegistrationDetals = NewRegistrationDetails()
    
    //MARK: VC LIFE CYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initObj(){
        unFilteredContentArray = contentArray
        navigTitle.text = navibarTitleStr
        tblView.reloadData()
    }
    
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID = "cellCustom"
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: cellID)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellID)
        }
        cell.accessoryType = self.arraySelectedCells.contains(indexPath.row) ? .checkmark : .none
        cell.tintColor = SelfServiceColor().NavigationBarBackgroundColor
        
        let currentContent = contentArray[indexPath.row]
        switch currentContent {
        case let model as ArrayActiveCampaignsByMall:
            cell.textLabel?.text = model.CampaignDesc
            cell.textLabel?.font = SelfServiceFont.KGothamBookFontWithSize(size: 17)
            cell.textLabel?.textColor = .black
            
        default:
            print("\n\n **** unknown type in cell  **\n\n")
            return cell
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if self.arraySelectedCells.contains(indexPath.row) {
            guard let indexDeleteRow = arraySelectedCells.index(of: indexPath.row) else { return  }
            self.arraySelectedCells.remove(at:indexDeleteRow)
        } else {
            
            let currentContent = contentArray[indexPath.row]
            switch currentContent {
            
            case let modelMall as ArrayActiveCampaignsByMall:
                if modelMall.ShareIDRequired == "Y" && modelNewRegistrationDetals.ShareId?.count == 0 {
                    view.makeToast("Share id Required to Participate \(modelMall.CampaignDesc ?? "this campaign")", duration: 3.0, position: .center)
                    return
            }
                
            default:
                print("\n\n **** unknown type in cell  **\n\n")
               
            }
            
            self.arraySelectedCells.append(indexPath.row)
        }
        
        
        if let selectedMall = self.contentArray[indexPath.row] as? ArrayActiveCampaignsByMall{
            if let _ = self.delegate?.activeCampaignsMallDidSelect(modelActiveCampaignsMall:selectedMall, totalSelecteRows: arraySelectedCells.count, selectedCellArray: arraySelectedCells){
                print ("\n\n *** is Delegate Method > activeCampaignsMallDidSelect Avaialble: YES  ***\n\n")
            }
        }
        
        tableView.reloadData()
    }
    
}

