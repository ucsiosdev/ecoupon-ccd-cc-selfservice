//
//  ReceiptConfirmationVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 04/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class ReceiptConfirmationVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet var lblCustomerName: SelfServiceTitleLabel!
    @IBOutlet var headerView: UITableViewCell!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewParentHeader: UIView!
    @IBOutlet var TFSelectCampaign: SelfSerticeTextField!
    @IBOutlet var TFSelectApprovalCode: SelfSerticeTextField!
    @IBOutlet weak var btnBack: SelfServiceBlueButton!
    
    @IBOutlet weak var btnSubmit: SelfServiceLightOrangeButton!
    var modelNewRegistrationDetals = NewRegistrationDetails()
    var modelReceiptArray = [ReceiptArray]()
    var restManager = RestManager()
    var globalActiveCampaignsMall = ActiveCampaignsByMall()
    var arraySelectedCells:[Int] = []
    var globalRefNo = String()
    var totalAPISaveReceiptHitCount = 0
   // var arrayCampaignMallIDs = [String]()
    var isSubmitButtonActionCompleted = true
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        stupView()
        tblView.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isMovingToParentViewController{
            
            var allMallIDs = ""
            var tempArray = [String]()
            var count = 0
            for model in modelReceiptArray{
                if count == 0{
                    tempArray.append(model.MallID ?? "")
                 }else {
                    if tempArray.contains(model.MallID ?? ""){
                        //skip
                    }else {
                        tempArray.append(model.MallID ?? "")
                    }
                    
                }
                count = count+1
            }
            
            allMallIDs = tempArray.joined(separator: "$")
            print("\n\n ** Hint all mall IDs for hit API: callAPIGetActiveCampaignsByMall :\(allMallIDs)  **\n\n")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.callAPIGetActiveCampaignsByMall(allMallIDs:allMallIDs)
            }
        }
    }
    
    func initObj(){
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib(nibName: "TblViewCellReceiptEntry", bundle: nil), forCellReuseIdentifier: "TblViewCellReceiptEntry")
        TFSelectApprovalCode.rightView = nil
        
    }
    
    func stupView(){
        lblCustomerName.text = "\(modelNewRegistrationDetals.CustName ?? "")"
    }
    
    
    //MARK: -TEXT FIELD DELEGATE METHODS
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField ==  TFSelectCampaign {
            view.endEditing(true)
            
            if globalActiveCampaignsMall.CampaignList.count == 0 {
                view.makeToast(KActiveCampaignNotAvailableMsg, duration: 2.0, position: .center)
                return false
            }
            
            let popup = PopupCampaignList()
            popup.delegate=self
            popup.navibarTitleStr = "Select Campaign(s)"
            popup.arraySelectedCells = arraySelectedCells
            popup.modelNewRegistrationDetals = modelNewRegistrationDetals
            popup.contentArray = globalActiveCampaignsMall.CampaignList as [AnyObject]
            let controller = UINavigationController(rootViewController: popup)
            controller.preferredContentSize = CGSize(width: 400, height: 300)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            if let popover = controller.popoverPresentationController  {
                popover.delegate = self
                popover.sourceView = TFSelectCampaign
                popover.sourceRect = TFSelectCampaign.bounds
                popover.permittedArrowDirections = .down
                self.present(controller, animated: true) {
                }
            }
            
            return false
        }
        
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == TFSelectApprovalCode {
            if textField.text?.count ?? 0  > KApprovalCodeTextFieldLimit && string != "" {
                return false
            }
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: ACTION BUTTON
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        changeButtonsState(isEnable: false)
        
        TFSelectCampaign.text =  TFSelectCampaign.text?.trimString()
        TFSelectApprovalCode.text =  TFSelectApprovalCode.text?.trimString()
        
        if  arraySelectedCells.count  == 0{
            changeButtonsState(isEnable: true)
            view.makeToast("Please select at least 1 campaign.", duration: 2.0, position: .center)
            TFSelectCampaign.shake()
            return
        }
        
       else if TFSelectApprovalCode.text?.count == 0{
            changeButtonsState(isEnable: true)
            view.makeToast("Please enter your approval code to processed.", duration: 2.0, position: .center)
            TFSelectApprovalCode.shake()
            return
        }
        
        else {
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
               changeButtonsState(isEnable: true)
               print("\n\n **** Net Not connected  ***\n\n")
               self.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
                return
                
            case .online(.wwan):
                print("\n\n **** Connected via WWAN  ***\n\n")
            case .online(.wiFi):
                print("\n\n **** Connected via WiFi  ***\n\n")
            }
            
            
             
            modelNewRegistrationDetals.CampaignID = ""
            var count = 0
            for index in arraySelectedCells{
                
                if index < globalActiveCampaignsMall.CampaignList.count{
                    
                    let model =  globalActiveCampaignsMall.CampaignList[index]
                    
                    if count == 0 {
                        modelNewRegistrationDetals.CampaignID = model.CampID ?? ""
                    }else{
                        modelNewRegistrationDetals.CampaignID = "\(modelNewRegistrationDetals.CampaignID ?? "")" + "," + "\(model.CampID ?? "")"
                    }
                    
                    count = count+1
                }
            }
            
             print("\n * All Campaign ID: \(modelNewRegistrationDetals.CampaignID ?? "") *\n")
            
            
            if modelNewRegistrationDetals.CustomerID?.count ?? 0 > 0 {
                callAPIGetNewEntryID()
            }else {
                callAPIPOSTSaveCustomer()
            }
            
            
            
            
            /*
            let uniqueID = Array(Set(arrayCampaignMallIDs))
            modelNewRegistrationDetals.CampaignID =  uniqueID.joined(separator: ",")
            print("\n * All CampaignID: \(String(describing: modelNewRegistrationDetals.CampaignID) ) *\n")
            */
            
        }
        
    }
    
    func changeButtonsState(isEnable:Bool){
        
        DispatchQueue.main.async(execute: {
            self.btnBack.isEnabled = isEnable
            self.btnSubmit.isEnabled = isEnable
            
            
            
            if isEnable {
               
                //back
                self.btnBack.setTitleColor(SelfServiceColor().KWhite, for:.normal)
                
                //submit
                self.btnSubmit.setBackgroundImage(UIImage(named: "btnLightOrang"), for: .normal)
                self.btnSubmit.setTitleColor(SelfServiceColor().ButtonTitleLigthOrange, for:.normal)
                self.btnSubmit.backgroundColor = UIColor.clear
                
                
            }else {
                
                //back
                let colorTemp = UIColor(red: 243.0/255.0, green: 236.0/255.0, blue: 228.0/255.0, alpha: 1.0)
                self.btnBack.setTitleColor(colorTemp, for:.normal)
                
                //submit
                self.btnSubmit.setBackgroundImage(nil, for: .normal)
                self.btnSubmit.setTitleColor(SelfServiceColor().ButtonDisableTitleLightOrange, for:.normal)
                self.btnSubmit.backgroundColor =  SelfServiceColor().ButtonDisableBGLightGrey
               
            }
            
            
         })
      }
    
  // MARK: TABLE VIEW DELEGATE METHODS
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerView.selectionStyle = .none
        headerView.backgroundColor = SelfServiceColor().KBlue
        //headerView.contentView.addTopandBottomBorder()
        viewParentHeader.layer.cornerRadius = 18
        viewParentHeader.layer.masksToBounds = false
        //headerView.contentView.clipsToBounds = true
        return headerView.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelReceiptArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblViewCellReceiptEntry", for: indexPath) as! TblViewCellReceiptEntry
        cell.selectionStyle = .none

        
        cell.viewParentLabels.layer.borderColor = SelfServiceColor().KGreyBorderColor.cgColor
        cell.viewParentLabels.layer.borderWidth = 2.0
        cell.viewParentLabels.layer.cornerRadius = 18
        cell.viewParentLabels.layer.masksToBounds = false
        cell.btnDelete.isHidden = true
        
        let model = modelReceiptArray[indexPath.row]
        cell.lblReceipt.text = model.InvoiceNumber ?? ""
        cell.lblMallName.text = model.MallName ?? ""
        cell.lblStore.text = model.StoreName ?? ""
        cell.lblDate.text = model.InvoiceDateStrDisplayFormat ?? ""
        cell.lblValue.text = model.InvoiceValue ?? ""
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
}

//MARK: API CALL METHODS
extension ReceiptConfirmationVC{
    
    func callAPIGetActiveCampaignsByMall(allMallIDs:String){
           
           /*
            
            MallList:1 // mallID of api GetInvoiceMallsList
            RequestTimeStamp:2020-04-21%2011:49:24
            AppKey:3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
            TokenID:tdLgcrdW7g5OoREtS9DWkPHTIR23wWYNyt5IpsbPtHE%3D
            
            */
           
           let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
           
           let HMAC_sha256_Value = "\(KMallList)=\(allMallIDs)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
           
           let queryParams: [String: String] = [
               KMallList: allMallIDs,
               KRequestTimeStamp: currentDateTimeInUTC,
               KAppKey : KAppKeyDefaultTestApp,
               KTokenID :HMAC_sha256_Value
           ]
           
           var urlComponents = URLComponents()
           urlComponents.setQueryItems(with: queryParams)
           
           var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetActiveCampaignsByMall)" + (urlComponents.url?.absoluteString ?? "")
           
           completeURLString = completeURLString.stringReplacePlusSign()
           
           
           guard let finalURL = URL(string:completeURLString) else {return}
           
           print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
           
           Indicator.shared.showProgressView(self.view)
           restManager = RestManager()
           restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
               
               Indicator.shared.hideProgressView()
               
               guard let response = results.response else { return }
               if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                   guard let data = results.data else { return }
                   let decoder = JSONDecoder()
                   if let modelActiveCampaignsByMall = try? decoder.decode(ActiveCampaignsByMall.self, from: data){
                       print("\n\n ***** API respone Model:\(modelActiveCampaignsByMall.description) *****\n\n")
                       self.handleAPIGetActiveCampaignsByMallRespone(modelActiveCampaignsByMall:modelActiveCampaignsByMall)
                       
                   }else {
                       data.handleAPIParsingErrorCase(currentVC: self)
                       
                   }
               }else {
                   
                   self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                   
               }
               
           }
       }
       
       func handleAPIGetActiveCampaignsByMallRespone(modelActiveCampaignsByMall:ActiveCampaignsByMall){
              
              if modelActiveCampaignsByMall.ResponseCode == KAPIResSuccess{
                  
                  //remove not usefult index from API Respone array
                  var tempArray = modelActiveCampaignsByMall.CampaignList
                  if let matchedOffset = modelActiveCampaignsByMall.CampaignList.index(where: {$0.CampaignDesc == K_PleaseSelect_}) {
                      tempArray.remove(at: matchedOffset)
                  }
                  
                  if tempArray.count == 0{
                      self.view.makeToast(KActiveCampaignNotAvailableMsg, duration: 2.0, position: .center)
                 }
                  else {
                        globalActiveCampaignsMall.CampaignList = tempArray
                       //self.showStoreListPopup(storeArray:storeArrayLocal)
                   
                  }
              }else {
                  showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelActiveCampaignsByMall.Result ?? KSomethingWentWrongTryAgainLater)
              }
    }
    
    
      func callAPIGetNewEntryID(){
              
               /*
               
               CustID:6CE7826C-E492-4C1A-9B88-37D521027461
               RequestTimeStamp:2020-05-05%2013:13:45
               AppKey:3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
               TokenID:L6GfmiLItMSszNRyF9ak3rkP8hqd%2Be7cWofCp3Sqvy8%3D
               ApprovalCode:NS547
               VersionNo:3
               
               
               */
              
             let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
              
              let HMAC_sha256_Value = "\(KCustID)=\(modelNewRegistrationDetals.CustomerID ?? "")&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
              
              let queryParams: [String: String] = [
                  KCustID: modelNewRegistrationDetals.CustomerID ?? "",
                  KRequestTimeStamp: currentDateTimeInUTC,
                  KAppKey : KAppKeyDefaultTestApp,
                  KApprovalCode: TFSelectApprovalCode.text ?? "",
                  KVersionNo: KVersionNoValue_3,
                  KTokenID :HMAC_sha256_Value
              ]
              
              var urlComponents = URLComponents()
              urlComponents.setQueryItems(with: queryParams)
              
              var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLGetNewEntryID)" + (urlComponents.url?.absoluteString ?? "")
              
              completeURLString = completeURLString.stringReplacePlusSign()
              
              
              guard let finalURL = URL(string:completeURLString) else {return}
              
              print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
              
              Indicator.shared.showProgressView(self.view)
              restManager = RestManager()
              restManager.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
                  
                  Indicator.shared.hideProgressView()
                  
                  guard let response = results.response else { return }
                  if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                      guard let data = results.data else { return }
                      let decoder = JSONDecoder()
                      if let modelNewCusOrEntry = try? decoder.decode(NewCustomerOREntryCreated.self, from: data){
                          print("\n\n ***** API respone Model:\(modelNewCusOrEntry.description) *****\n\n")
                          self.handleAPIGetNewEntryIDRespone(modelNewCusOrEntry:modelNewCusOrEntry)
                          
                      }else {
                          self.changeButtonsState(isEnable: true)
                          data.handleAPIParsingErrorCase(currentVC: self)
                          
                      }
                  }else {
                      self.changeButtonsState(isEnable: true)
                      self.showAlertOnMainThread(title:KCCSELFSERVICE , msg: KSomethingWentWrongTryAgainLater)
                      
                  }
                  
              }
          }
          
    func handleAPIGetNewEntryIDRespone(modelNewCusOrEntry:NewCustomerOREntryCreated){
        
        if modelNewCusOrEntry.ResponseCode == KAPIResSuccess{
            
            changeButtonsState(isEnable: false)
            globalRefNo = modelNewCusOrEntry.RefNo ?? ""
            
            //clear previous restManage, re-init this, becuase sometimes without clear this error occur
            restManager = RestManager()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.callAPIPOSTSaveReceipt(modelReceipt: self.modelReceiptArray.first!)
            }
        }
        else {
            changeButtonsState(isEnable: true)
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelNewCusOrEntry.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    
    func callAPIPOSTSaveCustomer(){
        
        /*
        // "CustRefNo":"6F5030FD6676585819089C999936F558F086496782021AA6328F5B96F58989A7”,
        // "Title":"MR”,
         //"FirstName":"Hari”,
         //"LastName":"Tyagi”,
        // "CountryCode":"91”, // dial code of mobile number
        // "AreaCode":”",
        // "MobileNo":"7247451404”,
        // "ShareID":"1111111111111111”,
        // "Email":"test@gmail.com”,
         //"RequestTimeStamp":"2020-04-21 11:51:20”,
        // "AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a”,
        // "TokenID":"+3LYk89xwfTjXelTlV2X7DWhIYhAU8F2/r9Glyu8hII=“,
        // "UserAgent":"Mobile”, // hard coded
         //"WebMallID":”", //hard coded
         //"PrivacyPolicy":"Y”, // hard coded
        // "Nationality":"IN”, // nationally
        // "VersionNo":3, // hard code
        // "CountryResidence":"IN”, residence
        // "ResidenceStatus":"R”,
        // "ResidenceCity":"15”, city list id
        // "ResidenceArea":"267”, ResidenceArea ID
        // "ApprovalCode":”NS546"

         */
        
        
        let custRefNo = String.createGuid()
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTSaveCustomer)") else { return }
    
        restManager = RestManager()
        
        var allParams = ""
        
        //Content-Type
        restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        
        //CustRefNo
        restManager.httpBodyParameters.add(value: custRefNo, forKey: KCustRefNo)
        allParams = "\(KCustRefNo)=\(custRefNo)&"
        
        //Title
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.TitleCode ?? "", forKey: KTitle)
        allParams =  allParams + "\(KTitle)=\(modelNewRegistrationDetals.TitleCode ?? "")&"
        
        //FirstName
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.FirstName ?? "", forKey: KFirstName)
        allParams =  allParams + "\(KFirstName)=\(modelNewRegistrationDetals.FirstName ?? "")&"
        
        //LastName
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.LastName ?? "", forKey: KLastName)
        allParams =  allParams + "\(KLastName)=\(modelNewRegistrationDetals.LastName ?? "")&"
        
        //CountryCode
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.DialCode ?? "", forKey: KCountryCode)
        allParams =  allParams + "\(KCountryCode)=\(modelNewRegistrationDetals.DialCode ?? "")&"
        
        //AreaCode
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.AreaOfResidenceID ?? "", forKey: KAreaCode)
        allParams =  allParams + "\(KAreaCode)=\(modelNewRegistrationDetals.AreaOfResidenceID ?? "")&"
        
        //MobileNo
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.MobileNumber ?? "", forKey: KMobileNo)
        allParams =  allParams + "\(KMobileNo)=\(modelNewRegistrationDetals.MobileNumber ?? "")&"
        
        //ShareID
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.ShareId ?? "", forKey: KShareID)
        //allParams =  allParams + "\(KShareID)=\(modelNewRegistrationDetals.ShareId ?? "")&"
        
        //Email
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.EmailAddress ?? "", forKey: KEmail)
        allParams =  allParams + "\(KEmail)=\(modelNewRegistrationDetals.EmailAddress ?? "")&"
        
        //RequestTimeStamp
        restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
        
        //AppKey
        restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)"
        
        //UserAgent
        restManager.httpBodyParameters.add(value: KUserAgentValue_Mobile, forKey: KUserAgent)
        
        //WebMallID
        restManager.httpBodyParameters.add(value: KWebMallIDValue_Blank, forKey: KWebMallID)
        
        //PrivacyPolicy
        restManager.httpBodyParameters.add(value: "Y", forKey: KPrivacyPolicy)
       
        //Nationality
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.NationalityID ?? "", forKey: KNationality)
      
        //VersionNo
        restManager.httpBodyParameters.add(value: KVersionNoValue_3, forKey: KVersionNo)
      
        //CountryResidence
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.CountryResidenceID ?? "", forKey: KCountryResidence)
      
        //ResidenceStatus
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.ResidentialStatusID ?? "", forKey:
            KResidenceStatus)
        
        //ResidenceCity
        restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.ResidenceCityID ?? "", forKey: KResidenceCity)
        
        //ResidenceArea
         restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.AreaOfResidenceID ?? "", forKey: KResidenceArea)
        
       //ApprovalCode
        restManager.httpBodyParameters.add(value:TFSelectApprovalCode.text ?? "", forKey: KApprovalCode)
        
        //TokenID
        let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
       
        print("**\n\n Final input params URL: \(allParams)  **\n\n")
        
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
       
        restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelNewCustomerCreated = try? decoder.decode(NewCustomerOREntryCreated.self, from: data){
                    print("\n\n ***** API respone Model:\(modelNewCustomerCreated.description) *****\n\n")
                    self.handleAPISaveCustomerRespone(modelNewCustomerCreated:modelNewCustomerCreated)
                    
                }else {
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    
    func handleAPISaveCustomerRespone(modelNewCustomerCreated:NewCustomerOREntryCreated){
        
        if modelNewCustomerCreated.ResponseCode == KAPIResSuccess{
            
            globalRefNo = modelNewCustomerCreated.RefNo ?? ""
             
             //clear previous restManage, re-init this, becuase sometimes without clear this error occur
            restManager = RestManager()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.callAPIPOSTSaveReceipt(modelReceipt: self.modelReceiptArray.first!)
            }
            
              
            
        }else {
           showAlertOnMainThread(title: KCCSELFSERVICE, msg:modelNewCustomerCreated.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            
        }
    }
    
    // <RK 2020-05-07> Hint: if Receipt more than 1 then hit api callAPIPOSTSaveReceipt via Loop for all receipts one by one. As discussion with Hariom & Peram
    func callAPIPOSTSaveReceipt(modelReceipt:ReceiptArray){
        
        /*
       
         "EntryRef":"5b9170f6-d2d6-45e9-af7a-c92acfb34b1a”,// RefNo respone parm of save customer API
         "RequestTimeStamp":"2020-04-21 11:51:25”,
         "AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a”,
         "MallID":"1”,  // dynamic in receipt entry
         "StoreID":"2”,  dynamic in store id
         "InvoiceNo":"1234”,
         "InvoiceValue":"500.000”,
         "InvoiceDate":"21 Apr 2020”, (as per hariom) but as per Peram > (2020-04-20) yyyy-mm-dd
         "InvoiceRefNo":"B83026A1-6785-4E52-8984-0807C7D3324A”, // generate randomly as salesworx
         "FileID":"B5EB9323-7C5C-45E5-97DA-748474C3B4A3”, // same Radom number
         "TokenID":"on/juuWlYXDzhHJHxbUcLEG5AeqDDWPt0YFGnVzC+y4=“,
         "PayMode":”OTHERS"
         
         Hint toeken id checksum =
         EntryRef
         RequestTimeStamp
         AppKey
         MallID
         StoreID
         InvoiceNo
         InvoiceValue
         InvoiceDat
         InvoiceRefNo
         FileID

         */
        
        
        let invoiceRefNo = String.createGuid()
        let fileID = String.createGuid()
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTSaveReceipt)") else { return }
    
     
        
        var allParams = ""
        
        
        //Content-Type
        restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        
        //EntryRef
        restManager.httpBodyParameters.add(value: globalRefNo, forKey: KEntryRef)
        allParams = "\(KEntryRef)=\(globalRefNo)&"
        
        //RequestTimeStamp
        restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
        
        //AppKey
        restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)&"
        
        
        //MallID
        restManager.httpBodyParameters.add(value: modelReceipt.MallID ?? "", forKey: KMallID)
        allParams =  allParams + "\(KMallID)=\(modelReceipt.MallID ?? "")&"
        
        //StoreID
        restManager.httpBodyParameters.add(value: modelReceipt.StoreID ?? "", forKey: KStoreID)
        allParams =  allParams + "\(KStoreID)=\(modelReceipt.StoreID ?? "")&"
        
        //InvoiceNo
        restManager.httpBodyParameters.add(value: modelReceipt.InvoiceNumber ?? "", forKey: KInvoiceNo)
        allParams =  allParams + "\(KInvoiceNo)=\(modelReceipt.InvoiceNumber ?? "")&"
        
        
        //InvoiceValue
        restManager.httpBodyParameters.add(value: modelReceipt.InvoiceValue ?? "", forKey: KInvoiceValue)
        allParams =  allParams + "\(KInvoiceValue)=\(modelReceipt.InvoiceValue ?? "")&"
        
        //InvoiceDate
        restManager.httpBodyParameters.add(value: modelReceipt.InvoiceDateStrAPIFormat ?? "", forKey: KInvoiceDate)
        allParams =  allParams + "\(KInvoiceDate)=\(modelReceipt.InvoiceDateStrAPIFormat ?? "")&"
        
        //InvoiceRefNo
        restManager.httpBodyParameters.add(value: invoiceRefNo, forKey: KInvoiceRefNo)
        allParams =  allParams + "\(KInvoiceRefNo)=\(invoiceRefNo)&"
        
        //FileID
        restManager.httpBodyParameters.add(value: fileID, forKey: KFileID)
        allParams =  allParams + "\(KFileID)=\(fileID)"
        
        //PayMode
        restManager.httpBodyParameters.add(value: modelReceipt.PaymentModeStr ?? "", forKey: KPayMode)
        
        //TokenID
        let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
       
        print("**\n\n Final input params URL: \(allParams)  **\n\n")
        
        print("\n\n **** Calling API: \(url.absoluteString)\ntotalAPISaveReceiptHitCount: \(totalAPISaveReceiptHitCount) \nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
       
        restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelNewReceiptCreated = try? decoder.decode(NewReceiptCreated.self, from: data){
                    print("\n\n ***** API respone Model:\(modelNewReceiptCreated.description) *****\n\n")
                    
                    
                    self.totalAPISaveReceiptHitCount = self.totalAPISaveReceiptHitCount+1
                    
                    print("\n\n *** Hint totalAPISaveReceiptHitCount: \(self.totalAPISaveReceiptHitCount) **\n\n")
                    if self.totalAPISaveReceiptHitCount == self.modelReceiptArray.count{
                        self.handleAPISaveReceiptRespone(modelNewReceiptCreated:modelNewReceiptCreated)
                    }else {
                        
                        if self.totalAPISaveReceiptHitCount < self.modelReceiptArray.count {
                            self.callAPIPOSTSaveReceipt(modelReceipt: self.modelReceiptArray[self.totalAPISaveReceiptHitCount])
                        }
                    }
                    
                  
                }else {
                     self.changeButtonsState(isEnable: true)
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                self.changeButtonsState(isEnable: true)
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    
    func handleAPISaveReceiptRespone(modelNewReceiptCreated:NewReceiptCreated){
        
        if modelNewReceiptCreated.ResponseCode == KAPIResSuccess{
            
            changeButtonsState(isEnable: false)
            //clear previous restManage, re-init this, becuase sometimes without clear this error occur
            restManager = RestManager()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.callAPIPOSTConfirmEntry()
            }
            
            
            
        }else {
           changeButtonsState(isEnable: true)
           showAlertOnMainThread(title: KCCSELFSERVICE, msg:modelNewReceiptCreated.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            
        }
    }
    
    
    
    func callAPIPOSTConfirmEntry(){
            
            /*
           
             "EntryRef":"5b9170f6-d2d6-45e9-af7a-c92acfb34b1a”,
             "RequestTimeStamp":"2020-04-21 11:51:27”,
             "AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a”,
             "TokenID":"sP+gwFBkBU5XMweYxqlPeiXNqi9YRiA+DEbIWMTpNIg=“,
             "CampaignList":"392" // in last screen campaign id

             
             Hint toeken id checksum =
             EntryRef
             RequestTimeStamp
             AppKey
            
             */
            
            let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
            
            guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSConfirmEntry)") else { return }
         
            var allParams = ""
            
            
            //Content-Type
            restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
            
            //EntryRef
            restManager.httpBodyParameters.add(value: globalRefNo, forKey: KEntryRef)
            allParams = "\(KEntryRef)=\(globalRefNo)&"
            
            //RequestTimeStamp
            restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
            allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
            
            //AppKey
            restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
            allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)"
            
            //CampaignList
            restManager.httpBodyParameters.add(value: modelNewRegistrationDetals.CampaignID ?? "", forKey: KCampaignList)
                   
           //TokenID
            let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
            restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
           
            print("**\n\n Final input params URL: \(allParams)  **\n\n")
            
            print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
            
            Indicator.shared.showProgressView(self.view)
           
            restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
                
                Indicator.shared.hideProgressView()
                
                guard let response = results.response else { return }
                if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                    guard let data = results.data else { return }
                    let decoder = JSONDecoder()
                    if let modelConfirmEntry = try? decoder.decode(ConfirmEntryForCampaign.self, from: data){
                        print("\n\n ***** API respone Model:\(modelConfirmEntry.description) *****\n\n")
                        self.handleAPIConfirmEntryRespone(modelConfirmEntry:modelConfirmEntry)
                        
                    }else {
                        self.changeButtonsState(isEnable: true)
                        data.handleAPIParsingErrorCase(currentVC: self)
                    }
                    
                }else {
                    self.changeButtonsState(isEnable: true)
                    self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
                }
            }
        }
        
        
    func handleAPIConfirmEntryRespone(modelConfirmEntry:ConfirmEntryForCampaign){
        
        if modelConfirmEntry.ReceiptStatusList.count > 0{
            let model = modelConfirmEntry.ReceiptStatusList.first
            if model?.ResponseCode == KAPIResSuccess{
                
                //handle case whether Campaign Token screen needs to show or not
                
                if modelConfirmEntry.Result == KAPIResSuccess && modelConfirmEntry.CampaignTokenList?.count ?? 0 > 0 {
                    Indicator.shared.showProgressView(self.view)
                    pushToCampaignTokenVC(arrayCampaignToken:modelConfirmEntry.CampaignTokenList!)
                    
                }else {
                    showSuccessMsgForSaveReceipt(msg: model?.ProcessResponse ?? KSaveReceiptAPISucessMsg)
                }
                
                
            }else {
                changeButtonsState(isEnable: true)
                showAlertOnMainThread(title: KError, msg:model?.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            }
        }else {
            changeButtonsState(isEnable: true)
            showAlertOnMainThread(title: KError, msg:KSomethingWentWrongTryAgainLater)
        }
    }
        

      func showSuccessMsgForSaveReceipt(msg:String){
      
        let alertController = UIAlertController(title: KCCSELFSERVICE, message: msg, preferredStyle: .alert)
        let btnOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { action in
           for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: StartVC.self) {
                  DispatchQueue.main.async(execute: {
                     _ =  self.navigationController!.popToViewController(controller, animated: true)
                  })
                  break
                }
            }
        })
        let actionsArray = [btnOK]
        showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
      }
    
    func pushToCampaignTokenVC(arrayCampaignToken:[ArrayCampaignToken]){
        
        let viewController = CampaignTokenVC()
        viewController.modelArrayCampaignToken = arrayCampaignToken
        viewController.modelNewRegistrationDetals = modelNewRegistrationDetals
        
        Indicator.shared.hideProgressView()
        
        DispatchQueue.main.async(execute: {
            self.navigationController?.pushViewController(viewController, animated: true)
        })
    }
    
    }

//MARK: POP UP LIST DELEGATE METHOD
extension ReceiptConfirmationVC :PopupCampaignListDelegate {
    func activeCampaignsMallDidSelect(modelActiveCampaignsMall: ArrayActiveCampaignsByMall, totalSelecteRows: NSInteger, selectedCellArray: [Int]) {
        arraySelectedCells = selectedCellArray
        TFSelectCampaign.text = "\(totalSelecteRows) campaign(s) selected."
       // arrayCampaignMallIDs.append(modelActiveCampaignsMall.CampID ?? "")
    }
}


