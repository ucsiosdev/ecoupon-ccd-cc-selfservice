//
//  SettingsVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 15/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController, UITextFieldDelegate,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var TFCSOAccount: SelfSerticeTextField!
    @IBOutlet var TFSetDefaultMall: SelfSerticeTextField!
    @IBOutlet var btnEnableAllMalls: UISwitch!
   
    var globalSelectedMall : InvoiceMallList?
    let restManagerApprovalCode = RestManager()
    let restManagerMallList = RestManager()
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        handlePrevousSavedData()
    }
    
    //MARK: HELPER METHODS
    func setupView(){
        
        TFCSOAccount.rightView = nil
        btnEnableAllMalls.tintColor = SelfServiceColor().KBlue
        btnEnableAllMalls.isEnabled = false
        btnEnableAllMalls.isSelected = false
        
    }
    
    func handlePrevousSavedData() {
        
        let currentActiveApprovalModel = SINGLETON.currentActiveApproveCode
        if currentActiveApprovalModel?.UserName?.count ?? 0 > 0 && currentActiveApprovalModel?.WorkingMall?.count ?? 0 > 0 {
            TFCSOAccount.text = currentActiveApprovalModel?.UserName
            TFSetDefaultMall.text = currentActiveApprovalModel?.WorkingMall
            
        }
        btnEnableAllMalls.isOn = UserDefaults.standard.bool(forKey: UserDefaultKeys.IS_ENABLE_ALL_MALLS)
    }
    
    //MARK: -TEXT FIELD DELEGATE METHODS
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField ==  TFCSOAccount {
            view.endEditing(true)
            
            let alertController = UIAlertController(title: KCCSELFSERVICE, message: KEneterYourApporovalCodeMsg, preferredStyle: .alert)
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                //textField.placeholder = "Enter Second Name"
                textField.isSecureTextEntry = true
            }
            
            let btnProceed = UIAlertAction(title: NSLocalizedString(KProceed, comment: ""), style: .default, handler: { action in
                
                
                if let alertTextField = alertController.textFields?.first, alertTextField.text != nil {
                    
                    if ( alertTextField.text == nil || alertTextField.text?.count == 0){
                        self.view.makeToast("Please enter your Approval Code, and try again", duration: 2.0, position: .center)
                    }else {
                        self.callAPIValidateApprovalCode(approvalCode:alertTextField.text!)
                    }
                }
            })
            
            
            let btnCancel = UIAlertAction(title: NSLocalizedString(KAlertCancelButtonTitle, comment: ""), style: .destructive, handler: { action in
                
            })
            
            let actionsArray = [btnCancel, btnProceed]
            showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
            return false
        }
        else if textField ==  TFSetDefaultMall {
            
            if TFCSOAccount.text?.count == 0{
                view.makeToast("Please set CSO Account first then try again", duration: 2.0, position: .center)
                TFCSOAccount.shake()
                return false
            }
            checkMallListCases()
            return false
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func checkMallListCases(){
        
        showMallListPopupWithStringArray()
        
        /*
        if (SINGLETON.currentActiveApproveCode == nil ||  SINGLETON.currentActiveApproveCode?.WorkingMall?.count == 0 ){
            callAPIGetInvoiceMallsList()
        }else if  btnEnableAllMalls.isOn {
            callAPIGetInvoiceMallsList()
        }else {
            showMallListPopupWithStringArray()
        }
        */
    }
    
    func showMallListPopupWithStringArray(){
        
        let mallListPopup = GenericStringArrayListPopup()
        mallListPopup.delegate=self
        mallListPopup.navigTitle = "Select Mall"
        mallListPopup.isComingFromSettingVC = true
        var stringArray  = [String]()
        stringArray.append(SINGLETON.currentActiveApproveCode?.WorkingMall ?? "")
        mallListPopup.stringsArray = stringArray
        let controller = UINavigationController(rootViewController: mallListPopup)
        controller.preferredContentSize = CGSize(width: 300, height: 200)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFSetDefaultMall
            popover.sourceRect = TFSetDefaultMall.bounds
            popover.permittedArrowDirections = .down
            self.present(controller, animated: true) {
            }
        }
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnEnableAllMallsValueChanged(_ sender: Any) {
        
    }
    @IBAction func btnCloseClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnUpdateClicked(_ sender: Any) {
        
        updateMallDetails()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRestClicked(_ sender: Any) {
        clearAllPreviousSaved()
        navigationController?.popViewController(animated: true)
    }
    
    func clearAllPreviousSaved(){
        
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.CURRENT_ACTIVE_APPROVE_CODE_DETAILS)
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.IS_ENABLE_ALL_MALLS)
        SINGLETON.currentActiveApproveCode = nil
    }
}


//MARK: API CALL METHODS
extension SettingsVC {
    
    func callAPIValidateApprovalCode(approvalCode:String){
        
        /*
         Excellent article for learn / demo for API
         https://www.appcoda.com/restful-api-library-swift/
         
         let HMAC_sha256_Value = "ApprovalCode=NS546&RequestTimeStamp=2020-04-17 12:04:33&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a".hmac(algorithm: .sha256, key: "1ae62e36396109bf379788556159525c1a3afa70c67ea5fb5b1a63c7f7484573")
         
         Base64 result via online site which is tested in working: Site: https://www.devglan.com/online-tools/hmac-sha256-online
         For example:
         input: ApprovalCode=NS546&RequestTimeStamp=2020-04-17 12:04:33&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a
         Output (Base64) : Oqzps6tAaEo3LtcWnNOl9xC7jKEYcqNcS96ApuGUF1Y=
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLSettings_ValidateApprovalCode)") else { return }
        
        restManagerApprovalCode.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        restManagerApprovalCode.httpBodyParameters.add(value: approvalCode, forKey: KApprovalCode)
        restManagerApprovalCode.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        restManagerApprovalCode.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        
        let HMAC_sha256_Value = "\(KApprovalCode)=\(approvalCode)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        
        restManagerApprovalCode.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManagerApprovalCode.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerApprovalCode.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let currentApprovalCode = try? decoder.decode(CurrentActiceApprovalCode.self, from: data){
                    print("\n\n ***** API respone Model:\(currentApprovalCode.description) *****\n\n")
                    self.handleAPIApprovalCodeRespone(currentApprovalCode:currentApprovalCode)
                }else {
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    
    func handleAPIApprovalCodeRespone(currentApprovalCode:CurrentActiceApprovalCode){
        
        if currentApprovalCode.ResponseCode == KAPIResSuccess{
            
            // Save API respone in UserDefaults & SINGLETON for future use
            let encoder = JSONEncoder()
            if let userData = try? encoder.encode(currentApprovalCode){
                UserDefaults.standard.set(userData, forKey: UserDefaultKeys.CURRENT_ACTIVE_APPROVE_CODE_DETAILS)
                SINGLETON.currentActiveApproveCode = currentApprovalCode
            }else {
                print("\n\n **** WARNING: Error during encoding ****\n\n")
            }
            print("\n\n  *** SINGLETON.currentActiveApproveCode: \(SINGLETON.currentActiveApproveCode ??  CurrentActiceApprovalCode())   ***\n\n")
            
            //avoid crash UI task must done on main thread.
            DispatchQueue.main.async(execute: {
                self.TFCSOAccount.text = currentApprovalCode.UserName ?? ""
                self.TFSetDefaultMall.text = currentApprovalCode.WorkingMall ?? ""
            })
            
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: KDefaultCSOAccoutSetMsg)
            
        }else {
           showAlertOnMainThread(title: KCCSELFSERVICE, msg:currentApprovalCode.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
            
        }
    }
    
    func callAPIGetInvoiceMallsList(){
        
        /*
         input params:
         RequestTimeStamp
         AppKey
         TokenID
         
         format > "RequestTimeStamp=""&AppKey=""
         */
        
        
        /*
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLSettings_GetInvoiceMallsList)") else { return }
        restManager.urlQueryParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        restManager.urlQueryParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        restManager.urlQueryParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        let HMAC_sha256_Value = "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        let queryParams: [String: String] = [
            KRequestTimeStamp: currentDateTimeInUTC,
            KAppKey : KAppKeyDefaultTestApp,
            KTokenID: HMAC_sha256_Value
        ]

        var urlComponents = URLComponents()
        urlComponents.setQueryItems(with: queryParams)
       
        var completeURLString = "\(KCurrentAPIBaseURL)\(KAPIURLSettings_GetInvoiceMallsList)" + (urlComponents.url?.absoluteString ?? "")
        
        completeURLString = completeURLString.stringReplacePlusSign()
        
        
        guard let finalURL = URL(string:completeURLString) else {return}
        
        print("\n\n **** Calling API: \(finalURL)\n\nInput Params: \(urlComponents.queryItems.debugDescription) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        //when you hit POST API after that GET api then must re-alloc (init) or use new instance of RestManager() object, else api will not give reponse properly
       
        
        restManagerMallList.makeRequest(toURL: finalURL, withHttpMethod: .get,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let mallListModel = try? decoder.decode(GetInvoiceMallList.self, from: data){
                    
                    print("\n\n ***** API respone Model:\(mallListModel.description) *****\n\n")
                    
                    //avoid crash UI task must done on main thread.
                    DispatchQueue.main.async(execute: {
                        self.handleAPIInvoiceMallsListRespone(mallListModel:mallListModel)
                    })
                }else {
                   data.handleAPIParsingErrorCase(currentVC: self)
                }
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    func handleAPIInvoiceMallsListRespone(mallListModel:GetInvoiceMallList){
        
        if mallListModel.ResponseCode == KAPIResSuccess{
            showMallListPopupWithModelArray(modelArray: mallListModel.ListInvoiceMall)
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: mallListModel.Result ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    
    func showMallListPopupWithModelArray(modelArray:[InvoiceMallList]){
        
        var arrayTemp = modelArray
        //remove not usefult index from API Respone array
        if let matchedOffset = modelArray.index(where: {$0.MallName == K_PleaseSelect_}) {
            arrayTemp.remove(at: matchedOffset)
        }
        
        if arrayTemp.count == 0{
            self.view.makeToast("Mall list not available, Please try again later.", duration: 2.0, position: .center)
            return
        }
        
        let mallListPopup = GenericModelArrayListPopup()
        mallListPopup.delegate=self
        mallListPopup.navigTitle = "Select Mall"
        mallListPopup.contentArray = arrayTemp as [AnyObject]
        mallListPopup.isComingFromSettingVC = true
        let controller = UINavigationController(rootViewController: mallListPopup)
        controller.preferredContentSize = CGSize(width: 300, height: 235)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = TFSetDefaultMall
            popover.sourceRect = TFSetDefaultMall.bounds
            popover.permittedArrowDirections = .down
            self.present(controller, animated: true) {
            }
        }
    }
    
    func updateMallDetails(){
        
        //update user choice in UserDefaults & SINGLETON for future use
        if SINGLETON.currentActiveApproveCode != nil && globalSelectedMall != nil  {
            
            var currentApprovalCode = SINGLETON.currentActiveApproveCode!
            currentApprovalCode.MallID = String(globalSelectedMall?.MallID ?? -1)
            currentApprovalCode.WorkingMall = globalSelectedMall?.MallName ?? ""
            let encoder = JSONEncoder()
            if let userData = try? encoder.encode(currentApprovalCode){
                UserDefaults.standard.set(userData, forKey: UserDefaultKeys.CURRENT_ACTIVE_APPROVE_CODE_DETAILS)
                SINGLETON.currentActiveApproveCode = currentApprovalCode
            }else {
                print("\n\n **** WARNING: Error during encoding ****\n\n")
            }
            
            print("\n\n  *** Updated SINGLETON.currentActiveApproveCode: \(SINGLETON.currentActiveApproveCode ??  CurrentActiceApprovalCode())   ***\n\n")
        }
        
        UserDefaults.standard.set(btnEnableAllMalls.isOn, forKey: UserDefaultKeys.IS_ENABLE_ALL_MALLS)
        
    }
}

//MARK: LIST POP UP DELEGATE METHOD | STRING ARRAY
extension SettingsVC :GenericStringArrayListPopupDelegate {
    
    
    func mallDidSelectWorkingMall() {
        print("\n\n **** mallDidSelectWorkingMall  ***\n\n")
        if (SINGLETON.currentActiveApproveCode != nil &&  SINGLETON.currentActiveApproveCode?.WorkingMall?.count ?? 0 > 0 ){
            TFSetDefaultMall.text = SINGLETON.currentActiveApproveCode?.WorkingMall ?? ""
        }
    }
    
    /*
    func mallPopupDidSelectAMall(currentApprovalCode: CurrentActiceApprovalCode) {
        print("\n\n **** mallPopupDidSelectAMall  ***\n\n")
        TFSetDefaultMall.text = currentApprovalCode.WorkingMall
    }
    */
}

//MARK: LIST POP UP DELEGATE METHOD | MODEL ARRAY
extension SettingsVC :GenericModelArrayListPopupDelegate {
    
    
    func activeCampaignMallDidSelect(modelActiveCampaignMall: ArrayActiveCampaignsByMall) {
        
    }
    
    
    
    func storeDidSelect(modelStore: StoreListaArray) {
        
    }
    
    
    func paymentModeSelectedPayment(modelPayment: PaymentListaArray) {
        
    }
    
    func mallPopupDidSelectedMall(selectedMall: InvoiceMallList) {
        
        print("\n\n ***   mallPopupDidSelectedMall   ***\n\n")
        TFSetDefaultMall.text = selectedMall.MallName
        
        //save model, and use this model when user clicks on update button
        globalSelectedMall = selectedMall
    }
    
    func areaSelected(modelArea: AreaListaArray) {
        
    }
    
    func cityEmirateSelected(modelCity: CityListaArray) {
        
    }
    
    func nationlitySelected(index: NSInteger) {
        
    }
    
    func residenceCounrySelected(index: NSInteger) {
        
    }
    
    func residentialStatusSelected(model: ResidentialStatusArray) {
        
    }
    
    func nameTitlePopupDidSelectedTitle(index: NSInteger) {
        
    }
    
    func countryListDidSelectCountry(rowNumber: NSInteger) {
        
    }
    
    func countryListPopupDidClose() {
        
    }
}


//MARK: SINGLETON
class Singleton{
    
    static let SharedInstance = Singleton()
    
    init(){
        
        if let userData = UserDefaults.standard.value(forKey: UserDefaultKeys.CURRENT_ACTIVE_APPROVE_CODE_DETAILS)as? NSData{
            
            let decoder = JSONDecoder()
            if let result = try? decoder.decode(CurrentActiceApprovalCode.self, from: userData as Data){
                currentActiveApproveCode = result
            }else{
                print("\n\n **** WARNING: Error during encoding ****\n\n")
            }
            print("\n\n *** SINGLETON: currentActiveApproveCode: \(currentActiveApproveCode ??  CurrentActiceApprovalCode())  **\n\n")
            
        }
        /*
         if let userAuthData = UserDefaults.standard.value(forKey: AppConstants.UserDefaultKeys.USER_AUTH)as? NSData{
           let decoder = JSONDecoder()
           userAuthModel = try! decoder.decode(AuthToken.self, from: userAuthData as Data)
         }
         */
    }
    
    lazy var currentActiveApproveCode :CurrentActiceApprovalCode? = nil
    // lazy var userAuthModel: AuthToken? = nil
}


