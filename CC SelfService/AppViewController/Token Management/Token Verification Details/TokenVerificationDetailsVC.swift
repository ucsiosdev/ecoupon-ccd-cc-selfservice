//
//  TokenVerificationDetailsVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 11/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class TokenVerificationDetailsVC: UIViewController {
    
    @IBOutlet var viewParentDetails: UIView!
    @IBOutlet var lblCustomerName: SelfServiceLabelBook_18!
    @IBOutlet var lblCustomerMobileNumber: SelfServiceLabelBook_18!
    @IBOutlet var lblCampaign: SelfServiceLabelBook_18!
    @IBOutlet var lblTokenNo: SelfServiceLabelBook_18!
    @IBOutlet var lblToeknDate: SelfServiceLabelBook_18!
    
    var modelValidateEToken = ValidateEToken()
    var restManager = RestManager()
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setCustomerDetails()
        
    }
    
    
    func setupView(){
        viewParentDetails.backgroundColor = .white
    }
    
    func setCustomerDetails(){
        
        lblCustomerName.text =  modelValidateEToken.CustName ?? ""
        lblCustomerMobileNumber.text = "+\(modelValidateEToken.MobileNo ?? "")"
        lblCampaign.text = modelValidateEToken.CampaignName ?? ""
        lblTokenNo.text =  modelValidateEToken.TokenCode ?? ""
        lblToeknDate.text =  modelValidateEToken.IssuedAt ?? ""
    }
    
    
    //MARK: ACTION BUTTON
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: KCCSELFSERVICE, message: "Do you really want to exit?", preferredStyle: .alert)
        
        let btnExit = UIAlertAction(title: NSLocalizedString("Exit", comment: ""), style: .default, handler: { action in
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: StartVC.self) {
                  DispatchQueue.main.async(execute: {
                     _ =  self.navigationController!.popToViewController(controller, animated: true)
                  })
                  break
                }
            }
        })
        
        let btnCancel = UIAlertAction(title: NSLocalizedString(KAlertCancelButtonTitle, comment: ""), style: .destructive, handler: { action in
            
        })
        
        let actionsArray = [btnCancel, btnExit]
        showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
        
    }
    
    @IBAction func btnGoBackClicked(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMarkTokenAsUsedClicked(_ sender: Any) {
        
        restManager = RestManager()
        callAPIPOSTMarkEToken()
    }
    

    
}

//MARK: API CALL METHODS
extension TokenVerificationDetailsVC{
    
    
    func callAPIPOSTMarkEToken(){
           
           /*
         {
         //"CustomerID":"bfd3c336-0f9b-43b9-8be5-0bdd2140b231",
         //"TokenCode":"Y8Y72F",
         //"CampaignID":"392",
        // "RequestTimeStamp":"2020-04-21 12:26:02",
         //"AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a",
        // "MarkedBy":"397",
        // "TokenID":"Zr4yP1LgsTSpBP7+HbkP2hRhpXSCDkjYwFGOPbRuGQ4="
         }
            
            Hint toeken id checksum =
            
           
            */
           
           let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
           
           guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTMarkEToken)") else { return }
        
           
           var allParams = ""
           
           //Content-Type
           restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
           
           //CustomerID
           restManager.httpBodyParameters.add(value: modelValidateEToken.CustID ?? "", forKey: KCustomerID)
           allParams = "\(KCustomerID)=\(modelValidateEToken.CustID ?? "")&"
           
           //TokenCode
           restManager.httpBodyParameters.add(value: modelValidateEToken.TokenCode ?? "", forKey: KTokenCode)
           allParams =  allParams + "\(KTokenCode)=\(modelValidateEToken.TokenCode ?? "")&"
        
           //CampaignID
           restManager.httpBodyParameters.add(value: modelValidateEToken.CampID ?? "", forKey: KCampaignID)
           allParams =  allParams + "\(KCampaignID)=\(modelValidateEToken.CampID ?? "")&"
        
           //RequestTimeStamp
           restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
           allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
           
           //AppKey
           restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
           allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)&"
           
           //MarkedBy
           restManager.httpBodyParameters.add(value: SINGLETON.currentActiveApproveCode?.UserID ?? "", forKey: KMarkedBy)
           allParams =  allParams + "\(KMarkedBy)=\(SINGLETON.currentActiveApproveCode?.UserID ?? "")"
                  
          //TokenID
           let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
           restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
          
           print("**\n\n Final input params URL: \(allParams)  **\n\n")
           
           print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
           
           Indicator.shared.showProgressView(self.view)
          
           restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
               
               Indicator.shared.hideProgressView()
               
               guard let response = results.response else { return }
               if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                   guard let data = results.data else { return }
                   let decoder = JSONDecoder()
                   if let modelMarkEToekn = try? decoder.decode(NewReceiptCreated.self, from: data){
                       print("\n\n ***** API respone Model:\(modelMarkEToekn.description) *****\n\n")
                       self.handleAPIConfirmEntryRespone(modelMarkEToekn:modelMarkEToekn)
                       
                   }else {
                        data.handleAPIParsingErrorCase(currentVC: self)
                   }
                   
               }else {
                   self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
               }
           }
       }
       
       
    func handleAPIConfirmEntryRespone(modelMarkEToekn:NewReceiptCreated){
        
        if modelMarkEToekn.ResponseCode == KAPIResSuccess{
            
            let alertController = UIAlertController(title: KCCSELFSERVICE, message: modelMarkEToekn.ProcessResponse ?? "Token code successfully marked", preferredStyle: .alert)
            let btnOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { action in
                DispatchQueue.main.async(execute: {
                    let newViewController = AcknowledgementFormVC()
                    newViewController.modelValidateEToken = self.modelValidateEToken
                    self.navigationController?.pushViewController(newViewController, animated: true)
                })
            })
            let actionsArray = [btnOK]
            showConfirmationAlertWithTextFiled(alertController:alertController , actionsArray: actionsArray)
        }
        else {
            showAlertOnMainThread(title: KError, msg: modelMarkEToekn.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
        }
    }

}
