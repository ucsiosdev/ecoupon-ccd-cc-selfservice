//
//  AcknowledgementFormVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 11/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit
import MessageUI

class AcknowledgementFormVC: BaseViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet var TFCustomerName: SelfSerticeTextField!
    @IBOutlet var TFCustomerMobileNumber: SelfSerticeTextField!
    @IBOutlet var TVPrizeTypeAndCost: UITextView!
    @IBOutlet var TFIDNo: SelfSerticeTextField!
    
    let restManagerPrintToken = RestManager()
    var modelValidateEToken = ValidateEToken()
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        setCustomerDetails()
        
        
        
    }
    
    func initObj(){
        
        TFCustomerName.isUserInteractionEnabled = false
        TFCustomerMobileNumber.isUserInteractionEnabled = false
       
        TVPrizeTypeAndCost.layer.borderWidth = 1.5
        let borderColor : UIColor = SelfServiceColor().KGreyBorderColor
        TVPrizeTypeAndCost.layer.borderColor = borderColor.cgColor
        TVPrizeTypeAndCost.layer.masksToBounds = false
        TVPrizeTypeAndCost.layer.cornerRadius = 18
        TVPrizeTypeAndCost.autocorrectionType = .no
        TVPrizeTypeAndCost.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        TVPrizeTypeAndCost.textColor = SelfServiceColor().KLigthOrange
        TVPrizeTypeAndCost.backgroundColor = SelfServiceColor().KTextFieldBGColor
    }
    
    func setCustomerDetails(){
        
        TFCustomerName.rightView = nil
        TFCustomerMobileNumber.rightView = nil
        TFIDNo.rightView = nil
        
        //TVPrizeTypeAndCost.scrollRangeToVisible(NSMakeRange(0, 0))
        TVPrizeTypeAndCost.contentInset = UIEdgeInsets.zero
        TVPrizeTypeAndCost.clipsToBounds = true


        
        TFCustomerName.text =  modelValidateEToken.CustName ?? ""
        TFCustomerMobileNumber.text = "+\(modelValidateEToken.MobileNo ?? "")"
        TVPrizeTypeAndCost.text = "\(modelValidateEToken.PrizeInfo ?? "")"
        TFIDNo.text =  modelValidateEToken.IDNo ?? ""
        
        if TVPrizeTypeAndCost.text?.count == 0 {
            TVPrizeTypeAndCost.isEditable = true
        }else {
             TVPrizeTypeAndCost.isEditable = false
        }
        if TFIDNo.text?.count == 0 {
            TFIDNo.isUserInteractionEnabled = true
        }else {
            TFIDNo.isUserInteractionEnabled = false
        }
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        TVPrizeTypeAndCost.text =  TVPrizeTypeAndCost.text?.trimString()
        TFIDNo.text =  TFIDNo.text?.trimString()
        
        if TVPrizeTypeAndCost.text?.count == 0{
            TVPrizeTypeAndCost.isEditable = true
            view.makeToast("Please enter Prize type & Cost", duration: 2.0, position: .center)
            TVPrizeTypeAndCost.shake()
            return
        }else if TFIDNo.text?.count == 0 {
            TFIDNo.isUserInteractionEnabled = true
            view.makeToast("Please enter ID", duration: 2.0, position: .center)
            TFIDNo.shake()
            return
        }
        else {
            
            /*
             Hint 1:
             in API name: GetETokenDetails if
             AckStatus = Y // No need to call API: PrinteTokenDED
             AckStatus = N // Needs to call API: PrinteTokenDED
             
             Hint 2: In settings screen> appoval code API:
             isDubalmall == "Yes"
             then show both controller which has 2 images (1 in top , 1 in bottom), else show which has "PRIZE RECEIVING DECLARATION" lable
             */
             
             if modelValidateEToken.AckStatus == "Y"{
                if SINGLETON.currentActiveApproveCode?.IsDubaiMall?.uppercased() == "YES"{
                   pushToDubaiMallAcknowledgementVC()
                }else {
                    pushTo_Non_Dubai_MallAcknowledgementVC()
                }
               
            }
            else {
               
                modelValidateEToken.PrizeInfo = TVPrizeTypeAndCost.text!
                modelValidateEToken.IDNo = TFIDNo.text!
               
                callAPIPOSTPrinteTokenDED()
                
                if SINGLETON.currentActiveApproveCode?.IsDubaiMall?.uppercased() == "YES"{
                   pushToDubaiMallAcknowledgementVC()
                }else {
                    pushTo_Non_Dubai_MallAcknowledgementVC()
                }
            }
        }
      }
    
    
    func pushToDubaiMallAcknowledgementVC(){
        
        let viewController = AcknowledgementDubaiDetailsVC()
        viewController.delegate = self
        viewController.modelEToken = modelValidateEToken
        //viewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        //present(viewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func pushTo_Non_Dubai_MallAcknowledgementVC(){
        
        let viewController = AcknowledgementGenricDetailsVC()
        viewController.delegate = self
        viewController.modelEToken = modelValidateEToken
        //viewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        //present(viewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK: API CALL METHODS

extension AcknowledgementFormVC{
    
    func callAPIPOSTPrinteTokenDED(){
        
        /*
         {
         //"TokenCode":"Y8Y72F",
         //"RequestTimeStamp":"2020-04-21 12:27:48",
         //"AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a",
         // "MarkedBy":"397",
         // "TokenID":"IU+aH0KIhZSbmLdC7DAHIMqvXeialvRoHi6dZvTq1rY=",
         // "IDNo":"1234",
         // "PrizeInfo":"test type and 500"
         }
         
         Checkout for token id:
         TokenCode
         RequestTimeStamp
         AppKey
         MarkedBy
         
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTPrinteTokenDED)") else { return }
        
        
        var allParams = ""
        
        //Content-Type
        restManagerPrintToken.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        
        //TokenCode
        restManagerPrintToken.httpBodyParameters.add(value: modelValidateEToken.TokenCode ?? "", forKey: KTokenCode)
        allParams =  allParams + "\(KTokenCode)=\(modelValidateEToken.TokenCode ?? "")&"
        
        //RequestTimeStamp
        restManagerPrintToken.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        allParams =  allParams + "\(KRequestTimeStamp)=\(currentDateTimeInUTC)&"
        
        //AppKey
        restManagerPrintToken.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        allParams =  allParams + "\(KAppKey)=\(KAppKeyDefaultTestApp)&"
        
        
        //MarkedBy
        restManagerPrintToken.httpBodyParameters.add(value: SINGLETON.currentActiveApproveCode?.UserID ?? "", forKey: KMarkedBy)
        allParams =  allParams + "\(KMarkedBy)=\(SINGLETON.currentActiveApproveCode?.UserID ?? "")"
        
        //TokenID
        let HMAC_sha256_Value = allParams.hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        restManagerPrintToken.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        
        //IDNo
        restManagerPrintToken.httpBodyParameters.add(value: modelValidateEToken.IDNo ?? "", forKey: KIDNo)
       
        //PrizeInfo
        restManagerPrintToken.httpBodyParameters.add(value: modelValidateEToken.PrizeInfo ?? "", forKey: KPrizeInfo)
        
        print("**\n\n Final input params URL: \(allParams)  **\n\n")
        
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManagerPrintToken.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManagerPrintToken.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelMarkEToekn = try? decoder.decode(NewReceiptCreated.self, from: data){
                    print("\n\n ***** API respone Model:\(modelMarkEToekn.description) *****\n\n")
                    self.handleAPIPrinteTokenDEDRespone(modelMarkEToekn:modelMarkEToekn)
                    
                }else {
                    data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    
    func handleAPIPrinteTokenDEDRespone(modelMarkEToekn:NewReceiptCreated){
        
        if modelMarkEToekn.ResponseCode == KAPIResSuccess{
            print("\n\n ** Hint: AcknowledgementFormVC > PrinteTokenDED API hit: \(modelMarkEToekn)  **\n\n")
        }
        else {
            showAlertOnMainThread(title: KError, msg: modelMarkEToekn.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
        }
    }
}

//MARK: Acknowledgement DISMISS DELEGATE | GENRIC
extension AcknowledgementFormVC : AcknowledgementGenricDetailsVCDelegate{
   
    func AcknowledgementGenricDetailsDidDismiss() {
        self.navigationController?.popViewController(animated: false)
    }
    
    
}

//MARK: Acknowledgement DISMISS DELEGATE | DUBAI
extension AcknowledgementFormVC : AcknowledgementDubaiDetailsVCDelegate{
    func AcknowledgementDubaiDetailsDidDismiss() {
        self.navigationController?.popViewController(animated: false)
    }
}
  /*
 func generatePDF(){
        
       
        let viewController = AcknowledgementDubaiDetailsVC()
        viewController.modelEToken = modelValidateEToken
        viewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        present(viewController, animated: true, completion: nil)
        
        
        
        
        //let PageTemplate = AcknowledgementDetailsPDFView(frame: CGRect(x: 0, y: 0, width: 768, height: 1024))
        
        //let PageTemplate = AcknowledgementDetailsPDFView.loadFromNibNamed(nibNamed: "AcknowledgementDetailsPDFView", modelEToken: modelValidateEToken)
        
        
        let PageTemplate = AcknowledgementDubaiDetailsVC()
        
        //PageTemplate.selectedCustomer = dicForPDFTemplateView
        // PageTemplate.arrCustomersAgeingReport = customerDuesArray
       // PageTemplate.pageNumber = i + 1
        
        //PageTemplate.updatePdfPgae()
       // PageTemplate.setup(model:modelValidateEToken)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: 0, y: 0, width: 768, height: 1300), nil)
        
        
        UIGraphicsBeginPDFPage()
        let pdfContext = UIGraphicsGetCurrentContext()
        if let pdfContext = pdfContext {
            PageTemplate.view.layer.render(in: pdfContext)
        }
        
        UIGraphicsEndPDFContext()
       
        
        if MFMailComposeViewController.canSendMail() {
            
            
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.navigationBar.tintColor = UIColor.black
            mailController.setSubject("Subject: Test")
            mailController.setMessageBody("Hi,\n Please find the attached statement.", isHTML: false)
           // mailController.addAttachmentData(pdfData  as Data, mimeType: "application/pdf", fileName: "CustomerAgeingReport-\(customerNo).pdf")
            
            mailController.addAttachmentData(pdfData  as Data, mimeType: "application/pdf", fileName: "ShriRam.pdf")
            
            present(mailController, animated: true)
        }
        else{
           
           showAlertOnMainThread(title: KFailureAlertTitle, msg: KUNSupportMailComposer)
            
        }
 
    }
    */
