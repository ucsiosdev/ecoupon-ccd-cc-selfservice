//
//  TblCellAcknowledgement.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 13/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit

class TblCellAcknowledgement: UITableViewCell {

    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDes_Eng: UILabel!
    @IBOutlet var lblDes_Arabic: UILabel!
    @IBOutlet var imgVDivider: UIImageView!
    @IBOutlet var imgVDivder_2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
