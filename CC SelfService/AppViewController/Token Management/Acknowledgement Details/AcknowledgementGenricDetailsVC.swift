//
//  AcknowledgementGenricDetailsVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 12/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit


protocol AcknowledgementGenricDetailsVCDelegate : class{
    
    func AcknowledgementGenricDetailsDidDismiss()
   
}


class AcknowledgementGenricDetailsVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var lblMallName: UILabel!
    @IBOutlet var viewContent: UIView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewNavigBar: UIView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var lblNavigTitle: UILabel!
    @IBOutlet var lblUndersign: UILabel!
    
    let origImage = UIImage(named: "dots-separator")
    var tintedImage: UIImage?
    
    var modelEToken = ValidateEToken()
    var modelArrayTblData = [AcknowledgementTblData]()
    weak var delegate : AcknowledgementGenricDetailsVCDelegate?
    
    /*
     class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil,modelEToken:ValidateEToken) -> UIView? {
     let modelEToken =  modelEToken
     print("\n\n *** modelEToken: \(modelEToken) ***\n\n")
     return UINib(
     nibName: nibNamed,
     bundle: bundle
     ).instantiate(withOwner: nil, options: nil)[0] as? UIView
     }
     */
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        setupView()
        addModelDataForTableView()
    }
    
    func initObj(){
        
        btnDone.titleLabel?.font = SelfServiceFont().NavigationBarButtonItemFont
        lblNavigTitle.font = SelfServiceFont().NavigBarTitleFont
        viewContent.dropShadowWithOutCornerRadius(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        
        let timestampStr = "\(Int(NSDate().timeIntervalSince1970 * 1000))"
        lblNavigTitle.text = "acknowledgement_\(timestampStr)"
        lblNavigTitle.textColor = .black
        
        
        self.view.backgroundColor = SelfServiceColor().KTextFieldBGColor
        viewNavigBar.backgroundColor = .white
        viewContent.backgroundColor = .white
        
        tblView.tableFooterView = UIView()
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 55
        tblView.register(UINib(nibName: "TblCellAcknowledgement", bundle: nil), forCellReuseIdentifier: "TblCellAcknowledgement")
    }
    
    func setupView(){
        
        if (SINGLETON.currentActiveApproveCode?.WorkingMall != nil && SINGLETON.currentActiveApproveCode?.WorkingMall?.count ?? 0 > 0 ) {
            let mallName = SINGLETON.currentActiveApproveCode!.WorkingMall!
            lblMallName.text = mallName
            lblUndersign.text = "I, the undersigned, hereby declare that I received the prize mentioned above from \(mallName)"
        }
        
        
    }
    
    func addModelDataForTableView(){
        
        //1 License No
        var model = AcknowledgementTblData()
        model.Title = "License No"
        
        if let value = SINGLETON.currentActiveApproveCode?.LicenseNo {
            model.Des_Eng = value
        }else {
            model.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model)
        
        //2 Permit no
        var model_2 = AcknowledgementTblData()
        model_2.Title = "Permit No"
        
        if let value = modelEToken.PermitNo {
            model_2.Des_Eng = value
        }else {
            model_2.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_2)
        
        //3 Prize type & cost
        var model_3 = AcknowledgementTblData()
        model_3.Title = "Prize type & cost"
        
        if let value = modelEToken.PrizeInfo {
            model_3.Des_Eng = value
        }else {
            model_3.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_3)
        
        //4 Winner's name
        var model_4 = AcknowledgementTblData()
        model_4.Title = "Winner's name"
        
        if let value = modelEToken.CustName {
            model_4.Des_Eng = value
        }else {
            model_4.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_4)
        
        //5 Coupon No
        var model_5 = AcknowledgementTblData()
        model_5.Title = "Coupon No"
        
        if let value = modelEToken.TokenCode {
            model_5.Des_Eng = value
        }else {
            model_5.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_5)
        
        
        //6 ID No
        var model_6 = AcknowledgementTblData()
        model_6.Title = "ID No"
        
        if let value = modelEToken.IDNo {
            model_6.Des_Eng = value
        }else {
            model_6.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_6)
        
        
        //7 Email
        var model_7 = AcknowledgementTblData()
        model_7.Title = "Email"
        
        if let value = modelEToken.Email {
            model_7.Des_Eng = value
        }else {
            model_7.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_7)
        
        
        //8 Mobile No
        var model_8 = AcknowledgementTblData()
        model_8.Title = "Mobile No"
        
        if let value = modelEToken.MobileNo {
            model_8.Des_Eng = "+\(value)"
        }else {
            model_8.Des_Eng = "N/A"
        }
        
        modelArrayTblData.append(model_8)
        
        //9 Date
        var model_9 = AcknowledgementTblData()
        model_9.Title = "Date"
        model_9.Des_Eng =  String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kdisplayDateFormatWithoutTime)
        
        modelArrayTblData.append(model_9)
        
        //10 Signature
        var model_10 = AcknowledgementTblData()
        model_10.Title = "Signature"
        model_10.Des_Eng = ""
        modelArrayTblData.append(model_10)
        
        //11 Prize handed over by
        var model_11 = AcknowledgementTblData()
        model_11.Title = " Prize handed over by"
        model_11.Des_Eng = ""
        modelArrayTblData.append(model_11)
        
        tblView.reloadData()
    }
    
    //MARK: ACTION BUTTON
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
           if controller.isKind(of: TokenVerificationVC.self) {
               DispatchQueue.main.async(execute: {
                   _ =  self.navigationController?.popToViewController(controller, animated: true)
               })
               break
           }
        }
        
        
        
        /*
        self.navigationController?.popViewController(animated: true)
        if let _ = self.delegate?.AcknowledgementGenricDetailsDidDismiss(){
            print ("\n\n *** is Delegate Method > AcknowledgementGenricDetailsDidDismiss Avaialble: YES  ***\n\n")
        }*/
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
       
        //for creating clear & clean PDF
        self.viewNavigBar.isHidden = true
        self.btnDone.isHidden = true
        self.btnShare.isHidden = true
        self.view.backgroundColor = .white
        
        let pdfFileName = "\(lblNavigTitle.text ?? "acknowledgement").pdf"
        let testPath = self.view.exportAsPdfFromView(fileName:pdfFileName)
        let url = NSURL.fileURL(withPath: testPath)
        
        self.sharePdf(path: url)
    }
    
    func sharePdf(path:URL) {
        
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: path.path) {
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
            // activityViewController.popoverPresentationController?.sourceRect = self.btnShare.bounds
            activityViewController.popoverPresentationController?.sourceView = btnShare
            activityViewController.popoverPresentationController?.sourceRect = btnShare.bounds
            activityViewController.setValue("\(lblNavigTitle.text ?? "acknowledgement").pdf", forKey: "subject")
            
            
            DispatchQueue.main.async(execute: {
                self.viewNavigBar.isHidden = false
                self.btnDone.isHidden = false
                self.btnShare.isHidden = false
                self.view.backgroundColor = SelfServiceColor().KTextFieldBGColor
                self.present(activityViewController, animated: true, completion: nil)
            })
        } else {
            print("\n\n *** WARNING: document was not found ****\n\n")
            self.showAlertOnMainThread(title: KFailureAlertTitle, msg: KNotAbleShareFileMsg)
            
        }
    }
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArrayTblData.count
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblCellAcknowledgement", for: indexPath) as! TblCellAcknowledgement
        cell.selectionStyle = .none

        let model = modelArrayTblData[indexPath.row]
        
        cell.lblTitle.text = model.Title
        cell.lblDes_Eng.text = model.Des_Eng
       // cell.lblDes_Arabic.text = model.Title
        
        cell.imgVDivider.image = tintedImage
        cell.imgVDivider.tintColor = .black
        
        //cell.imgVDivder_2.image = tintedImage
        //cell.imgVDivder_2.tintColor = .black
        return cell
        
    }
}
