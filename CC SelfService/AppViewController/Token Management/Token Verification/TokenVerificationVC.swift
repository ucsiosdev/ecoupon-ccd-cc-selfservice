

//
//  TokenVerificationVC.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 08/05/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit
import BarcodeScanner

class TokenVerificationVC: BaseViewController {

    @IBOutlet var btnScanQRCode: UIButton!
    @IBOutlet var TFTokenCode: SelfSerticeTextField!
    var restManager = RestManager()
    
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

       TFTokenCode.rightView = nil
       btnScanQRCode.imageView?.contentMode = . scaleAspectFit

    }
    
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController {
      let viewController = BarcodeScannerViewController()
      viewController.codeDelegate = self
      viewController.errorDelegate = self
      viewController.dismissalDelegate = self
      viewController.cameraViewController.showsCameraButton = true
     // viewController.cameraViewController.flashButton.isHidden = false
      return viewController
    }
    
    //MARK: ACTION BUTTON
    @IBAction func btnScanQRCodeClicked(_ sender: Any) {
        view.endEditing(true)
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        viewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        DispatchQueue.main.async(execute: {
            self.present(viewController, animated: true, completion: nil)
        })
    }
    
    @IBAction func btnVerifyClicked(_ sender: Any) {
        
         TFTokenCode.text = TFTokenCode.text?.trimString()
         
         if TFTokenCode.text?.count == 0{
            view.makeToast("Enter your token code", duration: 2.0, position: .center)
            TFTokenCode.shake()
            return
         }
         else {
            view.endEditing(true)
            callAPIPOSTValidateEToken()
        }
    }
    
    @IBAction func btnPrintReceiptClicked(_ sender: Any) {
        
        TFTokenCode.text = TFTokenCode.text?.trimString()
         
         if TFTokenCode.text?.count == 0{
            view.makeToast("Enter your token code", duration: 2.0, position: .center)
            TFTokenCode.shake()
            return
         }
         else {
             view.endEditing(true)
             callAPIPOSTGetETokenDetails()
        }
        
       
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
     
   
}
//MARK: API CALL METHOD
extension TokenVerificationVC{
    
    func callAPIPOSTValidateEToken(){
        
        /*
         TokenCode":"ns547",
         "RequestTimeStamp":"2020-05-08 08:22:20",
         "AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a",
         "TokenID":"QJk0LzeRa6dQAXm+eRXRmjCwh9X/eNjgvWpINBSeWyM="}
         */
        
        let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
        
        guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTValidateEToken)") else { return }
        
        restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
        restManager.httpBodyParameters.add(value: TFTokenCode.text!, forKey: KTokenCode)
        restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
        restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
        
        let HMAC_sha256_Value = "\(KTokenCode)=\(TFTokenCode.text!)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
        
        
        restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
        print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
        
        Indicator.shared.showProgressView(self.view)
        
        restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
            
            Indicator.shared.hideProgressView()
            
            guard let response = results.response else { return }
            if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                guard let data = results.data else { return }
                let decoder = JSONDecoder()
                if let modelValidateEToken = try? decoder.decode(ValidateEToken.self, from: data){
                    print("\n\n ***** API respone Model:\(modelValidateEToken.description) *****\n\n")
                    self.handleAPIValidateETokenRespone(modelValidateEToken:modelValidateEToken)
                }else {
                     data.handleAPIParsingErrorCase(currentVC: self)
                }
                
            }else {
                self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
            }
        }
    }
    
    
    
    func handleAPIValidateETokenRespone(modelValidateEToken:ValidateEToken){
        
        if modelValidateEToken.ResponseCode == KAPIResSuccess{
            DispatchQueue.main.async(execute: {
                let newViewController = TokenVerificationDetailsVC()
                newViewController.modelValidateEToken = modelValidateEToken
                self.navigationController?.pushViewController(newViewController, animated: true)
            })
            
            
            
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelValidateEToken.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
        }
    }
    
    func callAPIPOSTGetETokenDetails(){
         
         /*
          {"TokenCode":"STL2M5",
         "RequestTimeStamp":"2020-04-21 12:05:17",
         "AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a",
         "TokenID":"p+6V7Fz0HMB2RkS78pt716osluS+tl8UH8bFff6yCAI="
          }
          */
         
         let currentDateTimeInUTC = String.fetchStringfromDateInUTC(date: Date.init(), requiredFormat: kDatabaseDateFormatWithTime)
         
         guard let url = URL(string: "\(KCurrentAPIBaseURL)\(KAPIURLPOSTGetETokenDetails)") else { return }
         
         restManager = RestManager()
        
         restManager.requestHttpHeaders.add(value: Kapplication_json, forKey: KContent_Type)
         restManager.httpBodyParameters.add(value: TFTokenCode.text!, forKey: KTokenCode)
         restManager.httpBodyParameters.add(value: currentDateTimeInUTC, forKey: KRequestTimeStamp)
         restManager.httpBodyParameters.add(value: KAppKeyDefaultTestApp, forKey: KAppKey)
         
         let HMAC_sha256_Value = "\(KTokenCode)=\(TFTokenCode.text!)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(KAppKeyDefaultTestApp)".hmac(algorithm: .sha256, key: KSecretKeyDefaultTestApp)
         
         
         restManager.httpBodyParameters.add(value: HMAC_sha256_Value, forKey: KTokenID)
         print("\n\n **** Calling API: \(url.absoluteString)\nInput Params: \(self.restManager.httpBodyParameters) ****\n\n")
         
         Indicator.shared.showProgressView(self.view)
         
         restManager.makeRequest(toURL: url, withHttpMethod: .post,currentVC:self) { (results) in
             
             Indicator.shared.hideProgressView()
             
             guard let response = results.response else { return }
             if response.httpStatusCode == SelfServiceAPIResponseCode().K200_OK {
                 guard let data = results.data else { return }
                 let decoder = JSONDecoder()
                 if let modelGetToken = try? decoder.decode(ValidateEToken.self, from: data){
                     print("\n\n ***** API respone Model:\(modelGetToken.description) *****\n\n")
                     self.handleAPIPOSTValidateETokenRespone(modelGetToken:modelGetToken)
                 }else {
                      data.handleAPIParsingErrorCase(currentVC: self)
                 }
                 
             }else {
                 self.showAlertOnMainThread(title: KCCSELFSERVICE, msg: KSomethingWentWrongTryAgainLater)
             }
         }
     }
    
    func handleAPIPOSTValidateETokenRespone(modelGetToken:ValidateEToken){
        
        if modelGetToken.ResponseCode == KAPIResSuccess{
            DispatchQueue.main.async(execute: {
                let newViewController = AcknowledgementFormVC()
                newViewController.modelValidateEToken = modelGetToken
                self.navigationController?.pushViewController(newViewController, animated: true)
            })
        }else {
            showAlertOnMainThread(title: KCCSELFSERVICE, msg: modelGetToken.ProcessResponse ?? KSomethingWentWrongTryAgainLater)
        }
    }
}

//MARK: QR CODE DELEGATE METHOD
extension TokenVerificationVC : BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate{
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("\n\n **** Barcode Data: \(code)   ***\n\n")
        print("**** Symbology Type: \(type)   ***\n\n")
        
        DispatchQueue.main.async {
            self.TFTokenCode.text = code
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
           controller.dismiss(animated: true, completion: nil)
        }
        
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            controller.resetWithError()
        }*/
    }
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
        
        showAlertOnMainThread(title: KError, msg: "Unable to scan this QR code, Please enter manually your token code and try again.")
        
    }
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
