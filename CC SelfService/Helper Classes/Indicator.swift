//
//  Indicator.swift

import Foundation

import UIKit

open class Indicator {
    
    var containerView = UIView()
    
    var progressView = UIView()
    
    var activityIndicator = UIActivityIndicatorView()
    
    open class var shared: Indicator {
        
        struct Static {
            
            static let instance: Indicator = Indicator()
            
        }
        
        return Static.instance
        
    }
   
    open func showProgressView(_ view: UIView) {
     DispatchQueue.main.async {
        self.containerView.frame = view.frame
        self.containerView.center = view.center
        self.containerView.backgroundColor =  UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 0.5)// UIColor(white: 0, alpha: 0.3) // UIColor(hex: 0xAAAAAA, alpha: 0.6)
        //self.containerView.layer.opacity = 0.5
        
        self.progressView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        self.progressView.center = view.center
        self.progressView.backgroundColor = UIColor(hex: 0x1E1F1F, alpha: 1.0) //UIColor(red:0.00, green:0.61, blue:0.98, alpha:0.3)
        self.progressView.clipsToBounds = true
        self.progressView.layer.cornerRadius = 10
        
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color =  UIColor(hex: 0xDEDEDE, alpha: 1.0) //UIColor.darkGray
        self.activityIndicator.center = CGPoint(x: self.progressView.bounds.width / 2, y: self.progressView.bounds.height / 2)
        
       
        self.progressView.addSubview(self.activityIndicator)
        self.containerView.addSubview(self.progressView)
        view.addSubview(self.containerView)
        self.activityIndicator.startAnimating()
        }
}
   
    open func hideProgressView() {
         DispatchQueue.main.async {
            self.containerView.removeFromSuperview()
           self.activityIndicator.stopAnimating()
           
        }
    }
}

/*
extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
        
    }
}
*/
