//
//  SelfSerticeTextField.swift


import UIKit

@IBDesignable class SelfSerticeTextField: UITextField {



    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1.5
        let borderColor : UIColor = SelfServiceColor().KGreyBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 18
        self.autocorrectionType = .no;
        self.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        self.textColor = SelfServiceColor().KLigthOrange
        self.backgroundColor = SelfServiceColor().KTextFieldBGColor

       // let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 16))
        //let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: 16, height: 16))
        
        let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: kDropDownViewWidth, height: kDropDownViewHeight))
        let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: kDropDownImgViewWidth, height: kDropDownImgViewHeight))
        
        
        //dropDownView.backgroundColor = UIColor.red
        //dropDownImageView.backgroundColor = UIColor.green
        
        
        dropDownImageView.image = UIImage(named: kimageNameArrowDropDown)
        dropDownView.addSubview(dropDownImageView)
        dropDownView.isUserInteractionEnabled = false
        self.rightView = dropDownView
        self.rightViewMode = .always
        
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .always
         
        /*
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        */
        
    }
    func setIsReadOnly(status:Bool)  {
        self.rightView=UIView(frame: CGRect.zero)
        if status {
            self.isEnabled=false
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor
        }
        else
        {
            self.isEnabled = true
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor

            let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: kDropDownViewWidth, height: kDropDownViewHeight))
            let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: kDropDownImgViewWidth, height: kDropDownImgViewHeight))
            dropDownImageView.image = UIImage(named: kimageNameArrowDropDown)
            dropDownView.addSubview(dropDownImageView)
            dropDownView.isUserInteractionEnabled = false
            self.rightView = dropDownView
            self.rightViewMode = .always
        }
    }
    
   func setImageInRightView(imageName:String)  {
    
        let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: kDropDownViewWidth, height: kDropDownViewHeight))
        let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: kDropDownImgViewWidth, height: kDropDownImgViewHeight))
        dropDownImageView.image = UIImage(named: imageName)
        dropDownView.addSubview(dropDownImageView)
        dropDownView.isUserInteractionEnabled = false
        self.rightView = dropDownView
        self.rightViewMode = .always
    }
   
    func disableEditing(status:Bool)  {
        
        if status {
            self.isUserInteractionEnabled = false
            self.textColor=SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor
            self.rightView=UIView(frame: CGRect.zero)
        }
        else
        {
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor=UIColor.white
            self.isUserInteractionEnabled = true
        }
        
    }
    
    func setDropDownImage(imageName:String){
        if imageName.isEmpty == false{
            let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: kDropDownViewWidth, height: kDropDownViewHeight))
            let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: kDropDownImgViewWidth, height: kDropDownImgViewHeight))
            dropDownImageView.image = UIImage(named: "Arrow_DropDown")
            dropDownView.addSubview(dropDownImageView)
            dropDownView.isUserInteractionEnabled = false
            self.rightView = dropDownView
            self.rightViewMode = .always
        }
    }
    
    
}

class SelfServiceEditbleTextField: UITextField {
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth=1.0
        let borderColor : UIColor = SelfServiceColor().KGreyBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.autocorrectionType = .no;
        self.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        self.textColor = SelfServiceColor().KLigthOrange
        
       
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .never
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        
        
    }
    func setIsReadOnly(status:Bool)  {
        if status {
            self.isEnabled=false
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor
        }
        else
        {
            self.isEnabled = true
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = UIColor.white
            
        }
        self.rightView=UIView(frame: CGRect.zero)
    }
    
    
    func disableEditing(status:Bool)  {
        
        if status {
            self.isUserInteractionEnabled = false
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor
            self.rightView=UIView(frame: CGRect.zero)
        }
        else
        {
            self.textColor = SelfServiceColor().KLigthOrange
            self.backgroundColor = SelfServiceColor().KTextFieldBGColor
            self.isUserInteractionEnabled = true
        }
        
    }
    
    func setDropDownImage(imageName:String){
        if imageName.isEmpty == false{
            let dropDownView = UIView(frame: CGRect(x: 0, y: 0, width: kDropDownViewWidth, height: kDropDownViewHeight))
            let dropDownImageView = UIImageView(frame: CGRect(x: 6, y: 0, width: kDropDownImgViewWidth, height: kDropDownImgViewHeight))
            dropDownImageView.image = UIImage(named: "Arrow_DropDown")
            dropDownView.addSubview(dropDownImageView)
            dropDownView.isUserInteractionEnabled = false
            self.rightView = dropDownView
            self.rightViewMode = .always
        }
    }
    
}
class SelfServiceReadOnlyTextField: UITextField {
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth=1.0
        let borderColor : UIColor = SelfServiceColor().KGreyBorderColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.autocorrectionType = .no;
        self.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        self.textColor =  SelfServiceColor().KLigthOrange
        
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        leftPaddingView.isUserInteractionEnabled = false
        self.leftView = leftPaddingView
        self.leftViewMode = .always
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 1.0
        
        self.isUserInteractionEnabled = false
        self.backgroundColor = SelfServiceColor().KTextFieldBGColor
        self.rightView=UIView(frame: CGRect.zero)
    }
    
    
}
