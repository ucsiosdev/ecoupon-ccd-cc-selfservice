//
//  RestManager.swift
//  RestManager
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Foundation
import UIKit

class RestManager {
    
    // MARK: - Properties
    
    var requestHttpHeaders = RestEntity()
    
    var urlQueryParameters = RestEntity()
    
    var httpBodyParameters = RestEntity()
    
    var httpBody: Data?
    
    
    // MARK: - Public Methods
    
    func makeRequest(toURL url: URL,
                     withHttpMethod httpMethod: HttpMethod,currentVC:UIViewController,
                     completion: @escaping (_ result: Results) -> Void) {
        
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            print("\n\n **** Net Not connected  ***\n\n")
            Indicator.shared.hideProgressView()
            currentVC.view.makeToast(KPLEASE_CHECK_YOUR_INTERNET_CONNECTION, duration: 2.5, position: .center)
            return
            
        case .online(.wwan):
            print("\n\n **** Connected via WWAN  ***\n\n")
        case .online(.wiFi):
            print("\n\n **** Connected via WiFi  ***\n\n")
        }
        
       
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            
          // let url = URL.init(string: "https://testecoupon.maf.ae/svc/svcHifi.svc/FindCustomer?RequestTimeStamp=2020-04-21%2017:54:17&MobileNo=918652785531&AppKey=3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a&TokenID=EWVDvTxuoIvJt/hRhi3abX59/h/xO1rUeN4rubfzfPM%3D&ShareID=")
            
            
            
            let targetURL = self?.addURLQueryParameters(toURL: url)
            let httpBody = self?.getHttpBody()
            
            guard let request = self?.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: httpMethod) else
            {
                
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
            
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data,
                                   response: Response(fromURLResponse: response),
                                   error: error))
                
               
                
            }
            task.resume()
        }
    }
    
    
    
    func getData(fromURL url: URL, completion: @escaping (_ data: Data?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        if urlQueryParameters.totalItems() > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            //var index = 0
            for (key, value) in urlQueryParameters.allValues() {
                
//                var cs = CharacterSet.urlQueryAllowed
//                cs.remove(" ")
                
                //let generalDelimitersToEncode = "%2520"
                //var allowed = CharacterSet.urlQueryAllowed
                //allowed.remove(charactersIn: generalDelimitersToEncode)
                
               // let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                
                
                    //let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                    
                   
                //let item = URLQueryItem(name: key, value: value.replacingOccurrences(of: "+", with: "%2B").replacingOccurrences(of: "/", with: "%2F").replacingOccurrences(of: "=", with: "%3D"))
                
               // let item = URLQueryItem(name: key, value: value.replacingOccurrences(of: "+", with: "%2B").replacingOccurrences(of: "/", with: "%2F"))
                
                
//                 var allowedCharacters = CharacterSet.urlQueryAllowed
//                 allowedCharacters.remove("+=")
//                
//                 let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: allowedCharacters))
//                
//
//                
                
//                 print("\n\n ** Before: \(value)  **\n\n")
//                 value = value.stringReplacePlusSign()
//                 print("\n\n ** After: \(value)  **\n\n")
                
                 let item = URLQueryItem(name: key, value: value)
                 queryItems.append(item)
            }
            
            
            urlComponents.queryItems = queryItems
            
            guard let updatedURL = urlComponents.url else { return url }
            print("\n *** Final after append GET URL: \(updatedURL)  **\n")
            return updatedURL
        }
        print("\n *** Default Final URL: \(url)  **\n")
        return url
    }
    
    
    
    private func getHttpBody() -> Data? {
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") else { return nil }
        
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, httpMethod: HttpMethod) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        for (header, value) in requestHttpHeaders.allValues() {
            request.setValue(value, forHTTPHeaderField: header)
        }
        
        request.httpBody = httpBody
        return request
    }
}


// MARK: - RestManager Custom Types

extension RestManager {
    enum HttpMethod: String {
        case get
        case post
        case put
        case patch
        case delete
    }

    
    
    struct RestEntity {
        private var values: [String: String] = [:]
        
        mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> String? {
            return values[key]
        }
        
        func allValues() -> [String: String] {
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
    }
    
    
    
    struct Response {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = RestEntity()
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    
    
    struct Results {
        var data: Data?
        var response: Response?
        var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }

    
    
    enum CustomError: Error {
        case failedToCreateRequest
    }
}


// MARK: - Custom Error Description
extension RestManager.CustomError: LocalizedError {
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest: return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        }
    }
}
