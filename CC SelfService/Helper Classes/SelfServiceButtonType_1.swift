//
//  SelfServiceButtonType_1.swift

import UIKit

class SelfServiceBlueButton: UIButton {
    
    
    required init?(coder aDecoder: NSCoder) {
        // set myValue before super.init is called
        
        super.init(coder: aDecoder)
        
        // set other operations after super.init, if required
        //backgroundColor = SelfServiceColor().KBlue
        self.titleLabel?.font = SelfServiceFont.KGothamBoldFontWithSize(size: 22)
        self.setTitleColor(SelfServiceColor().KWhite, for: .normal)
      
    }
    
}
