//
//  TableViewHeaderLabel.swift


import Foundation
import UIKit

class TableViewHeaderLabel: UILabel {
   
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName(){
       
        self.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.font = SelfServiceFont.KGothamMediumFontWithSize(size: 16)
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}
