//
//  SelfServiceTitleLabelType_2.swift


import Foundation
import UIKit

class SelfServiceLabelBold_18: UILabel {
   
 
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName()
    {
        self.textColor = SelfServiceColor().KBlue
        self.font = SelfServiceFont.KGothamBoldFontWithSize(size: 18)
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}
