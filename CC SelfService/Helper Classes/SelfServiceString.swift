//
//  SelfServiceString.swift


import Foundation
import UIKit




extension String  {
    static func percentageString(str : String) -> String{
        return  String(format: "%0.2f",(str as NSString).doubleValue) + ("%")
    }
    
    
    public func stringReplacePlusSign() -> String {
        let finalString = self.replacingOccurrences(of: "+", with: "%2B")
        
        return finalString
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func doubleValueString()->Double{
        var refinedDouble = 0.0
        if let currentDouble = Double(self){
            refinedDouble = currentDouble
        }
        return refinedDouble
    }
    func AmountStringWithOutComas() -> String {
        return replacingOccurrences(of: ",", with: "")
    }
    
    func trimString() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    static func createGuid() -> String {
        let uuidRef = CFUUIDCreate(nil)
        let uuidStringRef = CFUUIDCreateString(nil, uuidRef)
        let uuid = uuidStringRef! as String
        return uuid
    }
    
    static func isEmpty(str : String) -> Bool {
        if str.isEmpty {
            return true
        }
        if str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            return true
        }
        if (str == "") {
            return true
        }
        return false
    }
    
    func sentenceCapitalizedString() -> String {
        
        return prefix(1).uppercased() + dropFirst()
    }
    
    static func getValidStringValue(string:String) -> String {
        var resultString = ""
        let updatedString:AnyObject = string as AnyObject
        if updatedString is NSNull{
            return resultString
        }
        if string.hasPrefix("\"") {
            resultString = string.replacingOccurrences(of: "\"", with: "")
        }
        else
        {
            resultString = string
        }
        return resultString
    }
    
    // static  func getValidStringFomDBString(dbString:Bindable?) -> String
    //    {
    ////        var resultString = ""
    ////        if let refinedString = dbString as! String{
    ////            resultString=refinedString
    ////        }
    //        return dbString as! String
    //    }
    
    static func getValidRouteStringValue(inputStr : String) -> String {
        return inputStr
        //        if (inputStr as AnyObject).isEmpty ?? true {
        //            return "--"
        //        }
        //        return "\(inputStr)"
    }
    func urlEncode() -> String {
        let output = ""
        
        return  output.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    
    
    func base64StringEncoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64StringDecoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func fetchDateString(date:Date,requiredFormat:String)->String
    {
        let dashDateFormatter = DateFormatter()
        dashDateFormatter.dateFormat = requiredFormat
        let dateString = dashDateFormatter.string(from: date)
        return dateString
    }
    
    static  func fetchStringfromDate(date:Date,requiredFormat:String) -> String
    {
        let dashDateFormatter = DateFormatter()
        dashDateFormatter.dateFormat = requiredFormat
        dashDateFormatter.locale = Locale.init(identifier: KEnglishLocaleStr)
        dashDateFormatter.timeZone = Calendar.current.timeZone
        let dateString = dashDateFormatter.string(from: date)
        return dateString
    }
    
    static  func fetchStringfromDateInUTC(date:Date,requiredFormat:String) -> String
    {
        let dashDateFormatter = DateFormatter()
        dashDateFormatter.dateFormat = requiredFormat
        dashDateFormatter.locale = Locale.init(identifier: KEnglishLocaleStr)
        dashDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dateString = dashDateFormatter.string(from: date)
        return dateString
    }
    
    static  func fetchNextDateString(selectedDate:Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = String.fetchStringfromDate(date: selectedDate, requiredFormat: "yyyy-MM-dd")
        let myDate = dateFormatter.date(from: dateString)!
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let nextDateString = dateFormatter.string(from: tomorrow!)
        return nextDateString
    }
    
    static  func fetchPreviousDateString(selectedDate:Date)-> String
    {
        var dayComp = DateComponents()
        dayComp.day = -1
        let yesterday = Calendar.current.date(byAdding: dayComp, to: selectedDate)
        let dateString = String.fetchStringfromDate(date: yesterday!, requiredFormat: "yyyy-MM-dd")
        return dateString
    }
    
    static  func convertDateTODBFormat(inputDate:Date) -> String {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let formatterDateString = dateFormatter.string(from: inputDate)
        return formatterDateString
    }
    /*+(NSString*)convertNSDatetoDataBaseDateFormat:(NSDate *)inputDate
     {
     NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
     [dateFormatter setLocale:usLocale];
     //NSDate* currentDate=inputDate;
     NSString * formattedDate=[dateFormatter stringFromDate:inputDate];
     return formattedDate;
     
     
     }*/
    
    
    
    static   func refineDateFormat(sourceFormat:String,destinationFormat:String,sourceString:String) -> String {
        
        /**
         cheat sheet
         
         Tuesday, Aug 9, 2016
         EEEE, MMM d, yyyy
         
         08/09/2016
         MM/dd/yyyy
         
         08-09-2016 06:09
         MM-dd-yyyy HH:mm
         
         Aug 9, 6:09 AM
         MMM d, H:mm a
         
         August 2016
         MMMM yyyy
         
         Aug 9, 2016
         MMM d, yyyy
         
         Tue, 9 Aug 2016 06:09:29 +0000
         E, d MMM yyyy HH:mm:ss Z
         
         2016-08-09T06:09:29+0000
         yyyy-MM-dd'T'HH:mm:ssZ
         
         09.08.16
         dd.MM.yy
         
         
         
         **/
        var dateString = ""
        let sourceDateFormatter = DateFormatter()
        sourceDateFormatter.timeZone = NSTimeZone.system
        sourceDateFormatter.locale = Locale(identifier: KEnglishLocaleStr)
        sourceDateFormatter.dateFormat=sourceFormat
        let sourceDate = sourceDateFormatter.date(from: sourceString)
        
        let destinationDateFormatter = DateFormatter()
        destinationDateFormatter.timeZone = NSTimeZone.system
        destinationDateFormatter.locale = Locale(identifier: KEnglishLocaleStr)
        destinationDateFormatter.dateFormat=destinationFormat
        if sourceDate == nil {
            print("\n\n :::: WARNING: Date String return as: EMPTY  ::::\n\n")
            return dateString
        }
        let stringFromDate = destinationDateFormatter.string(from: sourceDate!)
        if stringFromDate.isEmpty {
            
        }else{
            dateString = stringFromDate
        }
        
        //print("\n\n :::: Hint: Date String return as: \(dateString) ::::\n\n")
        return dateString
    }
    
    
    static   func convertToDate(dateString: String) -> Date? {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let usLocaleq = NSLocale(localeIdentifier: "en_US_POSIX")
        df.locale = usLocaleq as Locale
        if let anAbbreviation = NSTimeZone(abbreviation: "UTC+4:00") {
            df.timeZone = anAbbreviation as TimeZone
        }
        let date: Date? = df.date(from: dateString)
        return date
    }
    
    static   func convertToDateWithoutTime(dateString: String) -> Date? {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let usLocaleq = NSLocale(localeIdentifier: "en_US_POSIX")
        df.locale = usLocaleq as Locale
        if let anAbbreviation = NSTimeZone(abbreviation: "UTC+4:00") {
            df.timeZone = anAbbreviation as TimeZone
        }
        let date: Date? = df.date(from: dateString)
        return date
    }
    
    static   func timeBetweenStartTime(startTime:String,endTime:String) -> String {
        
        if startTime.isEmpty == false  && endTime.isEmpty == false {
            let elapsedTimeDisplayingPoints: TimeInterval = self.convertToDate(dateString: endTime)!.timeIntervalSince(self.convertToDate(dateString: startTime)!)
            let ti = Int(elapsedTimeDisplayingPoints)
            let seconds: Int = ti % 60
            let minutes: Int = (ti / 60) % 60
            let hours: Int = ti / 3600
            return String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds)
        }else{
            return ""
        }
    }
    static func validateEmail(sourceString:String) -> Bool{
        do{
            let emailRegularExpressionPattern = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"
            let emailRegularExpression = try NSRegularExpression(pattern: emailRegularExpressionPattern, options: .caseInsensitive)
            let regExMatches = emailRegularExpression .numberOfMatches(in: sourceString, options: [], range: NSMakeRange(0, sourceString.count))
            if regExMatches == 0{
                return false
            }else{
                return true
            }
        }catch{
            print("error validating email string \(sourceString)")
            return false
        }
    }
    
}


extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

/*
 extension URLComponents {
    mutating func setQueryItems(with parameters: [String: String]) {
    self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
 }
 */

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}

extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        layer.borderColor = UIColor(red: 109.0/255.0, green: 39.0/255.0, blue: 38.0/255.0, alpha: 1.0).cgColor
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            self.changeBorderColorToOriginalState()
        })
        
    }
    
    func changeBorderColorToOriginalState() {
         layer.borderColor = SelfServiceColor().KGreyBorderColor.cgColor
    }
}
