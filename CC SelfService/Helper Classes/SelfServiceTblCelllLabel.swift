//
//  SelfServiceTblCelllLabel.swift


import Foundation
import UIKit

class SelfServiceTblCelllLabel: UILabel {
   
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName(){
       
        self.textColor = UIColor(red: 12.0/255.0, green: 99.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        self.font = SelfServiceFont.KGothamBookFontWithSize(size: 15)
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}
