//
//  SelfServiceConstants.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 10/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//


import Foundation
import UIKit

/*
 
 "Test" : {
     "APP_KEY" : "3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a", // (may be for web)
     "PRIVATE_KEY" : "1ae62e36396109bf379788556159525c1a3afa70c67ea5fb5b1a63c7f7484573", (may be for web)
     "BASE_URL" : "https://testecoupon.maf.ae/"
 },
 
 
 "Live" : {
     "APP_KEY" : "7d12042ae69cda12cdf205b213a391201f255b5c5fc5251c37ed32af1297e39c",
     "PRIVATE_KEY" : "4ca12f14326203af379778352153525d1a3afd70c67da5fa5b1a63c7f2185293",
     "BASE_URL" : "https://ecoupon.maf.ae/"
 },
 
 */


let SINGLETON = Singleton.SharedInstance
let kEmptyString = ""
let kDatabaseDateFormatWithTime = "yyyy-MM-dd HH:mm:ss"
let kdisplayDateFormatWithoutTime = "dd MMM, yyyy"
let kdisplayDateFormatWithTime = "dd MMM, yyyy hh:mm a"
let kDatabaseDateFormatWithOutTime = "yyyy-MM-dd"

let kDropDownViewWidth = 28
let kDropDownViewHeight = 8
let kDropDownImgViewWidth = 16
let kDropDownImgViewHeight = 8

let kDeviceTimeValidationRange = "5"
let kVisitsSyncInterval = "60"
let kStockSyncInterval = "60"
let kCommunicationSyncInterval = "10"
let kAgeingDisplayFormat = "DAYS"
let kDefaultMessageModuleVersion = "1"
let kDefaultMessageExpiryDays = "7"
let kVisitConclusionNotesWordLimit = "20"
let kOrderHistoryDuplicateCheck = "0"
let kDefaultVATRuleLevel = "P" //ProductLevel
let kDefaultVatLevel = "NET"
let kFSVisitTimeLimit = "100"
let kCloseVisitLocationUpdateLimit = "50"
let kRouteSyncDays = "1"
let kSDBucketSize = "1"
let kSDDuration = "6"
let kBonusMode = "DEFAULT"
let kNearByCustomerDistanceThreshold = "1000"


//let validString = NSCharacterSet(charactersInString: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
let kAcceptableCharactersinTextField = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.@ ").inverted
let kAcceptableCharactersWithSpaceinTextField = NSCharacterSet.init(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.@").inverted
let kAcceptableCharactersForMobileNumberinTextField = NSCharacterSet.init(charactersIn: "0123456789").inverted

let KWholeNumberQuantityRegExpression = "^[0-9]*$"
let KDecimalQuantityRegExpression = String(format: "^[0-9]*((\\.|,)[0-9]{0,%0.0f})?$", 2.0)

let KNotApplicable = "N/A"
let KTitleStrAlert = "Alert"

let KEmptyString = ""
let KSearch = "Search"
let KCCSELFSERVICE = "CC SelfService"
let KUnSavedString = "UnSaved"
let KSavedString = "Saved"
let KAlertOkButtonTitle = "Ok"
let KAlertYESButtonTitle = "Yes"
let KProceed = "Proceed"
let KDelete = "Delete"
let KAlertNoButtonTitle = "No"
let KAlertCancelButtonTitle = "Cancel"
let KEnglishLocaleStr = "en_US"

let KSettingsPasswordDefault = "@b@cu$"

//let KAppKeyDefaultTestApp(may be for web) = "3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a"
//let KAppKeyDefaultLiveApp = "5d19072ae69bea19bfa208b213a494201f255b5c5fc4251c37ed32af1297e39c"
//let KSecretKeyDefaultTestApp (may be for web) = "1ae62e36396109bf379788556159525c1a3afa70c67ea5fb5b1a63c7f7484573"
//let KSecretKeyDefaultLiveApp = "9cd72a36396109cd379788556159525f1a3afd70c67da5fa5b1a63c7f2185293"
//let KTestAPIBaseURL = "https://testecoupon.maf.ae"


/*
 WARNING THESE ARE LIVE APP KEYS, DO NOT USE WITHOUT ADMIN PERMISSION
 API Key  :7d12042ae69cda12cdf205b213a391201f255b5c5fc5251c37ed32af1297e39c
 Private Key :4ca12f14326203af379778352153525d1a3afd70c67da5fa5b1a63c7f2185293
*/


//HINT 1: THESE ARE FOR IPAD, TEST SERVER
let KTestCurrentAPIBaseURL = "https://newtestecoupon.maf.ae"
let KTestAppKeyDefaultTestApp = "8ae62e86396109bf3797885561595e5c1a3afa70167ea5fb5b1a63f7f7484568"
let KTestSecretKeyDefaultTestApp = "499dcfaba0bcfbde99f88a38b97056b6a8fdf0244d262ee72f542f3ca6d8f81b"
//HINT 1 ENDED HERE
 


//HINT 2: THESE ARE FOR IPAD, LIVE SERVER
var KCurrentAPIBaseURL = "https://ecoupon.maf.ae"
var KAppKeyDefaultTestApp = "7d12042ae69cda12cdf205b213a391201f255b5c5fc5251c37ed32af1297e39c"
var KSecretKeyDefaultTestApp = "4ca12f14326203af379778352153525d1a3afd70c67da5fa5b1a63c7f2185293"
//HINT 2 ENDED HERE


let KAPIURLSettings_ValidateApprovalCode = "/svc/svcHifi.svc/ValidateApprovalCode"
let KAPIURLSettings_GetInvoiceMallsList = "/svc/svcHifi.svc/GetInvoiceMallsList"
let KAPIURLSettings_GetInvoiceStoresList = "/svc/svcHifi.svc/GetInvoiceStoresList"
let KAPIURLFindCustomer = "/svc/svcHifi.svc/FindCustomer"
let KAPIURLGetTitle = "/svc/svcHifi.svc/GetTitle"
let KAPIURLGetNationality = "/svc/svcHifi.svc/GetNationality"
let KAPIURLGetResidenceCity = "/svc/svcHifi.svc/GetResidenceCity"
let KAPIURLGetResidenceArea = "/svc/svcHifi.svc/GetResidenceArea"
let KAPIURLGetPaymentMethods = "/svc/svcHifi.svc/GetPaymentMethods"
let KAPIURLGetActiveCampaignsByMall = "/svc/svcHifi.svc/GetActiveCampaignsByMall"
let KAPIURLPOSTSaveCustomer = "/svc/svcHifi.svc/SaveCustomer"

//  SaveReceipt : POST
let KAPIURLPOSTSaveReceipt = "/svc/svcHifi.svc/SaveReceipt"
let KEntryRef = "EntryRef"
let KStoreID = "StoreID"
let KInvoiceNo = "InvoiceNo"
let KInvoiceValue = "InvoiceValue"
let KInvoiceDate = "InvoiceDate"
let KInvoiceRefNo = "InvoiceRefNo"
let KFileID = "FileID"
let KPayMode = "PayMode"

//ConfirmEntry : POST
let KAPIURLPOSConfirmEntry = "/svc/svcHifi.svc/ConfirmEntry"
let KCampaignList = "CampaignList"
let KSaveReceiptAPISucessMsg = "Thank you for participating, your receipts have been submitted successfully."

//GetNewEntryID
let KAPIURLGetNewEntryID = "/svc/svcHifi.svc/GetNewEntryID"
let KCustID = "CustID"

//ValidateEToken post
let KAPIURLPOSTValidateEToken = "/svc/svcHifi.svc/ValidateEToken"
let KTokenCode = "TokenCode"
let KAPIURLPOSTGetETokenDetails = "/svc/svcHifi.svc/GetETokenDetails"

//MarkEToken: POST
let KAPIURLPOSTMarkEToken = "/svc/svcHifi.svc/MarkEToken"
let KCustomerID = "CustomerID"
let KCampaignID = "CampaignID"
let KMarkedBy = "MarkedBy"
let KIDNo = "IDNo"
let KPrizeInfo = "PrizeInfo"



//PrinteTokenDED: POST
let KAPIURLPOSTPrinteTokenDED = "/svc/svcHifi.svc/PrinteTokenDED"


//UpdateCustomer: POST
let KAPIURLPOSTUpdateCustomer = "/svc/svcHifi.svc/UpdateCustomer"
let KUpdatedBy = "UpdatedBy"


    
let kimageNameArrowDropDown = "Arrow_DropDown"
let KContent_Type = "Content-Type"
let Kapplication_json = "application/json"
let KApprovalCode = "ApprovalCode"
let KRequestTimeStamp = "RequestTimeStamp"
let KMallID = "MallID"
let KAppKey = "AppKey"
let KTokenID = "TokenID"
let KMobileNo = "MobileNo"
let KShareID = "ShareID"
let KCountry = "Country"
let KCity = "City"
let KMallList = "MallList"
let KCustRefNo = "CustRefNo"
let KTitle = "Title"
let KFirstName = "FirstName"
let KLastName = "LastName"
let KCountryCode = "CountryCode"
let KAreaCode = "AreaCode"
let KEmail = "Email"
let KUserAgent = "UserAgent"
let KUserAgentValue_Mobile = "Mobile"
let KWebMallID = "WebMallID"
let KWebMallIDValue_Blank = ""
let KPrivacyPolicy = "PrivacyPolicy"
let KNationality = "Nationality"
let KVersionNo = "VersionNo"
let KVersionNoValue_3 = "3"

let KCountryResidence = "CountryResidence"
let KResidenceStatus = "ResidenceStatus"
let KResidenceCity = "ResidenceCity"
let KResidenceArea = "ResidenceArea"



let K_PleaseSelect_ = "--PLEASE SELECT--"
let KError = "Error"
let KMobilenNumberLengthError = "Mobile number should be 5 to 15 digits"
let KDefaultCSOAccoutSetMsg = "Default CSO account is set."
let KPLEASE_CHECK_YOUR_INTERNET_CONNECTION = "Please check your internet connection."
let KAPIResSuccess = "Y"
let KAPIResFailed = "F"
let KSomethingWentWrongTryAgainLater = "Something went wrong. Please ensure internet working properly in your device and try again later"
let KUnableToParseAPIResponseMsg = "Unable to parsing API response. Please contact to Admin."
let KNameTextFieldLimit = 50
let KEmailTextFieldLimit = 50
let KApprovalCodeTextFieldLimit = 50
let KEneterYourApporovalCodeMsg = "Enter your Approval Code to set default CSO account"
let KActiveCampaignNotAvailableMsg = "Active campaign list is not available, please try again later"
let KInvaidReceiptAmountMsg = "Please enter valid Receipt Amount"
let KFailureAlertTitle  = "Failure"
let KUNSupportMailComposer = "Your device doesn't support the mail composer sheet. Please configure mail box in your device"
let KNotAbleShareFileMsg = "Unable to share this file, Please try again later."

struct SelfServicePopOverType
{
    
    
}
struct SelfServiceAPIResponseCode{
    let K200_OK = 200
    let K201_CREATED = 201
    let K202_ACCEPTED = 202
    let K203_NON_AUTHORITATIVE_INFO = 203
    let K204_NO_CONTENT = 204
    let K205_RESET_CONTENT = 205
}

struct SelfServiceColor{
   
    let KLigthOrange = UIColor(red: 234.0/255.0, green: 116.0/255.0, blue: 18.0/255.0, alpha: 1.0)//done
    let KBlue = UIColor(red: 12.0/255.0, green: 99.0/255.0, blue: 176.0/255.0, alpha: 1.0) // done
    let KWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)// done
    let KGreyBorderColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1.0)//done
    let KTextFieldBGColor = UIColor(red: 231.0/255.0, green: 232.0/255.0, blue: 234.0/255.0, alpha: 1.0) // done
    let HeaderViewBackgroundColor = UIColor(red: 235.0/255.0, green: 223.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    let UIViewBackGroundColor = UIColor.white
    let NavigationBarBackgroundColor = UIColor(red: 234.0/255.0, green: 116.0/255.0, blue: 18.0/255.0, alpha: 1.0) //done
    let HeaderViewBorderColor = UIColor(red: 78.0/255.0, green: 74.0/255.0, blue: 75.0/255.0, alpha: 1.0)
    let SubTitle = UIColor(red: 167.0/255.0, green: 165.0/255.0, blue: 166.0/255.0, alpha: 1.0)//done
    let ButtonTitleLigthOrange = UIColor(red: 240.0/255.0, green: 134.0/255.0, blue: 14.0/255.0, alpha: 1.0)//done
    let ButtonDisableTitleLightOrange = UIColor(red: 202.0/255.0, green: 202.0/255.0, blue: 174.0/255.0, alpha: 1.0)//done
    let ButtonDisableBGLightGrey = UIColor(red: 230.0/255.0, green: 225.0/255.0, blue: 219.0/255.0, alpha: 1.0)//done
}


struct SelfServiceFont {
   
    /*
    static func KGothamBook_RegularFontWithSize(size: CGFloat) -> UIFont {
           return UIFont(name: "GothamBook", size: size)!
       }
    */
    static func KGothamBookFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Book", size: size)!
    }
    
    static func KGothamMediumFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Medium", size: size)!
    }
    
    static func KGothamBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Bold", size: size)!
    }
    
    static func KPoppinsLightFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Light", size: size)!
    }
    
    static func KPoppinsMediumFontWithSize(size: CGFloat) -> UIFont {
           return UIFont(name: "Poppins-Medium", size: size)!
       }
    
    static func KPoppinsBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size)!
    }
    
    static func KFuturaBookFontWithSize(size: CGFloat) -> UIFont {
           return UIFont(name: "FuturaBT-Book", size: size)!
    }
    
    static func KFuturaMediumFontWithSize(size: CGFloat) -> UIFont {
           return UIFont(name: "Futura-Medium", size: size)!
    }
    
    static func KFuturaBoldFontWithSize(size: CGFloat) -> UIFont {
           return UIFont(name: "Futura-Bold", size: size)!
    }

    
   // let HeaderFont = KGothamBookFontWithSize(size: 16)
    let bodyFont = KGothamBookFontWithSize(size: 12)
    let bodySemiBold = KGothamMediumFontWithSize(size: 14)
    let NavigationBarButtonItemFont = KGothamMediumFontWithSize(size: 14)
    let SelfServiceTextFont = KGothamBookFontWithSize(size: 14)
    let UITableViewCellFont = KGothamBookFontWithSize(size: 19)
    let UITableViewCellDescFont = KGothamMediumFontWithSize(size: 16)
    let SelfServiceTitleFont = KGothamBookFontWithSize(size: 19)
    let NavigBarTitleFont = KFuturaBookFontWithSize(size: 20)
   
  

}
let KRegistrationScreenOrderQuantityTextFieldMaximumDigitsLimit = 6


enum SignatureViewStatusType {
     case KEmptySignature, KSignatureDisabled, KSignatureSaved, KSignatureNotSaved
}
enum ProductSearchType {
    case ProductSerachNormal, ProductSearchByBarCode, ProductSearchByPopOver
}
enum OrderItemsTableViewCellType {
    case NormalProductTypeCell, ManualFOCTypeCell, SimpleBonusTypeCell, AssortMentBonusTypeCell
}


struct UserDefaultKeys{
       static let IS_ALREADY_LOGIN = "IS_ALREADY_LOGIN"
       static let CURRENT_ACTIVE_APPROVE_CODE_DETAILS = "CURRENT_ACTIVE_APPROVE_CODE_DETAILS"
       static let IS_ENABLE_ALL_MALLS = "IS_ENABLE_ALL_MALLS"
    }


class GetConstants
{
    /*
    static let retrieveConstants = SelfServiceConstants()
    private init(){}
    
    
    func getHMAC256FinalValue(approvalCode: String,currentDateTimeInUTC:String,appKey:String) -> String {
           
           return "\(KApprovalCode)=\(approvalCode)&\(KRequestTimeStamp)=\(currentDateTimeInUTC)&\(KAppKey)=\(appKey)".hmac(algorithm: .sha256, key: KSecretKeyDefault)
            
       }
    
    
    
    
    
    
    
    func applicationDocumentsDirectory() -> String {
        let filePath: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        return (filePath?.path)!
    }
    
    func setUserProfile(myArray : NSDictionary) -> Void
    {
        UserDefaults.standard.set(myArray, forKey: kUserProfile)
    }
    func userProfile() -> Dictionary<String, Any> {
        
        let userProfileFromDefaults = UserDefaults.standard.value(forKey: "UserProfile") as! String
        let jsonData = userProfileFromDefaults.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        return dictionary as! Dictionary<String, Any>
    }
    
    func getStrSalesRepID(userProfileDic:Dictionary<String, Any> ) -> String{
        
        var salesRepID = ""
        if let result = userProfileDic[kSalesRep_ID] as? String{
            salesRepID =  result
        }else if let result = userProfileDic[kSalesRep_ID] as? NSInteger{
            salesRepID =  "\(result)"
        }else if let result = userProfileDic[kSalesRep_ID] as? NSNumber{
            salesRepID =  "\(result)"
        }
        return salesRepID
    }
    
    func getStrDecimalDigits(userProfileDic:Dictionary<String, Any> ) -> String{
        
        var decimalDigits = ""
        if let result = userProfileDic[kDecimal_Digits] as? String{
            decimalDigits =  result
        }else if let result = userProfileDic[kDecimal_Digits] as? NSInteger{
            decimalDigits =  "\(result)"
        }else if let result = userProfileDic[kDecimal_Digits] as? NSNumber{
            decimalDigits =  "\(result)"
        }
        return decimalDigits
    }
   
    
    func getStrEmpCode(userProfileDic:Dictionary<String, Any> ) -> String{
        
        var empCode = ""
        if let result = userProfileDic[kEmp_Code] as? String{
            empCode =  result
        }else if let result = userProfileDic[kEmp_Code] as? NSInteger{
            empCode =  "\(result)"
        }else if let result = userProfileDic[kEmp_Code] as? NSNumber{
            empCode =  "\(result)"
        }
        
        return empCode
    }
    
    func getStrUserID(userProfileDic:Dictionary<String, Any> ) -> String{
        
        var userID = ""
        if let result = userProfileDic[kUser_ID] as? String{
            userID =  result
        }else if let result = userProfileDic[kUser_ID] as? NSInteger{
            userID =  "\(result)"
        }else if let result = userProfileDic[kUser_ID] as? NSNumber{
            userID =  "\(result)"
        }
        return userID
    }
    
    
    
    
    func removeTimeStamp(fromDate: Date) -> Date {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: fromDate)) else {
            fatalError("Failed to strip time from Date object")
        }
        return date
    }
 
 */
}
