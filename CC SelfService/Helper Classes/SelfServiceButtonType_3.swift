//
//  SelfServiceButtonType_3.swift

import UIKit

class SelfServiceLightOrangeButton: UIButton {
    
    
    required init?(coder aDecoder: NSCoder) {
        // set myValue before super.init is called
        
        super.init(coder: aDecoder)
        
        // set other operations after super.init, if required
        self.setBackgroundImage(UIImage(named: "btnLightOrang"), for: .normal)
        self.titleLabel?.font = SelfServiceFont.KGothamBoldFontWithSize(size: 22)
        self.setTitleColor(SelfServiceColor().ButtonTitleLigthOrange, for: .normal)
      
    }
    
}
