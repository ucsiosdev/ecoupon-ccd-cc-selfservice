//
//  SelfServiceMainTitle.swift


import Foundation
import UIKit

class SelfServiceMainTitle: UILabel {
   
 
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName()
    {
        self.textColor = SelfServiceColor().KBlue
        self.font = SelfServiceFont.KGothamBoldFontWithSize(size: 30)
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}

class SelfServiceNavigBarTitle: UILabel {
   
 
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName()
    {
        self.textColor = SelfServiceColor().KWhite
        self.font = SelfServiceFont().NavigBarTitleFont
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}

