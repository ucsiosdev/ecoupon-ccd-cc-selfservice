//
//  SelfServiceButtonType_2.swift

import UIKit

class SelfServiceOrangeButton: UIButton {
    
    
    required init?(coder aDecoder: NSCoder) {
        // set myValue before super.init is called
        
        super.init(coder: aDecoder)
        
        // set other operations after super.init, if required
        backgroundColor = SelfServiceColor().KLigthOrange
        self.titleLabel?.font = SelfServiceFont.KGothamMediumFontWithSize(size: 20)
        self.setTitleColor(SelfServiceColor().KWhite, for: .normal)
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
      
    }
    
}
