//
//  SelfServiceView.swift


import Foundation
import UIKit


class DropShadowView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 1
        
    }
}

class NavigationHeaderView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = SelfServiceColor().KLigthOrange
    }
}

extension UIView {
    
    // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView(fileName:String) -> String {
        
        let pdfPageFrame = self.bounds
        //let pdfPageFrame = CGRect(x: 0 , y: 0, width: self.bounds.size.width, height: (self.bounds.size.height * 1.7))

        
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData, fileName: fileName)
        
    }
    
    // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData,fileName:String) -> String {
      let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      let docDirectoryPath = paths[0]
      let pdfPath = docDirectoryPath.appendingPathComponent(fileName)
      if data.write(to: pdfPath, atomically: true) {
          return pdfPath.path
      } else {
          return ""
      }
    }
}

extension NSDate {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

/*
class DropShadowViewwithBorders: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 1
        
        self.backgroundColor = SelfServiceColors().HeaderViewBackgroundColor
        let topBorder = CALayer()
        topBorder.backgroundColor = SelfServiceColors().HeaderViewBorderColor.cgColor
        topBorder.frame = CGRect(x: 0, y: 0, width: frame.width, height: 1)
        layer.addSublayer(topBorder)
        
        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = SelfServiceColors().HeaderViewBorderColor.cgColor
        bottomBorder.frame = CGRect(x: 0, y: frame.height-1, width: frame.width, height: 1)
        layer.addSublayer(bottomBorder)
    }
}
*/


extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    func addRoundedCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    enum ViewSide {
        case left, right, top, bottom//, topandBottom
    }
    
    func addTopandBottomBorder(){
        self.backgroundColor = SelfServiceColor().HeaderViewBackgroundColor
        let topBorder = CALayer()
        topBorder.backgroundColor =  SelfServiceColor().HeaderViewBorderColor.cgColor
        topBorder.frame = CGRect(x: 0, y: 0, width: frame.width, height: 0.3)
        layer.addSublayer(topBorder)

        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = SelfServiceColor().HeaderViewBorderColor.cgColor
        bottomBorder.frame = CGRect(x: 0, y: frame.height-1, width: frame.width, height: 0.3)
        layer.addSublayer(bottomBorder)
    }
    func addBorder(toSide side: ViewSide, withThickness thickness: CGFloat) {
        
        let border = CALayer()
//        border.backgroundColor = SelfServiceColors().HeaderViewBorderColor.cgColor
        border.backgroundColor = UIColor.red.cgColor

        switch side {
        case .left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height)
        case .right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height)
        case .top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness)
        case .bottom: border.frame = CGRect(x: 0, y: frame.height-1, width: frame.width, height: thickness)
        
        }
//        switch side {
//        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
//        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
//        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
//        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
//        case .TopandBottom:
//            let topBorder = CALayer()
//            topBorder.backgroundColor = color
//            topBorder.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness)
//            layer.addSublayer(topBorder)
//
//            let bottomBorder = CALayer()
//            bottomBorder.backgroundColor = color
//            bottomBorder.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness)
//            layer.addSublayer(bottomBorder)
//
//            break
//
//        }
        
        layer.addSublayer(border)
    }
    
    func addShadowOnViewWithCornerRadius(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
        self.layer.cornerRadius = 4
    }
    
    func dropShadowWithOutCornerRadius(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
      layer.masksToBounds = false
      layer.shadowColor = color.cgColor
      layer.shadowOpacity = opacity
      layer.shadowOffset = offSet
      layer.shadowRadius = radius

      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension Data {
    
    func handleAPIParsingErrorCase(currentVC:UIViewController){
        
        
        currentVC.showAlertOnMainThread(title: KError, msg: KUnableToParseAPIResponseMsg)
        
        print("\n\n ***** WARNING: Error during handling API Response  *****")
        
        if let jsonResult = try? JSONSerialization.jsonObject(with: self, options: .allowFragments) as? [String: AnyObject]{
            print("\n\n *** API JSON Result (Error Case) :\n\(String(describing: jsonResult)) ***\n\n")
        }else {
             print("\n\n ***  JSON serialization failed   ***\n\n")
            // *****
            // What do you do here? Do you tell your users anything?
            // *****
          }
    }
}

extension URL {
    
    /// Creates a QR code for the current URL in the given color.
    func qrImage(using color: UIColor, logo: UIImage? = nil) -> UIImage? {
        
        guard let tintedQRImage = qrImage?.tinted(using: color) else {
            return nil
        }
        
        guard let logo = logo?.cgImage else {
            return UIImage(ciImage: tintedQRImage)
        }
        
        guard let final = tintedQRImage.combined(with: CIImage(cgImage: logo)) else {
            return UIImage(ciImage: tintedQRImage)
        }
        
        return UIImage(ciImage: final)
    }
    
    /// Returns a black and white QR code for this URL.
    var qrImage: CIImage? {
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
        let qrData = absoluteString.data(using: String.Encoding.ascii)
        qrFilter.setValue(qrData, forKey: "inputMessage")
        
        let qrTransform = CGAffineTransform(scaleX: 12, y: 12)
        return qrFilter.outputImage?.transformed(by: qrTransform)
    }
}


extension CIImage {
    /// Inverts the colors and creates a transparent image by converting the mask to alpha.
    /// Input image should be black and white.
    var transparent: CIImage? {
        return inverted?.blackTransparent
    }
    
    /// Inverts the colors.
    var inverted: CIImage? {
        guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }
        
        invertedColorFilter.setValue(self, forKey: "inputImage")
        return invertedColorFilter.outputImage
    }
    
    /// Converts all black to transparent.
    var blackTransparent: CIImage? {
        guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
        blackTransparentFilter.setValue(self, forKey: "inputImage")
        return blackTransparentFilter.outputImage
    }
    
    /// Applies the given color as a tint color.
    func tinted(using color: UIColor) -> CIImage? {
        guard
            let transparentQRImage = transparent,
            let filter = CIFilter(name: "CIMultiplyCompositing"),
            let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }
        
        let ciColor = CIColor(color: color)
        colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter.outputImage
        
        filter.setValue(colorImage, forKey: kCIInputImageKey)
        filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)
        
        return filter.outputImage!
    }
}

extension CIImage {
    
    /// Combines the current image with the given image centered.
    func combined(with image: CIImage) -> CIImage? {
        guard let combinedFilter = CIFilter(name: "CISourceOverCompositing") else { return nil }
        let centerTransform = CGAffineTransform(translationX: extent.midX - (image.extent.size.width / 2), y: extent.midY - (image.extent.size.height / 2))
        combinedFilter.setValue(image.transformed(by: centerTransform), forKey: "inputImage")
        combinedFilter.setValue(self, forKey: "inputBackgroundImage")
        return combinedFilter.outputImage!
    }
}
