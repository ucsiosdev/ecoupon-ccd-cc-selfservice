//
//  SelfServiceLabel.swift


import Foundation
import UIKit

class SelfServiceTitleLabel: UILabel {
   
 
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
    }
   
    func changeFontName()
    {
        self.textColor = SelfServiceColor().KBlue
        self.font = SelfServiceFont.KGothamMediumFontWithSize(size: 18)
        
        /*
        if(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft) {
            //RTL
            self.textAlignment = .right
        } else {
            //LTR
            self.textAlignment = .left
        }
        */
       // self.text = super.text?.localized
    }
    

}
