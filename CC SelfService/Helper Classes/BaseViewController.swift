//
//  BaseViewController.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 10/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//


import UIKit

class BaseViewController: UIViewController {
    
    var navigationTitle:String = ""
    var isPopOver:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = SelfServiceColor().NavigationBarBackgroundColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        // if device iOS Version is >= 11
        if #available(iOS 11.0, *) {
            let navigationTitleLabel:UILabel = UILabel(frame: CGRect.zero)
            navigationTitleLabel.textAlignment = .center
            navigationTitleLabel.textColor=UIColor.white
            if isPopOver
            {
                navigationTitleLabel.font = SelfServiceFont.KGothamBookFontWithSize(size: 20)
            }
            else
            {
                navigationTitleLabel.font = SelfServiceFont().NavigBarTitleFont
            }
            
            navigationTitleLabel.text = navigationTitle
            self.navigationItem.titleView=navigationTitleLabel
        }
        // Alternative code for earlier versions of iOS.
        else{
            self.title = navigationTitle
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: SelfServiceFont().NavigBarTitleFont, NSAttributedStringKey.foregroundColor : UIColor.white]
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.view.backgroundColor = SelfServiceColor().UIViewBackGroundColor

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
