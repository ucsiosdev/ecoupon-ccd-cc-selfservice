//
//  Defaults.swift
//  MedRep
//
//  Created by Unique Computer Systems on 8/2/18.
//  Copyright © 2018 Unique Computer Systems. All rights reserved.
//

import Foundation
//import Charts
//import SwiftEntryKit
import UIKit
//import MapKit
//import Segmentio


class SelfServiceDefaults{
    
    static let retrieveDefaults = SelfServiceDefaults()
    private init(){}
    
    func getAppVersionWithBuild() -> String? {

        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let build = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
        print("updating client version \("\(version ?? "") (\(build ?? ""))")")
        let AppVersion = "\(version ?? "")(\(build ?? ""))"
        return AppVersion

    }
    
    
    
}

extension UIViewController{
    
    /*
    func showAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    */
    
    func showAlertOnMainThread(title:String,msg:String){
        
        //avoid crash UI task must done on main thread.
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                print("You've pressed OK Button")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    /*
    func showToastMsgOnMainThread(msg:String,forDuration:TimeInterval){
        //avoid crash UI task must done on main thread.
        DispatchQueue.main.async(execute: {
            self.view.makeToast(msg, duration: forDuration, position: .center)
        })
        
    }
    */
    
    func showConfirmationAlertWithTextFiled(alertController:UIAlertController,actionsArray:[UIAlertAction])
       {
        
        DispatchQueue.main.async(execute: {
           self.view.endEditing(true)
          // let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
           for action in actionsArray
           {
               alertController.addAction(action)
           }
           self.present(alertController, animated: true, completion: nil)
        })
            
       }
    
    func showConfirmationAlert(alertController:UIAlertController,alertTitle:String,alertMessage:String,actionsArray:[UIAlertAction])
          {
            
             DispatchQueue.main.async(execute: {
              self.view.endEditing(true)
             let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
              for action in actionsArray
              {
                  alertController.addAction(action)
              }
              self.present(alertController, animated: true, completion: nil)
            })
          }
       
       func showConfirmationAlert(alertTitle: String?, andMessage alertMessage: String?, andActions actionsArray: [AnyHashable]?, with
           viewController: UIViewController?) {
        
        DispatchQueue.main.async(execute: {
           let actionsArray = actionsArray
           
           UIView.animate(withDuration: 0, animations: {
               viewController?.view.endEditing(true)
               
           }) { finished in
               let alert = UIAlertController(title: NSLocalizedString(alertTitle ?? "", comment: ""), message: NSLocalizedString(alertMessage ?? "", comment: ""), preferredStyle: .alert)
               for i in 0..<(actionsArray?.count ?? 0) {
                   if let object = actionsArray?[i] as? UIAlertAction {
                       alert.addAction(object)
                   }
               }
               viewController?.present(alert, animated: true)
           }
           
          })
       }

}


