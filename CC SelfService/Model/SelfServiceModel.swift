//  SelfServiceModel.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 17/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import Foundation

var defString = String(stringLiteral: "")
//var defInt = -1

struct CurrentActiceApprovalCode: Codable, CustomStringConvertible {
    var ApprovalCode: String?
    var IsDubaiMall: String?
    var LicenseNo: String?
    var MallID: String?
    var ProcessResponse: String?
    var ResponseCode: String?
    var TradeName: String?
    var UserID: String?
    var UserName: String?
    var WorkingMall: String?

    var description: String {
        return """
        ------------
        ApprovalCode = \(ApprovalCode ?? defString)
        IsDubaiMall = \(IsDubaiMall ?? defString)
        LicenseNo = \(LicenseNo ?? defString)
        MallID = \(MallID ?? defString)
        ProcessResponse = \(ProcessResponse ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        TradeName = \(TradeName ?? defString)
        UserID = \(UserID ?? defString)
        UserName = \(UserName ?? defString)
        WorkingMall = \(WorkingMall ?? defString)
        ------------
        """
    }
}

struct Customer: Codable, CustomStringConvertible {
    var AreaCode: String?
    var CountryCode: String?
    var CustID: String?
    var CustName: String?
    var Email: String?
    var FirstName: String?
    var Gender: String?
    var LastName: String?
    var MobileNo: String?
    var Nationality: String?
    var ProcessResponse: String?
    var ResidenceArea: String?
    var ResidenceCity: String?
    var ResidenceCountry: String?
    var ResidenceStatus: String?
    var ResponseCode: String?
    var ShareID: String?
    var Title: String?
  
    var description: String {
        return """
        ------------
        AreaCode = \(AreaCode ?? defString)
        CountryCode = \(CountryCode ?? defString)
        CustID = \(CustID ?? defString)
        CustName = \(CustName ?? defString)
        Email = \(Email ?? defString)
        FirstName = \(FirstName ?? defString)
        Gender = \(Gender ?? defString)
        LastName = \(LastName ?? defString)
        MobileNo = \(MobileNo ?? defString)
        Nationality = \(Nationality ?? defString)
        ProcessResponse = \(ProcessResponse ?? defString)
        ResidenceArea = \(ResidenceArea ?? defString)
        ResidenceCity = \(ResidenceCity ?? defString)
        ResidenceCountry = \(ResidenceCountry ?? defString)
        ResidenceStatus = \(ResidenceStatus ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        ShareID = \(ShareID ?? defString)
        Title = \(Title ?? defString)
        
        ------------
        """
    }
}


struct GetInvoiceMallList: Codable, CustomStringConvertible {
    
    var ListInvoiceMall = [InvoiceMallList]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListInvoiceMall = \(ListInvoiceMall)
        ------------
        """
    }
    
}

 struct InvoiceMallList: Codable, CustomStringConvertible {
    
    var Currency: String?
    var MallID: Int?
    var MallName: String?
    var MallNameArabic: String?
    

    var description: String {
        return """
        ------------
        Currency = \(Currency ?? defString )
        MallID = \(MallID ?? -1)
        MallName = \(MallName ?? defString)
        MallNameArabic = \(MallNameArabic ?? defString )
        ------------
        """
    }
}

struct GetInvoiceMallListFailledCase: Codable, CustomStringConvertible {
    
    var ListInvoiceMall : String?
    var ResponseCode: String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ListInvoiceMall = \(ListInvoiceMall ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ------------
        """
    }
}

struct NewRegistrationDetails: Codable, CustomStringConvertible {
    
    var Title: String?
    var TitleCode: String?
    var FirstName: String?
    var LastName: String?
    var CustName: String?
    var ResidentialStatusID: String?
    var NationalityID: String?
    var CountryResidenceID: String?
    //var CityEmirate: String?
    var AreaOfResidenceID: String?
    var CountryCode: String?
    var DialCode: String?
    var MobileNumber: String?
    var ShareId: String?
    var EmailAddress: String?
    var ResidenceCityID: String?
    /*
    var MallID: String?
    var StoreID: String?
    var PaymentModeStr: String?
    var InvoiceDateStr: String?
    var InvoiceNumber: String?
    var InvoiceValue: String?
    */
    var CampaignID : String?
    var CustomerID : String?
    
    var description: String {
        return """
        ------------
        Title = \(Title ?? defString)
        TitleCode = \(TitleCode ?? defString)
        FirstName = \(FirstName ?? defString)
        LastName = \(LastName ?? defString)
        CustName = \(CustName ?? defString)
        ResidentialStatusID = \(ResidentialStatusID ?? defString)
        NationalityID = \(NationalityID ?? defString)
        CountryResidenceID = \(CountryResidenceID ?? defString)
        AreaOfResidenceID = \(AreaOfResidenceID ?? defString)
        CountryCode = \(CountryCode ?? defString)
        DialCode =  = \(DialCode ?? defString)
        MobileNumber = \(MobileNumber ?? defString)
        ShareId = \(ShareId ?? defString)
        EmailAddress = \(EmailAddress ?? defString)
        CampaignID = \(CampaignID ?? defString)
        CustomerID = \(CustomerID ?? defString)
        ResidenceCityID = \(ResidenceCityID ?? defString)
        ------------
        """
        
        /*
        MallID = \(MallID ?? defString)
        StoreID = \(StoreID ?? defString)
        PaymentModeStr =  \(PaymentModeStr ?? defString)
        InvoiceDateStr = \(InvoiceDateStr ?? defString)
        InvoiceNumber = \(InvoiceNumber ?? defString)
        InvoiceValue = \(InvoiceValue ?? defString)
        */
    }
}


struct NameTitleList: Codable, CustomStringConvertible {
    
    var ListTitleContract = [TitleArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListTitleContract = \(ListTitleContract)
        ------------
        """
    }
    
}

struct TitleArray: Codable, CustomStringConvertible {
    
    var Code: String?
    var Description: String?
    var Description_Arabic: String?
    

    var description: String {
        return """
        ------------
        Code = \(Code ?? defString )
        Description = \(Description ?? defString)
        Description_Arabic = \(Description_Arabic ?? defString )
        ------------
        """
    }
}


struct Nationality: Codable, CustomStringConvertible {
    
    var ListNationalityContract = [NationalityArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListNationalityContract = \(ListNationalityContract)
        ------------
        """
    }
    
}

struct NationalityArray: Codable, CustomStringConvertible {
    
       var Code: String?
       var Description: String?
       var DialCode: String?
       

       var description: String {
           return """
           ------------
           Code = \(Code ?? defString)
           Description = \(Description ?? defString)
           DialCode = \(DialCode ?? defString)
           ------------
           """
       }
}

/*
struct ResidentialStatus: Codable, CustomStringConvertible {
    
    //var ResidentialStatusList = [ResidentialStatusArray]()
    var Code: String?
    var Description: String?
    
    var description: String {
        return """
        ------------
         Code = \(Code ?? defString)
         Description = \(Description ?? defString)
        ------------
        """
    }
    
}
*/

struct ResidentialStatusArray: Codable, CustomStringConvertible {
        
        var Code: String?
        var Description: String?
        
       var description: String {
        
        return """
        ------------
        
        Code = \(Code ?? defString)
        Description = \(Description ?? defString)
        ------------
        """
    }
    
}


struct CityList: Codable, CustomStringConvertible {
    
    var ListCityContract = [CityListaArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListCityContract = \(ListCityContract)
        ------------
        """
    }
    
}

struct CityListaArray: Codable, CustomStringConvertible {
    
    var CityID: Int?
    var CityName: String?
    var CityNameArabic: String?
    var CountryCode: String?
    

    var description: String {
        return """
        ------------
        CityID = \(CityID ?? -1 )
        CityName = \(CityName ?? defString)
        CityNameArabic = \(CityNameArabic ?? defString )
        CountryCode = \(CountryCode ?? defString )
        ------------
        """
    }
}

struct AreaList: Codable, CustomStringConvertible {
    
    var ListAreaContract = [AreaListaArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListAreaContract = \(ListAreaContract)
        ------------
        """
    }
    
}

struct AreaListaArray: Codable, CustomStringConvertible {
    
    var AreaID: Int?
    var AreaName: String?
    var AreaNameArabic: String?
    var CityID: Int?
    

    var description: String {
        return """
        ------------
        AreaID = \(AreaID ?? -1 )
        AreaName = \(AreaName ?? defString)
        AreaNameArabic = \(AreaNameArabic ?? defString )
        CityID = \(CityID ?? -1 )
        ------------
        """
    }
}

struct PaymentList: Codable, CustomStringConvertible {
    
    var ListPaymentModeContract = [PaymentListaArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListPaymentModeContract = \(ListPaymentModeContract)
        ------------
        """
    }
    
}

struct PaymentListaArray: Codable, CustomStringConvertible {
    
    var Code: String?
    var Description: String?
    var Description_Arabic: String?
    
    

    var description: String {
        return """
        ------------
        Code = \(Code ?? defString )
        Description = \(Description ?? defString)
        Description_Arabic = \(Description_Arabic ?? defString )
        
        ------------
        """
    }
    
}
 struct ReceiptArray: Codable, CustomStringConvertible {
    
    var InvoiceNumber: String?
    var MallName: String?
    var StoreName: String?
    var MallID: String?
    var StoreID: String?
    var InvoiceValue: String?
    var InvoiceDateStrAPIFormat: String?
    var InvoiceDateStrDisplayFormat: String?
    var PaymentModeStr: String?
    

    var description: String {
        return """
        ------------
        InvoiceNumber = \(InvoiceNumber ?? defString )
        MallName = \(MallName ?? defString)
        StoreName = \(StoreName ?? defString )
        MallID = = \(MallID ?? defString )
        StoreID = \(StoreID ?? defString )
        InvoiceValue = \(InvoiceValue ?? defString )
        InvoiceDateStrAPIFormat = \(InvoiceDateStrAPIFormat ?? defString )
        InvoiceDateStrDisplayFormat = \(InvoiceDateStrDisplayFormat ?? defString )
        PaymentModeStr = \(PaymentModeStr ?? defString )
        ------------
        """
     }
    
}


struct StoreList: Codable, CustomStringConvertible {
    
    var ListInvoiceStores = [StoreListaArray]()
    var ResponseCode:  String?
    var Result: String?
    

    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        ListInvoiceStores = \(ListInvoiceStores)
        ------------
        """
    }
    
}

struct StoreListaArray: Codable, CustomStringConvertible {
    
    var CategoryAr: String?
    var CategoryEn: String?
    var MallID: Int?
    var StoreID: Int?
    var StoreName: String?
    var StoreNameArabic: String?
    

    var description: String {
        return """
        ------------
        CategoryAr = \(CategoryAr ?? defString )
        CategoryEn = \(CategoryEn ?? defString)
        MallID = \(MallID ?? -1 )
        StoreID = \(StoreID ?? -1 )
        StoreName = \(StoreName ?? defString )
        StoreNameArabic = \(StoreNameArabic ?? defString )
        
        ------------
        """
    }
}


struct ActiveCampaignsByMall: Codable, CustomStringConvertible {
    
    var CampaignList = [ArrayActiveCampaignsByMall]()
    var ResponseCode:  String?
    var Result: String?
    
    var description: String {
        return """
        ------------
        ResponseCode = \(ResponseCode ?? defString)
        Result = \(Result ?? defString)
        CampaignList = \(CampaignList)
        ------------
        """
    }
}

struct ArrayActiveCampaignsByMall: Codable, CustomStringConvertible {
    
    var CampID: String?
    var CampaignDesc: String?
    var ShareIDRequired: String?
    
    var description: String {
        return """
        ------------
        CampID = \(CampID ?? defString)
        CampaignDesc = \(CampaignDesc ?? defString)
        ShareIDRequired = \(ShareIDRequired ?? defString )
        ------------
        """
    }
}

struct NewCustomerOREntryCreated: Codable, CustomStringConvertible {
    
    var ProcessResponse: String?
    var ResponseCode:  String?
    var RefNo: String?
    
    var description: String {
        return """
        ------------
        ProcessResponse = \(ProcessResponse ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        RefNo = \(RefNo ?? defString)
        ------------
        """
    }
}

struct NewReceiptCreated: Codable, CustomStringConvertible {
    
    var ProcessResponse: String?
    var ResponseCode:  String?
    var RefNo: String?
    
    var description: String {
        return """
        ------------
        ProcessResponse = \(ProcessResponse ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        RefNo = \(RefNo ?? defString)
        ------------
        """
    }
}

/*
{"EntryRef":"eee49d19-f5e5-435d-bb56-ae1e4ab9acdc","RequestTimeStamp":"2020-05-07 11:21:30","AppKey":"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a","TokenID":"uCYdIVMIR93UMCAWprk3aBZ87YMGGbkp1tpXqby+CGw=","CampaignList":"388,392"}
*/

struct ConfirmEntryForCampaign: Codable, CustomStringConvertible {
    
    var CampaignTokenList:  [ArrayCampaignToken]?
    var ReceiptStatusList =  [ArrayReceiptStatus]()
    var Result : String?
    
    var description: String {
        return """
        ------------
        CampaignTokenList = \(String(describing: CampaignTokenList))
        ReceiptStatusList = \(ReceiptStatusList)
        Result = \(Result ?? defString)
        ------------
        """
    }
}

struct ArrayCampaignToken: Codable, CustomStringConvertible {
    
    var CampID: String?
    var CampaignName: String?
    var MallName: String?
    var TokenCode: String?
    
    var description: String {
        return """
        ------------
        CampID = \(CampID ?? defString)
        CampaignName = \(CampaignName ?? defString)
        MallName = \(MallName ?? defString)
        TokenCode = \(TokenCode ?? defString)
        ------------
        """
    }
}

struct ArrayReceiptStatus: Codable, CustomStringConvertible {
    
    var FileRefNo: String?
    var ProcessResponse: String?
    var ResponseCode: String?
   
    
    var description: String {
        return """
        ------------
        FileRefNo = \(FileRefNo ?? defString)
        ProcessResponse = \(ProcessResponse ?? defString)
        ResponseCode = \(ResponseCode ?? defString)
        ------------
        """
    }
}


struct ValidateEToken: Codable, CustomStringConvertible {
    
    var AckStatus: String?
    var CampID: String?
    var CampaignName: String?
    var CustID:  String?
    var CustName: String?
    var Email:  String?
    var IDNo:  String?
    var IsUsed:  String?
    var IssuedAt:  String?
    var IssuedBy:  String?
    var MobileNo:  String?
    var PermitNo:  String?
    var PrizeInfo:  String?
    var ProcessResponse: String?
    var ResponseCode:  String?
    var TokenCode:  String?
    var TotalTimeUsed:  String?
    
    var description: String {
        return """
        ------------
        AckStatus = \(AckStatus ?? defString)
        CampID = \(CampID ?? defString)
        CampaignName = \( CampaignName ?? defString)
        CustID = \(CustID ?? defString)
        CustName  = \(CustName ?? defString)
        Email  = \( Email ?? defString)
        IDNo = \(IDNo ?? defString)
        IsUsed = \( IsUsed ?? defString)
        IssuedAt  = \(IssuedAt ?? defString)
        IssuedBy = \(IssuedBy ?? defString)
        MobileNo  = \(MobileNo ?? defString)
        PermitNo  = \( PermitNo ?? defString)
        PrizeInfo  = \(PrizeInfo ?? defString)
        ProcessResponse  = \(ProcessResponse ?? defString)
        ResponseCode  = \(ResponseCode ?? defString)
        TokenCode  = \(TokenCode ?? defString)
        TotalTimeUsed  = \(TotalTimeUsed ?? defString)
        
        ------------
        """
    }
}


struct AcknowledgementTblData: Codable, CustomStringConvertible {
    
    var Title = String()
    var Des_Eng = String()
    var Des_Arabic = String()
    
    var description: String {
        return """
        ------------
        Title = \(Title )
        Des_Eng = \(Des_Eng)
        Des_Arabic = \(Des_Arabic)
        ------------
        """
    }
}


struct FirebaseDataModel {
    
    var isTest: Bool? = false
    
    init(isTest:Bool) {
        self.isTest = isTest
    }
    
    init(snapsot: [String: Any]) {
        
        isTest = snapsot["isTest"] as? Bool ?? false
    }
}
