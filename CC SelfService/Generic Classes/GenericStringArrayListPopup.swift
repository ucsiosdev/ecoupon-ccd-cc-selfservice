//
//  GenericStringArrayListPopup.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 17/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit


protocol GenericStringArrayListPopupDelegate : class{
    
    func mallDidSelectWorkingMall()
    //func mallPopupDidClose()
}

class GenericStringArrayListPopup: BaseViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var navigationTitleString: UILabel!
    @IBOutlet var tblViewSettings: UITableView!
    @IBOutlet var searchBarGeneric: UISearchBar!
    @IBOutlet var viweParentSearchBar: UIView!
    weak var delegate : GenericStringArrayListPopupDelegate?
    
    var stringsArray :[String] = []
    var unfilteredStringsArray :[String] = []
    var isSearching = false
    var isComingFromSettingVC = false
    var navigTitle = KCCSELFSERVICE
    var searchBarPlaceHolderStr = KSearch
    
    //MARK: VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initObj()
        
        setupView()
    }
    
    //MARK: HELPER METHODS
    func initObj(){
        
        unfilteredStringsArray = stringsArray
        searchBarGeneric.delegate = self
        tblViewSettings.tableFooterView = UIView()
        tblViewSettings.register(UINib(nibName: "TblCellGeneric", bundle: nil), forCellReuseIdentifier: "TblCellGeneric")
        navigationTitleString.text = navigTitle
        searchBarGeneric.placeholder = searchBarPlaceHolderStr
    }
    
    func setupView(){
        viweParentSearchBar.addTopandBottomBorder()
        searchBarGeneric.barTintColor = SelfServiceColor().HeaderViewBackgroundColor
    }
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblCellGeneric", for: indexPath) as! TblCellGeneric
        cell.lblTitle.text = stringsArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
       // var matchedString = stringsArray[indexPath.row]
        //var matchedRow = indexPath.row
        
        if isComingFromSettingVC {
            dismiss(animated: true) {
                /*
                if let matchedOffset = self.unfilteredStringsArray.index(where: {$0 == matchedString}) {
                    matchedString = self.unfilteredStringsArray[matchedOffset]
                   // matchedRow = matchedOffset
                } else {
                    //matchedRow = indexPath.row
                }
                
                let model = CurrentActiceApprovalCode (ApprovalCode: "", IsDubaiMall: "", LicenseNo: "", MallID: "", ProcessResponse: "", ResponseCode: "", TradeName: "", UserID: "", UserName: "", WorkingMall: matchedString)
                */
                
                if let _ = self.delegate?.mallDidSelectWorkingMall(){
                    print ("\n\n *** is Delegate Method > mallDidSelectWorkingMall Avaialble: YES  ***\n\n")
                }
            }
        }
  }
    
    //MARK:- SEARCH BAR METHODS
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        isSearching = true
        searchBarGeneric.setShowsCancelButton(true, animated: true)
        self.searchContentwithText(searchString: searchText)
    }
    
    func searchContentwithText(searchString:String){
        if searchString == "" {
            stringsArray = unfilteredStringsArray
            tblViewSettings.reloadData()
        }
        else {
            let filterContent = unfilteredStringsArray.filter({$0.lowercased().contains("\(searchString.lowercased())")})
            stringsArray = filterContent
            tblViewSettings.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        
        stringsArray = unfilteredStringsArray
        searchBarGeneric.resignFirstResponder()
        searchBarGeneric.setShowsCancelButton(false, animated: true)
        searchBarGeneric.text = ""
        tblViewSettings.reloadData()
        isSearching = false
    }
}
