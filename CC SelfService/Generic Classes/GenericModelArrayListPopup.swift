//
//  GenericModelArrayListPopup.swift
//  CC SelfService
//
//  Created by Ravinder Kumar on 17/04/20.
//  Copyright © 2020 UCS. All rights reserved.
//

import UIKit


protocol GenericModelArrayListPopupDelegate : class{
    
    //case 1: isComingFromMobileNumberAndShareIDVC
    func countryListDidSelectCountry(rowNumber:NSInteger)
    
    //case 2: isComingFromSettingVC
    func mallPopupDidSelectedMall(selectedMall:InvoiceMallList)
    
    //case 3: isNeedsShowMallList
    func nameTitlePopupDidSelectedTitle(index:NSInteger)
    
    //case 4: isComingNewRegistartionVC_ResStatus
    func residentialStatusSelected(model:ResidentialStatusArray)
    
     //case 5 isComingNewRegistartionVC_Nationality
    func nationlitySelected(index:NSInteger)
    
    //case 6: isComingNewRegistartionVC_ResCountry
    func residenceCounrySelected(index:NSInteger)
    
    //case 7: isComingNewRegistartionVC_City
    func cityEmirateSelected(modelCity:CityListaArray)
    
    //case 8: isComingNewRegistartionVC_Area
    func areaSelected(modelArea:AreaListaArray)
    
    //case 9: isComingReceiptEntry
    func paymentModeSelectedPayment(modelPayment:PaymentListaArray)
    
    //case 10: isShowStoreList
    func storeDidSelect(modelStore:StoreListaArray)
}


class GenericModelArrayListPopup: BaseViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var searchBarGeneric: UISearchBar!
    @IBOutlet var viweParentSearchBar: UIView!
    @IBOutlet var navigationTitleString: UILabel!
    
    var contentArray :[AnyObject] = []
    var unFilteredContentArray :[AnyObject] = []
    var isSearching = false
    var isComingFromSettingVC = false
    var isComingFromMobileNumberAndShareIDVC = false
    var isNeedsShowMallList = false
    var isComingNewRegistartionVC_ResStatus = false
    var isComingNewRegistartionVC_Nationality = false
    var isComingNewRegistartionVC_ResCountry = false
    var isComingNewRegistartionVC_City = false
    var isComingNewRegistartionVC_Area = false
    var isComingReceiptEntry = false
    var isShowStoreList = false
    
    
    var navigTitle = KCCSELFSERVICE
    var searchBarPlaceHolderStr = KSearch
    
    weak var delegate : GenericModelArrayListPopupDelegate?
    
    //MARK: VC LIFE CYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        initObj()
        setupView()
    }
    
    //MARK: HELPER METHODS
    func initObj(){
        
        unFilteredContentArray = contentArray
        searchBarGeneric.delegate = self
        tblView.tableFooterView = UIView()
        
        tblView.register(UINib(nibName: "TblCellGeneric", bundle: nil), forCellReuseIdentifier: "TblCellGeneric")
        viweParentSearchBar.addTopandBottomBorder()
        navigationTitleString.text = navigTitle
        searchBarGeneric.placeholder = searchBarPlaceHolderStr
    }
    
    func setupView(){
        searchBarGeneric.barTintColor = SelfServiceColor().HeaderViewBackgroundColor
    }
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblCellGeneric", for: indexPath) as! TblCellGeneric
        cell.lblTitle.text = ""
        
        let currentContent = contentArray[indexPath.row]
        switch currentContent {
        // case: 1 isComingFromSettingVC
        case let modelMall as InvoiceMallList:
            cell.lblTitle.text = modelMall.MallName
            return cell
            
        //case 2: isComingFromMobileNumberAndShareIDVC
        case let modelCountry as Country:
            cell.lblTitle.text = "+\(modelCountry.phoneExtension)   -   \(modelCountry.name ?? "")"
            return cell
            
        //case 3: isNeedsShowMallList
        case let modelTitle as TitleArray:
            cell.lblTitle.text = modelTitle.Description
            return cell
        
        //case 4: isNeedsShowMallList
        case let modelResidentialStatus as ResidentialStatusArray:
            cell.lblTitle.text = modelResidentialStatus.Description
            return cell
           
        //case 5 & 6 isComingNewRegistartionVC_Nationality , isComingNewRegistartionVC_ResCountry
        case let modelNationlity as NationalityArray:
            cell.lblTitle.text = modelNationlity.Description
            return cell
            
       //case 7: isComingNewRegistartionVC_ResCountry
       case let modelCity as CityListaArray:
            cell.lblTitle.text = modelCity.CityName
            return cell
            
       //case 8: isComingNewRegistartionVC_Area
        case let model as AreaListaArray:
            cell.lblTitle.text = model.AreaName
            return cell
            
        //case 9: isComingReceiptEntry
        case let model as PaymentListaArray:
            cell.lblTitle.text = model.Description
            return cell
            
        
            
        //case 10: isShowStoreList
        case let model as StoreListaArray:
             cell.lblTitle.text = model.StoreName
             return cell
            
            
        default:
            print("\n\n **** unknown type in cell  **\n\n")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        var matchedIndex = indexPath.row
        
        let currentContent = contentArray[indexPath.row]
        switch currentContent {
            
        // case: 1 isComingFromSettingVC
        case let modelMallList as InvoiceMallList:
            
            if let unFilterdMallList = unFilteredContentArray as? [InvoiceMallList] {
                if let matchedOffset = unFilterdMallList.index(where: {$0.MallName == modelMallList.MallName}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
            
        //case 2: isComingFromMobileNumberAndShareIDVC
        case let modelCountry as Country:
            
            if let unFilteredCountry = unFilteredContentArray as? [Country] {
                
                if let matchedOffset = unFilteredCountry.index(where: {$0.name == modelCountry.name}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
            
       //case 3: isNeedsShowMallList
       case let modelTitle as TitleArray:
           
           if let unFilteredTitle = unFilteredContentArray as? [TitleArray] {
               
               if let matchedOffset = unFilteredTitle.index(where: {$0.Description == modelTitle.Description}) {
                   matchedIndex = matchedOffset
               }else {
                   matchedIndex = indexPath.row
               }
           }
            
        //case 4: isNeedsShowMallList
        case let modelResStatus as ResidentialStatusArray:
            
            if let unFilteredTitle = unFilteredContentArray as? [ResidentialStatusArray] {
                
                if let matchedOffset = unFilteredTitle.index(where: {$0.Description == modelResStatus.Description}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
            
        //case 5 & 6 isComingNewRegistartionVC_Nationality , isComingNewRegistartionVC_ResCountry
        case let modelNationlity as NationalityArray:
            
            if let unFilteredTitle = unFilteredContentArray as? [NationalityArray] {
                
                if let matchedOffset = unFilteredTitle.index(where: {$0.Description == modelNationlity.Description}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
                
         //case 7: isComingNewRegistartionVC_City
         case let modelCity as CityListaArray:
            if let unFilteredCity = unFilteredContentArray as? [CityListaArray] {
                
                if let matchedOffset = unFilteredCity.index(where: {$0.CityName == modelCity.CityName}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
            
         //case 8: isComingNewRegistartionVC_Area
        case let modelArea as AreaListaArray:
          if let unFilteredArea = unFilteredContentArray as? [AreaListaArray] {
                
                if let matchedOffset = unFilteredArea.index(where: {$0.AreaName == modelArea.AreaName}) {
                    matchedIndex = matchedOffset
                }else {
                    matchedIndex = indexPath.row
                }
            }
            
          //case 9: isComingReceiptEntry
           case let model as PaymentListaArray:
             if let unFilteredPayment = unFilteredContentArray as? [PaymentListaArray] {
                   
                   if let matchedOffset = unFilteredPayment.index(where: {$0.Description == model.Description}) {
                       matchedIndex = matchedOffset
                   }else {
                       matchedIndex = indexPath.row
                   }
               }
            
            
            //case 10: isShowStoreList
            case let model as StoreListaArray:
                if let unFilteredPayment = unFilteredContentArray as? [StoreListaArray] {
                    
                    if let matchedOffset = unFilteredPayment.index(where: {$0.StoreName == model.StoreName}) {
                        matchedIndex = matchedOffset
                    }else {
                        matchedIndex = indexPath.row
                    }
            }
            
            
        default:
            print("unknown type in cell")
        }
        
        // case: 1 isComingFromSettingVC
        if isComingFromSettingVC{
            dismiss(animated: true) {
                
                if let selectedMallList = self.contentArray[indexPath.row] as? InvoiceMallList{
                    if let _ = self.delegate?.mallPopupDidSelectedMall(selectedMall:selectedMallList){
                        print ("\n\n *** is Delegate Method > mallPopupDidSelectedMall Avaialble: YES  ***\n\n")
                    }
                }
            }
        }
            
        //case 2: isComingFromMobileNumberAndShareIDVC
        else if isComingFromMobileNumberAndShareIDVC {
            dismiss(animated: true) {
                if let _ = self.delegate?.countryListDidSelectCountry(rowNumber: matchedIndex){
                    print ("\n\n *** is Delegate Method > countryListDidSelectCountry Avaialble: YES  ***\n\n")
                }
            }
            
        }
        
        //case 3: isNeedsShowMallList
        else if isNeedsShowMallList {
            dismiss(animated: true) {
                if let _ = self.delegate?.nameTitlePopupDidSelectedTitle(index: matchedIndex){
                    print ("\n\n *** is Delegate Method > nameTitlePopupDidSelectedTitle Avaialble: YES  ***\n\n")
                }
            }
            
        }
            
       //case 4: isComingNewRegistartionVC_ResStatus
        else if isComingNewRegistartionVC_ResStatus{
                   dismiss(animated: true) {
                       
                       if let selectedModel = self.contentArray[indexPath.row] as? ResidentialStatusArray{
                           if let _ = self.delegate?.residentialStatusSelected(model:selectedModel ){
                               print ("\n\n *** is Delegate Method > residentialStatusSelected Avaialble: YES  ***\n\n")
                           }
                       }
                   }
               }
        
        //case 5: isComingNewRegistartionVC_Nationality
        else if isComingNewRegistartionVC_Nationality{
            dismiss(animated: true) {
                
                if self.contentArray[indexPath.row] is NationalityArray{
                    if let _ = self.delegate?.nationlitySelected(index:matchedIndex){
                        print ("\n\n *** is Delegate Method > nationlitySelected Avaialble: YES  ***\n\n")
                    }
                }
            }
        }
        
        //case 6 isComingNewRegistartionVC_ResCountry
        else if isComingNewRegistartionVC_ResCountry{
            dismiss(animated: true) {
                
                if self.contentArray[indexPath.row] is NationalityArray{
                    if let _ = self.delegate?.residenceCounrySelected(index:matchedIndex){
                        print ("\n\n *** is Delegate Method > residenceCounrySelected Avaialble: YES  ***\n\n")
                    }
                }
            }
        }
        
        
        //case 7: isComingNewRegistartionVC_City
        else if isComingNewRegistartionVC_City{
            dismiss(animated: true) {
                if let selectedModel = self.contentArray[indexPath.row] as? CityListaArray{
                    if let _ = self.delegate?.cityEmirateSelected(modelCity:selectedModel){
                        print ("\n\n *** is Delegate Method > cityEmirateSelected Avaialble: YES  ***\n\n")
                    }
                }
            }
        }
        
      //case 8: isComingNewRegistartionVC_Area
        else if isComingNewRegistartionVC_Area{
             dismiss(animated: true) {
                 if let selectedModel = self.contentArray[indexPath.row] as? AreaListaArray{
                     if let _ = self.delegate?.areaSelected(modelArea:selectedModel){
                         print ("\n\n *** is Delegate Method > areaSelected Avaialble: YES  ***\n\n")
                     }
                 }
             }
         }
        
        
        //case 9: isComingReceiptEntry
        else if isComingReceiptEntry{
             dismiss(animated: true) {
                 if let selectedModel = self.contentArray[indexPath.row] as? PaymentListaArray{
                    if let _ = self.delegate?.paymentModeSelectedPayment(modelPayment:selectedModel ){
                         print ("\n\n *** is Delegate Method > paymentModeSelectedPayment Avaialble: YES  ***\n\n")
                     }
                 }
             }
         }
        
        
         //case 10: isShowStoreList
        else if isShowStoreList{
            dismiss(animated: true) {
                if let selectedModel = self.contentArray[indexPath.row] as? StoreListaArray{
                    if let _ = self.delegate?.storeDidSelect(modelStore: selectedModel){
                    print ("\n\n *** is Delegate Method > storeDidSelect Avaialble: YES  ***\n\n")
                }
            }
        }
        
       }
    }
    //MARK:- SEARCH BAR METHODS
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        isSearching = true
        searchBarGeneric.setShowsCancelButton(true, animated: true)
        self.searchContentwithText(searchString: searchText)
    }
    
    
    func searchContentwithText(searchString:String){
        if searchString == "" {
            contentArray = unFilteredContentArray
            tblView.reloadData()
            
        }
        else {
            if(unFilteredContentArray.count>0){
                let currentContent = unFilteredContentArray[0]
                
                switch currentContent {
                    
                // case: 1 isComingFromSettingVC
                case _ as InvoiceMallList:
                    let mallArray = unFilteredContentArray.compactMap { $0 as? InvoiceMallList }
                    let filterContent = mallArray.filter{($0.MallName ?? "").lowercased().contains("\(searchString.lowercased())")}
                    contentArray = filterContent as [AnyObject]
                    tblView.reloadData()
                    
                //case 2: isComingFromMobileNumberAndShareIDVC
                case _ as Country:
                    let countryArray = unFilteredContentArray.compactMap { $0 as? Country }
                    let filterContent = countryArray.filter{($0.name ?? "").lowercased().contains("\(searchString.lowercased())")}
                    contentArray = filterContent
                    tblView.reloadData()
                    
                    
                //case 3: isNeedsShowMallList
                case _ as TitleArray:
                    let titleArray = unFilteredContentArray.compactMap { $0 as? TitleArray }
                    let filterContent = titleArray.filter{($0.Description ?? "").lowercased().contains("\(searchString.lowercased())")}
                    contentArray = filterContent as [AnyObject]
                    tblView.reloadData()
                    
                    
                //case 4: isNeedsShowMallList
                case _ as ResidentialStatusArray:
                    let resStatus = unFilteredContentArray.compactMap { $0 as? ResidentialStatusArray }
                    let filterContent = resStatus.filter{($0.Description ?? "").lowercased().contains("\(searchString.lowercased())")}
                    contentArray = filterContent as [AnyObject]
                    tblView.reloadData()
                    
                //case 5 & 6 isComingNewRegistartionVC_Nationality , isComingNewRegistartionVC_ResCountry
                case _ as NationalityArray:
                    let nationality = unFilteredContentArray.compactMap { $0 as? NationalityArray }
                    let filterContent = nationality.filter{($0.Description ?? "").lowercased().contains("\(searchString.lowercased())")}
                    contentArray = filterContent as [AnyObject]
                    tblView.reloadData()
              
            //case 7: isComingNewRegistartionVC_City
             case _ as CityListaArray:
                 let city = unFilteredContentArray.compactMap { $0 as? CityListaArray }
                 let filterContent = city.filter{($0.CityName ?? "").lowercased().contains("\(searchString.lowercased())")}
                 contentArray = filterContent as [AnyObject]
                 tblView.reloadData()
                    
            //case 8: isComingNewRegistartionVC_Area
            case _ as AreaListaArray:
                let city = unFilteredContentArray.compactMap { $0 as? AreaListaArray }
                let filterContent = city.filter{($0.AreaName ?? "").lowercased().contains("\(searchString.lowercased())")}
                contentArray = filterContent as [AnyObject]
                tblView.reloadData()

                    
           //case 9: isComingReceiptEntry
           case _ as PaymentListaArray:
              let payment = unFilteredContentArray.compactMap { $0 as? PaymentListaArray }
              let filterContent = payment.filter{($0.Description ?? "").lowercased().contains("\(searchString.lowercased())")}
              contentArray = filterContent as [AnyObject]
              tblView.reloadData()
                    
                    
           //case 10: isShowStoreList
            case _ as StoreListaArray:
            let storeArray = unFilteredContentArray.compactMap { $0 as? StoreListaArray }
            let filterContent = storeArray.filter{($0.StoreName ?? "").lowercased().contains("\(searchString.lowercased())")}
            contentArray = filterContent as [AnyObject]
            tblView.reloadData()
                    
                default:
                    print("unknown search type")
                }
                
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        
        contentArray = unFilteredContentArray
        searchBarGeneric.resignFirstResponder()
        searchBarGeneric.setShowsCancelButton(false, animated: true)
        searchBarGeneric.text = ""
        tblView.reloadData()
        isSearching = false
    }
}
